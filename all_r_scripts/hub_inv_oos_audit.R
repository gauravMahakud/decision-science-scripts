library(RPostgreSQL)
library(dplyr)
library(RMySQL)
cn <- dbConnect(
  drv      = RMySQL::MySQL(),
  username = "puja",
  password = "SaeN6Bee6c",
  host     = "prod-read-replica.ce6lasmdz6oo.ap-southeast-1.rds.amazonaws.com",
  port     = 4008,
  dbname   = "jt_appsheet"
)
get_from_last_state <-
  "select * from jt_appsheet.hub_inventory_last_state"

get_inv_details<-"select t.*,case when a.available_units is null then 0 else a.available_units end as available_units
from (select distinct product_id,f.natural_id as facility_id,f.name as hubid
from jt_space_prod.space_product_qty_map psm
join jt_space_prod.facility f on f.id=psm.facility_id
where psm.association_status='ACTIVE' and f.facility_type='HUB') t
left join (select ii.product_id,f.natural_id,sum(ii.left_qty) as available_units
from jt_space_prod.inventory_item ii
join jt_space_prod.space s on s.id=ii.space_id
join jt_space_prod.facility f on f.id=s.facility_id
where ii.state='SELLABLE' and ii.status='ACTIVE' and f.facility_type='HUB'
group by 1,2) a on a.product_id=t.product_id and a.natural_id=t.facility_id"

delete_last_state<-"delete from jt_appsheet.hub_inventory_last_state"

inv_state<-dbGetQuery(cn, get_inv_details)
last_inventory_state<-dbGetQuery(cn, get_from_last_state)

drv <- dbDriver("PostgreSQL")
con1 <-
  dbConnect(
    drv,
    host = "datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com",
    port = "5439",
    dbname = "datawarehousedb",
    user = "jttech",
    password = "wRu$ASwad86X"
  )

get_moq_details <- "
select jpin as product_id,min_order_quantity
from(select sp.jpin,li.min_order_quantity, row_number() over(partition by sp.jpin order by liv.available_in_lots desc) as rnk
from listing_snapshot li
join sellerproduct_snapshot sp on sp.sp_id=li.sp_id
join listing_inventory_snapshot liv on liv.listing_id=li.listing_id
where li.channel_id='ORGPROF-1304473229' and li.status='ACTIVE')
where rnk=1
"

moq_details <- dbGetQuery(con1, get_moq_details)
dbDisconnect(con1)

instock<-inv_state %>%
  left_join(moq_details) %>%
  mutate(state=ifelse(!is.na(min_order_quantity) & available_units>=10,"In-Stock","Out of Stock"),
         update_timestamp = Sys.time()) %>%
  select(product_id,facility_id,hubid,state,update_timestamp)

change_check<-instock %>%
  left_join(last_inventory_state, by =c("product_id"="product_id","facility_id"="facility_id")) %>%
  filter((state.x!=state.y & state.y=="Out of Stock") |is.na(state.y) ) %>%
  mutate(state=state.x,hubid=hubid.x, update_timestamp=Sys.time(),pkey=paste(product_id,facility_id,state,update_timestamp)) %>%
  select(product_id,facility_id,hubid,state,update_timestamp,pkey)
dbWriteTable(
  cn,
  value = change_check,
  name = "hub_inventory_in_stock_audit",
  field.types = NULL,
  row.names = FALSE,
  overwrite = FALSE,
  append = TRUE,
  allow.keywords = FALSE
)
dbGetQuery(cn, delete_last_state)
dbWriteTable(
  cn,
  value = instock,
  name = "hub_inventory_last_state",
  field.types = NULL,
  row.names = FALSE,
  overwrite = FALSE,
  append = TRUE,
  allow.keywords = FALSE
)


dbDisconnect(cn)
