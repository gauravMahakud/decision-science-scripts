#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
drop table if exists customer_milestone_;
select distinct s.bid,
s.storeonboarddate,extract(month from s.storeonboarddate) as onboarded_month_number,
to_char(s.storeonboarddate, 'Month') as onboarded_month,
oa.first_order_date,
oa.first_order_date-s.storeonboarddate as days_to_first_order,
act.date as activation_date,
act.date-s.storeonboarddate as days_to_activation,
oa.last_order_date,
current_date-oa.last_order_date as days_since_last_order
into  customer_milestone_
from store_name s 
left join(select s.bid,
          date(min(o.srccreatedtime)) as first_order_date,
          date(max(o.srccreatedtime)) as last_order_date,
          count(distinct(o.srccreatedtime)) as number_of_order_dates
          from order_snapshot_ o 
          join store_name s on s.customerid=o.customerid
          where o.statusstring!='Cancelled' and o.statusstring!='Return Completed' and o.orderamount>0
          group by 1) oa on oa.bid=s.bid
          
left join (select s.bid,date(o.srccreatedtime),dense_rank() over (partition by s.bid order by date(o.srccreatedtime)) as rank
          from order_snapshot_ o 
          join store_name s on s.customerid=o.customerid
          where o.statusstring!='Cancelled' and o.statusstring!='Return Completed' and o.orderamount>0) act on act.bid=s.bid and act.rank=5;
EOF
echo "Ran the customer query on" `date` >> ~/cronScripts/customerMilestoneLog
