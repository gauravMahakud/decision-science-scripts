SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/calendardateCustomerHubLog


DELETE calendardate_customer_hub_mapping_audit
WHERE calendardate >= CURRENT_DATE;

INSERT INTO calendardate_customer_hub_mapping_audit
SELECT DISTINCT calendardate,
       ca.customerid,
       ca.hubid
FROM customer_hub_audit ca
  JOIN (SELECT calendardate,
               customerid,
               MAX(VERSION)
        FROM calendar c
          JOIN customer_hub_audit ch ON c.calendardate >= DATE (ch.srclastupdatedtime)
        WHERE calendardate <= CURRENT_DATE+ 7
        AND   calendardate >= CURRENT_DATE
        GROUP BY 1,
                 2) m
    ON m.customerid = ca.customerid
   AND m.max = ca.version;

COMMIT;



EOF
echo "Ran summary script on" `date` >> ~/cronScripts/calendardateCustomerHubLog



