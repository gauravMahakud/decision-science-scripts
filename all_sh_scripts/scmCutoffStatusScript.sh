#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/scmCutoffStatusLog
insert into scm_cutoff_status_15_11
select
  convert_timezone('GMT', 'NEWZONE -5:30',getdate()) snapshot_time
  , date(GETDATE()+1) delivery_date
  , org_name
  , blt.order_item_id
  , blt.order_id
  , blt.listing_id
  , quantity
  , created_units
  , underprocessunitquantity
  , confirmedunitquantity
  , ready_to_ship_units
  , invoicenumbers
  , shipped_unit_quantity
  , return_requested_quantity
  , in_transit_units
  , delivered_units
  , cancelled_units
  , returned_units
from
  bolt_order_item_snapshot blt
  join customer_snapshot_ c on
    c.customerid = blt.buyer_id
left join
org_profile_snapshot org
on blt.seller_id = org.org_profile_id
where
  blt.src_created_time > date(GETDATE() -1) + interval '15:10:59'
  and blt.src_created_time <= date(GETDATE()) + interval '15:10:59';
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/scmCutoffStatusLog
