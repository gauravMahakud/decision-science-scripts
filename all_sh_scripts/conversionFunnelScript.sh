#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/conversionFunnelLog
delete from conversion_business_level 
where event_date=current_date;

insert INTO conversion_business_level
SELECT DATE (convert_timezone ('GMT','NEWZONE -5:30',ce.ets)) AS event_date,
       COUNT(DISTINCT (CASE WHEN etype != 'BG' AND en NOT LIKE 'PUSH_%' THEN c.businessid END)) AS came_to_app,
       COUNT(DISTINCT (CASE WHEN en = 'ON_SUBMIT_BROWSE' THEN c.businessid END)) AS clicked_on_browse,
       COUNT(DISTINCT (CASE WHEN en = 'ON_LOAD_PRODUCT_LIST_SCREEN' THEN c.businessid END)) AS opened_product_list,
       COUNT(DISTINCT (CASE WHEN en = 'ON_LOAD_PRODUCT_SCREEN' THEN c.businessid END)) AS opened_product_view,
       COUNT(DISTINCT (CASE WHEN en = 'ON_ADD_CART_ITEM' THEN c.businessid END)) AS added_to_cart,
       COUNT(DISTINCT (CASE WHEN en = 'ON_SUCCESS_ORDER' THEN c.businessid END)) AS ordered 
FROM customer_app_events ce
  JOIN customer_snapshot_ c ON c.customerid = ce.uid
  --JOIN store_name s ON s.customerid = ce.uid
WHERE DATE (convert_timezone('GMT','NEWZONE -5:30',ce.ets)) =current_date
AND   c.istestcustomer = 'false'
GROUP BY 1;

DELETE FROM conversion_business_level_pre_feb
WHERE event_date=current_date;

INSERT INTO conversion_business_level_pre_feb
SELECT DATE (convert_timezone ('GMT','NEWZONE -5:30',ce.ets)) AS event_date,
       COUNT(DISTINCT (CASE WHEN etype != 'BG' AND en NOT LIKE 'PUSH_%' THEN c.businessid END)) AS came_to_app,
       COUNT(DISTINCT (CASE WHEN en = 'ON_SUBMIT_BROWSE' THEN c.businessid END)) AS clicked_on_browse,
       COUNT(DISTINCT (CASE WHEN en = 'ON_LOAD_PRODUCT_LIST_SCREEN' THEN c.businessid END)) AS opened_product_list,
       COUNT(DISTINCT (CASE WHEN en = 'ON_LOAD_PRODUCT_SCREEN' THEN c.businessid END)) AS opened_product_view,
       COUNT(DISTINCT (CASE WHEN en = 'ON_ADD_CART_ITEM' THEN c.businessid END)) AS added_to_cart,
       COUNT(DISTINCT (CASE WHEN en = 'ON_SUCCESS_ORDER' THEN c.businessid END)) AS ordered 
FROM customer_app_events ce
  JOIN customer_snapshot_ c ON c.customerid = ce.uid
  JOIN business_snapshot s ON s.businessid = c.businessid
WHERE DATE (convert_timezone('GMT','NEWZONE -5:30',ce.ets))=current_date
AND   s.onboarddatenew < '2017-02-01'
AND   c.istestcustomer = 'false'
GROUP BY 1;

DELETE FROM conversion_business_level_feb_mar
WHERE event_date=current_date;

INSERT INTO conversion_business_level_feb_mar
SELECT DATE (convert_timezone ('GMT','NEWZONE -5:30',ce.ets)) AS event_date,
       COUNT(DISTINCT (CASE WHEN etype != 'BG' AND en NOT LIKE 'PUSH_%' THEN c.businessid END)) AS came_to_app,
       COUNT(DISTINCT (CASE WHEN en = 'ON_SUBMIT_BROWSE' THEN c.businessid END)) AS clicked_on_browse,
       COUNT(DISTINCT (CASE WHEN en = 'ON_LOAD_PRODUCT_LIST_SCREEN' THEN c.businessid END)) AS opened_product_list,
       COUNT(DISTINCT (CASE WHEN en = 'ON_LOAD_PRODUCT_SCREEN' THEN c.businessid END)) AS opened_product_view,
       COUNT(DISTINCT (CASE WHEN en = 'ON_ADD_CART_ITEM' THEN c.businessid END)) AS added_to_cart,
       COUNT(DISTINCT (CASE WHEN en = 'ON_SUCCESS_ORDER' THEN c.businessid END)) AS ordered 
FROM customer_app_events ce
  JOIN customer_snapshot_ c ON c.customerid = ce.uid
  JOIN business_snapshot s ON s.businessid = c.businessid
WHERE DATE (convert_timezone('GMT','NEWZONE -5:30',ce.ets))=current_date
AND   s.onboarddatenew < '2017-04-01' and s.onboarddatenew >= '2017-02-01'
AND   c.istestcustomer = 'false'
GROUP BY 1;

DELETE FROM conversion_business_level_post_mar
WHERE event_date=current_date;

INSERT INTO conversion_business_level_post_mar
SELECT DATE (convert_timezone ('GMT','NEWZONE -5:30',ce.ets)) AS event_date,
       COUNT(DISTINCT (CASE WHEN etype != 'BG' AND en NOT LIKE 'PUSH_%' THEN c.businessid END)) AS came_to_app,
       COUNT(DISTINCT (CASE WHEN en = 'ON_SUBMIT_BROWSE' THEN c.businessid END)) AS clicked_on_browse,
       COUNT(DISTINCT (CASE WHEN en = 'ON_LOAD_PRODUCT_LIST_SCREEN' THEN c.businessid END)) AS opened_product_list,
       COUNT(DISTINCT (CASE WHEN en = 'ON_LOAD_PRODUCT_SCREEN' THEN c.businessid END)) AS opened_product_view,
       COUNT(DISTINCT (CASE WHEN en = 'ON_ADD_CART_ITEM' THEN c.businessid END)) AS added_to_cart,
       COUNT(DISTINCT (CASE WHEN en = 'ON_SUCCESS_ORDER' THEN c.businessid END)) AS ordered 
FROM customer_app_events ce
  JOIN customer_snapshot_ c ON c.customerid = ce.uid
  JOIN business_snapshot s ON s.businessid = c.businessid
WHERE DATE (convert_timezone('GMT','NEWZONE -5:30',ce.ets)) = CURRENT_DATE
AND   s.onboarddatenew >= '2017-04-01'
AND   c.istestcustomer = 'false'
GROUP BY 1;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/conversionFunnelLog
