SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/notshippedfromfcLog



insert into daily_not_shipped_from_fc
select
  ord.order_item_id as mp_order_item_id
  , date(pr.updated_promise_time) as promise_date
  , p.jpin
  , p.title
  , ord.quantity as ordered_quantity
  , ord.order_item_amount
  , iimr.qty_to_transfer
  , iimr.qty_transferred 
  , iimr.qty_missing
  , iimr.qty_damaged
  , iimr.qty_delegated
  , si.quantity as rts_qty
  , fupick.display_name as pick_user
  , fupick.phone_number as pick_user_pn
  , fuput.display_name as put_user
  , fuput.phone_number as put_user_pn
  , f.name as facility_name
  , ti.transit_item_status
  , dispatch_agent
  , change_timestamp as dispatch_timestamp
,olid.trip_number
,iimr.qty_transferred  as qty_to_be_dispatched
,dispatched as qty_dispatched
,ord.order_item_amount/ord.quantity*(iimr.qty_transferred-dispatched) as gmv_lost
from
  bolt_order_item_v2_snapshot ord
  join listing_snapshot li on
    li.listing_id = ord.listing_id
    and ord.boltordertype = 'MARKETPLACE'
  join sellerproduct_snapshot sp on
    sp.sp_id = li.sp_id
  join product_snapshot_ p on
    p.jpin = sp.jpin
  join promise_snapshot pr on
    pr.promised_entity_id = ord.order_item_id
  join fulfilment_item_snapshot fump on
    fump.order_item_id = ord.order_item_id
  join fulfilment_item_snapshot fujw on
    fujw.order_item_id = fump.jit_purchase_order_item_id
  join dw_bradman.inventory_item ii on
    ii.creator_process_ref_id = fujw.fulfilment_item_id
    and ii.space_id not in (
    10002651
    , 100002651
  )
  and ii.state = 'FULFILMENT'
  join dw_bradman.space s on
    s.id = ii.space_id
  join facility f on
    f.id = s.facility_id
  join dw_bradman.inventory_item_movement_request iimr on
    iimr.inventory_item_id = ii.natural_id
    and iimr.status != 'CANCELLED'
  join dw_bradman.transfer_job_step_request tjsrpick on
    tjsrpick.inventory_movement_request_id = iimr.id
    and tjsrpick.transfer_job_step_type = 'PICK'
  join dw_bradman.transfer_job tjpick on
    tjpick.id = tjsrpick.transfer_job_id
  join dw_bradman.transfer_job_step_request tjsrput on
    tjsrput.inventory_movement_request_id = iimr.id
    and tjsrput.transfer_job_step_type = 'PUT'
  join dw_bradman.transfer_job tjput on
    tjput.id = tjsrput.transfer_job_id
  join dw_bradman.fc_user fupick on
    fupick.natural_id = tjpick.assigned_user_id
  join dw_bradman.fc_user fuput on
    fuput.natural_id = tjput.assigned_user_id
  join dw_shipment.shipment_item si on
    si.shipment_item_entity_id=ord.order_item_id
  join dw_shipment.transit_item ti on
    si.shipment_item_id = ti.shipment_item_id
  left join order_level_info_dispatch_new olid on
    olid.order_item_id = ord.order_item_id
  left join bulk_dispatch_new bd on
    bd.bulk_key = olid.bulk_key
where
  date(pr.updated_promise_time) = current_date
  and ti.deleted_at is null
  and si.deleted_at is null
  and ti.transit_item_status in (
    'CREATED'
    , 'RESHIP'
  )
;
commit;



\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/notshippedfromfcLog


