#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/dispatchMetadataLog
insert into dispatch_meta
select date(change_timestamp) as attempt_date, d.order_item_id, d.total_items_to_be_loaded, d.quantity_loaded, d.reason_code, null, d.user_email_id, d.change_timestamp, d._metadata__timestamp, null,d._metadata__uuid,d._metadata_total_rows,d._metadata_line_number, null, d._metadata_file_modified_ts
from
dispatch_app_backend  d 
join (select date(change_timestamp), max(_metadata_file_modified_ts) 
from dispatch_app_backend 
group by 1) f on date(d.change_timestamp)=f.date and f.max=d._metadata_file_modified_ts;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/dispatchMetadataLog
