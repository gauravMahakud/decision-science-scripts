HELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/businessAffinityChannelLog


drop table if exists business_channel_affinity_metadata;
commit;
select ppv.businessid, ppv.jpin, ppv.title, ppv.source_state, count(distinct ppv.date) as distinct_ppv_dates,
       count(distinct atc.date) as distinct_atc_dates,
       count(distinct ord.date) as distinct_ord_dates,
       max(ord.date) as last_order_date,
       max(ppv.date) as last_ppv_date,
       max(atc.date) as last_atc_date
       
       into business_channel_affinity_metadata
from
(
select *
from
(
select c.businessid, e.jpin, title, date(ets + interval '5:30:00'), source_state
from event_wise_target_source_new e
left join customer_snapshot_ c on c.customerid = uid
left join product_snapshot_ prod on prod.jpin = e.jpin
left join bolt_order_item_snapshot ord on ord.cartitemid = e.target_identifier
where e.state= 'Product Page View' and  c.istestcustomer is false --and date(ets + interval '5:30:00') >= current_date - 90
group by 1,2,3,4,5
)
) ppv 
left join
(
select *
from
(
select c.businessid, e.jpin, title, date(ets + interval '5:30:00'), source_state
from event_wise_target_source_new e
left join customer_snapshot_ c on c.customerid = uid
left join product_snapshot_ prod on prod.jpin = e.jpin
left join bolt_order_item_snapshot ord on ord.cartitemid = e.target_identifier
where e.state= 'Add to Cart' and c.istestcustomer is false --and date(ets+interval'5:30:00') >=  current_date - 90
group by 1,2,3,4,5
)
) atc on atc.businessid = ppv.businessid and atc.jpin = ppv.jpin and atc.date = ppv.date and atc.source_state = ppv.source_state
left join
(
select *
from
(
select c.businessid, e.jpin, title, date(ets + interval '5:30:00'), source_state
from event_wise_target_source_new e
left join customer_snapshot_ c on c.customerid = uid
left join product_snapshot_ prod on prod.jpin = e.jpin
left join bolt_order_item_snapshot ord on ord.cartitemid = e.target_identifier
where ord.order_item_id is not null and c.istestcustomer is false --and date(ets + interval '5:30:00') >= current_date - 90
group by 1,2,3,4,5
)
) ord on ord.businessid = ppv.businessid and ord.jpin = ppv.jpin and ord.date = ppv.date and ord.source_state = ppv.source_state
where ppv.businessid is not null 
group by 1,2,3,4
;
commit;






EOF
echo "Ran summary script on" `date` >> ~/businessAffinityChannelLog
