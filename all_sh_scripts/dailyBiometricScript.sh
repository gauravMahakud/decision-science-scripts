#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/dailyBiometricLog
DELETE
FROM daily_biometric_data
WHERE calendardate = CURRENT_DATE -1;

INSERT INTO daily_biometric_data
SELECT a.*,
       be.name,
       be.department,
       be.designation,
       be.company,
       be.phone,
       CASE
         WHEN check_out_time IS NULL OR check_in_time IS NULL THEN 'Yes'
         ELSE 'No'
       END AS single_punch,
       CASE
         WHEN be.enrollment_id IS NULL THEN 'Biometric Data Not Available'
       END AS biometric_data_flag,
       GREATEST(EXTRACT(epoch FROM check_out_time - check_in_time) / 3600 -9,0) AS overtime_9_hours,
       GREATEST(EXTRACT(epoch FROM check_out_time - check_in_time) / 3600 -12,0) AS overtime_12_hours,
       999952
FROM (SELECT calendardate,
             enrollnumber,
             same_day_first_check_in AS check_in_time,
             CASE
               WHEN same_day_first_check_in > same_day_last_check_out THEN next_day_first_check_out
               ELSE
                 CASE
                   WHEN same_day_last_check_out IS NULL THEN next_day_first_check_out
                   ELSE same_day_last_check_out
                 END 
             END AS check_out_time
      FROM (SELECT calendardate,
                   enrollnumber,
                   MIN(CASE WHEN punch_type = 'Check-In' AND calendardate = punchdate THEN punchtime END) AS same_day_first_check_in,
                   MAX(CASE WHEN punch_type = 'Check-Out' AND calendardate = punchdate THEN punchtime END) AS same_day_last_check_out,
                   MIN(CASE WHEN punch_type = 'Check-Out' AND calendardate = punchdate -1 THEN punchtime END) AS next_day_first_check_out
            FROM calendar c
              JOIN (SELECT DATE (a.punchdate) AS punchdate,
                           a.enrollnumber,
                           a.punchtime AS punchtime,
                           e.name,
                           e.department,
                           e.designation,
                           CASE
                             WHEN deviceid = 'OIN7033017031200139' THEN 'Check-Out'
                             WHEN deviceid = 'OIN7013017010500264' THEN 'Check-In'
                           END AS punch_type
                    FROM employee_attendance a
                      LEFT JOIN biometric_employee e ON e.enrollment_id = a.enrollnumber
                    WHERE a.deviceid IN ('OIN7013017010500264','OIN7033017031200139')
                    AND   DATE (punchdate) >= CURRENT_DATE -1) a
                ON CASE WHEN
            --punch_type='Check-In'  or date_part('hour', TO_CHAR(punchtime, 'YYYY-MM-DD HH24:MI:SS' )::timestamp without time zone) >= 11  
            1 = 1 THEN a.punchdate END = c.calendardate
                OR (c.calendardate + 1 = a.punchdate
               AND DATE_PART ('hour',TO_CHAR (punchtime,'YYYY-MM-DD HH24:MI:SS')::TIMESTAMP WITHOUT TIME ZONE) < 11
               AND punch_type = 'Check-Out')
            WHERE calendardate = CURRENT_DATE -1
            GROUP BY 1,
                     2)
      WHERE (same_day_first_check_in IS NULL AND DATE_PART('hour',TO_CHAR((CASE WHEN same_day_first_check_in > same_day_last_check_out THEN next_day_first_check_out ELSE CASE WHEN same_day_last_check_out IS NULL THEN next_day_first_check_out ELSE same_day_last_check_out END END),'YYYY-MM-DD HH24:MI:SS')::TIMESTAMP WITHOUT TIME ZONE) >= 11)
      OR    same_day_first_check_in IS NOT NULL) a
  LEFT JOIN biometric_employee be ON be.enrollment_id = a.enrollnumber;

INSERT INTO daily_biometric_data
SELECT a.*,
       be.name,
       be.department,
       be.designation,
       be.company,
       be.phone,
       CASE
         WHEN check_out_time IS NULL OR check_in_time IS NULL THEN 'Yes'
         ELSE 'No'
       END AS single_punch,
       CASE
         WHEN be.enrollment_id IS NULL THEN 'Biometric Data Not Available'
       END AS biometric_data_flag,
       GREATEST(EXTRACT(epoch FROM check_out_time - check_in_time) / 3600 -9,0) AS overtime_9_hours,
       GREATEST(EXTRACT(epoch FROM check_out_time - check_in_time) / 3600 -12,0) AS overtime_12_hours,
       100001 AS facility_id
FROM (SELECT calendardate,
             enrollnumber,
             same_day_first_check_in AS check_in_time,
             CASE
               WHEN same_day_first_check_in > same_day_last_check_out THEN next_day_first_check_out
               ELSE
                 CASE
                   WHEN same_day_last_check_out IS NULL THEN next_day_first_check_out
                   ELSE same_day_last_check_out
                 END 
             END AS check_out_time
      FROM (SELECT calendardate,
                   enrollnumber,
                   MIN(CASE WHEN punch_type = 'Check-In' AND calendardate = punchdate THEN punchtime END) AS same_day_first_check_in,
                   MAX(CASE WHEN punch_type = 'Check-Out' AND calendardate = punchdate THEN punchtime END) AS same_day_last_check_out,
                   MIN(CASE WHEN punch_type = 'Check-Out' AND calendardate = punchdate -1 THEN punchtime END) AS next_day_first_check_out
            FROM calendar c
              JOIN (SELECT DATE (a.punchdate) AS punchdate,
                           a.enrollnumber,
                           a.punchtime AS punchtime,
                           e.name,
                           e.department,
                           e.designation,
                           CASE
                             WHEN deviceid = 'OIN7043017031200295' THEN 'Check-Out'
                             WHEN deviceid = 'OIN7033017031200062' THEN 'Check-In'
                           END AS punch_type
                    FROM employee_attendance a
                      LEFT JOIN biometric_employee e ON e.enrollment_id = a.enrollnumber
                    WHERE a.deviceid IN ('OIN7033017031200062','OIN7043017031200295')
                    AND   DATE (punchdate) >= CURRENT_DATE -1) a
                ON a.punchdate = c.calendardate
                OR (c.calendardate + 1 = a.punchdate
               AND DATE_PART ('hour',TO_CHAR (punchtime,'YYYY-MM-DD HH24:MI:SS')::TIMESTAMP WITHOUT TIME ZONE) < 11
               AND punch_type = 'Check-Out')
            WHERE calendardate = CURRENT_DATE -1
            GROUP BY 1,
                     2)
      WHERE (same_day_first_check_in IS NULL AND DATE_PART('hour',TO_CHAR((CASE WHEN same_day_first_check_in > same_day_last_check_out THEN next_day_first_check_out ELSE CASE WHEN same_day_last_check_out IS NULL THEN next_day_first_check_out ELSE same_day_last_check_out END END),'YYYY-MM-DD HH24:MI:SS')::TIMESTAMP WITHOUT TIME ZONE) >= 11)
      OR    same_day_first_check_in IS NOT NULL) a
  LEFT JOIN biometric_employee be ON be.enrollment_id = a.enrollnumber;
\o
COMMIT;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/dailyBiometricLog
