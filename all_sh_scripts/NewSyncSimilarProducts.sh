PYTHON_ENV='pythonanaconda3'

cd /home/ec2-user/cronScripts/boughtTogether/

#source `/home/ec2-user/anaconda3/envs/pythonanaconda3/bin/activate`
source activate $PYTHON_ENV
python ./por.py
source deactivate $PYTHON_ENV

#source `/home/ec2-user/anaconda3/envs/pythonanaconda3/bin/deactivate`

FILE_TO_UPLOAD=`ls -t1 |  head -n 1`
echo $FILE_TO_UPLOAD

BUCKET='jt-boughttogether';
aws s3 cp ./$FILE_TO_UPLOAD s3://$BUCKET/ --region ap-south-1

KEY=`aws s3 ls $BUCKET --recursive --region ap-south-1 | sort | tail -n 1 | awk '{print $4}'`
echo $KEY


aws s3 cp s3://$BUCKET/$KEY ./ --region ap-south-1


curl --request POST \
  --url http://rambo.jumbotail.com/api/similar-products/ingest \
  --header 'authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJzYWphbEBqdW1ib3RhaWwuY29tIiwiaWF0IjoxNDkxMjAxMDkzLCJzdWIiOiJzYWphbEBqdW1ib3RhaWwuY29tIiwiaXNzIjoiSnVtYm90YWlsIn0.FVJdB-_0mYYJUE7GU00TBiKQ7CWB7gKaUL8cQhoPCdE' \
  --header 'cache-control: no-cache' \
  --header 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
  --header 'postman-token: 0a427629-0c44-6acb-0880-4c30394fb53a' \
  --form userName=Hercules \
  --form isUpdateExisting=false \
  --form file=@/home/ec2-user/cronScripts/boughtTogether/$KEY

#rm $KEY

echo "Ran the boughtTogether query on" `date` >> ~/cronScripts/boughtTogetherLog
