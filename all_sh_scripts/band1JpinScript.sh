#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;         
\o | cat >> ~/cronScripts/band1JpinLog
insert into daily_band1jpin
select current_date+1, jpin, reason 
from (select distinct jpin,reason
	from daily_band1jpin
	where date=current_date);
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/band1JpinLog
