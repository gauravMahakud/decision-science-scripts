#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/hourlyOOSJpins


insert into hourly_oos_jpins
SELECT getdate() +INTERVAL '5:30' AS ts,
       category_name,
       listagg(jpin,',') within group(ORDER BY jpin ASC) AS jpin_list
FROM (SELECT category_name,
             jpin,
             title,
             COUNT(DISTINCT (listing_id)) AS listings,
             COUNT(DISTINCT (CASE WHEN moq > current_inventory THEN listing_id END)) AS oos_listings
      FROM (SELECT DISTINCT category_name,
                   sp.jpin,
                   p.title,
                   li.listing_id,
                   li.min_order_quantity AS moq,
                   sum(case when mpc.mp_listing_id is null then 0 else available_for_sale end) AS current_inventory
            FROM listing_inventory_snapshot ll
              JOIN listing_snapshot li ON li.listing_id = ll.listing_id
              JOIN sellerproduct_snapshot sp ON sp.sp_id = li.sp_id
              JOIN product_snapshot_ p ON p.jpin = sp.jpin
              JOIN category cat ON cat.pvid = p.pvid
left join listing_inventory_details mpc on mpc.mp_listing_id = li.listing_id and mpc.process_idx = 1
            WHERE p.catalogstatus = 'Active'
            AND   li.status = 'ACTIVE'
            AND   li.channel_id = 'ORGPROF-1304473229'
            group by 1,2,3,4,5)
      GROUP BY 1,
               2,
               3)
WHERE listings = oos_listings
GROUP BY 1,2;

commit;



EOF
echo "Ran summary script on" `date` >> ~/cronScripts/hourlyOOSJpins


