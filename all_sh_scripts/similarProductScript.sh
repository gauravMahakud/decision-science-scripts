#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/similarProductLog
--for fmcg and consumer packs
DROP TABLE if exists all_products_relation;

WITH product_details AS
(
  SELECT p.jpin,
         p.title,
         p.pvid,
         p.productlineid,
         p.brandid,
         b.manufacturerid,
         ca.distributed,
         sc.deadweight,
         sc.volumetricweight,
         mrp.max AS mrp,
         ui.entityid AS category_id,
         ca.category_name
  FROM product_snapshot_ p
    LEFT JOIN uinode_snapshot_ ui ON ui.childnodeid = p.pvid
    LEFT JOIN category ca ON p.pvid = ca.pvid
    LEFT JOIN supplychainattributes_snapshot_ sc ON sc.jpin = p.jpin
    LEFT JOIN brand_snapshot_ b ON b.brandid = p.brandid
    LEFT JOIN (SELECT sp.jpin,
                      MAX(l.mrp)
               FROM listing_snapshot l
                 JOIN sellerproduct_snapshot sp ON sp.sp_id = l.sp_id
                 JOIN product_snapshot_ p ON p.jpin = sp.jpin
               WHERE l.status = 'ACTIVE'
               AND   l.channel_id = 'ORGPROF-1304473229'
               GROUP BY 1
               HAVING COUNT(DISTINCT (l.mrp)) = 1
                  AND MAX(l.mrp) <= 20) mrp ON mrp.jpin = p.jpin
)
SELECT *INTO all_products_relation
FROM ((SELECT DISTINCT p1.jpin,
              'Other products in PL' AS reason,
              1 AS priority,
              p2.jpin related_jpin
       FROM product_details p1
         JOIN product_details p2
                ON p1.productlineid = p2.productlineid
               AND p1.jpin != p2.jpin
       WHERE p1.distributed IS TRUE
       OR    p1.deadweight <= 5)
       UNION ALL
       (SELECT p1.jpin,
              'Same PV and Same Brand/Manufacturer and Same MRP or Grammage',
              2,
              p2.jpin
       FROM product_details p1
         JOIN product_details p2
                ON p1.pvid = p2.pvid
               AND (p1.brandid = p2.brandid
                OR p1.manufacturerid = p2.manufacturerid)
               AND (ABS (p1.deadweight - p2.deadweight) < 0.01*p1.deadweight
                OR ABS (p1.mrp - p2.mrp) < 0.01*p1.mrp)
               AND p1.jpin != p2.jpin
       WHERE p1.distributed IS TRUE
       OR    p1.deadweight <= 5)
       UNION ALL
       (SELECT p1.jpin,
              'Same PV and Other Brands Less than 10 pct  variance in MRP or Grammage',
              3,
              p2.jpin
       FROM product_details p1
         JOIN product_details p2
                ON p1.pvid = p2.pvid
               AND p1.manufacturerid != p2.manufacturerid
               AND (ABS (p1.deadweight - p2.deadweight) < 0.01*p1.deadweight
                OR ABS (p1.mrp - p2.mrp) < 0.01*p1.mrp)
               AND p1.jpin != p2.jpin
       WHERE p1.distributed IS TRUE
       OR    p1.deadweight <= 5)
       UNION ALL
       (SELECT p1.jpin,
              'Same PV and Same Brand or Manufacturer',
              4,
              p2.jpin
       FROM product_details p1
         JOIN product_details p2
                ON p1.pvid = p2.pvid
               AND p1.manufacturerid = p2.manufacturerid
               AND p1.jpin != p2.jpin
       WHERE p1.distributed IS TRUE
       OR    p1.deadweight <= 5)
       UNION ALL
       (SELECT p1.jpin,
              'Other PVs and Same category and Same Brand or Manufacturer',
              5,
              p2.jpin
       FROM product_details p1
         JOIN product_details p2
                ON p1.pvid != p2.pvid
               AND p1.category_name = p2.category_name
               AND p1.manufacturerid = p2.manufacturerid
               AND p1.jpin != p2.jpin
       WHERE p1.distributed IS TRUE
       OR    p1.deadweight <= 5));

INSERT INTO all_products_relation 

WITH product_details_
AS
(SELECT p.jpin,
       p.title,
       p.pvid,
       p.productlineid,
       p.brandid,
       b.manufacturerid,
       ca.distributed,
       sc.deadweight,
       bp.browsenode,
       sp.sp / sc.deadweight AS sp,
       ui.category_name AS cat
FROM product_snapshot_ p
  LEFT JOIN category ui ON ui.pvid = p.pvid
  LEFT JOIN category ca ON p.pvid = ca.pvid
  LEFT JOIN supplychainattributes_snapshot_ sc ON sc.jpin = p.jpin
  LEFT JOIN brand_snapshot_ b ON b.brandid = p.brandid
  LEFT JOIN temp_product_bp_mapping bp ON bp.jpin = p.jpin
  LEFT JOIN (SELECT sp.jpin,
                    AVG(selling_price) AS sp
             FROM listing_snapshot l
               JOIN sellerproduct_snapshot sp ON sp.sp_id = l.sp_id
               JOIN product_snapshot_ p ON p.jpin = sp.jpin
             WHERE l.status = 'ACTIVE'
             AND   l.channel_id = 'ORGPROF-1304473229'
             AND   selling_price > 0
             GROUP BY 1) sp ON sp.jpin = p.jpin
WHERE ca.distributed IS FALSE
AND   sc.deadweight > 5),product_details1 as (SELECT p.jpin,
       p.title,
       p.pvid,
       p.productlineid,
       p.brandid,
       b.manufacturerid,
       ca.distributed,
       sc.deadweight,
       bp.browsenode,
       sp.sp / sc.deadweight AS sp,
       ui.category_name AS cat
FROM product_snapshot_ p
  LEFT JOIN category ui ON ui.pvid = p.pvid
  LEFT JOIN category ca ON p.pvid = ca.pvid
  LEFT JOIN supplychainattributes_snapshot_ sc ON sc.jpin = p.jpin
  LEFT JOIN brand_snapshot_ b ON b.brandid = p.brandid
  LEFT JOIN temp_product_bp_mapping bp ON bp.jpin = p.jpin
  LEFT JOIN (SELECT sp.jpin,
                    AVG(selling_price) AS sp
             FROM listing_snapshot l
               JOIN sellerproduct_snapshot sp ON sp.sp_id = l.sp_id
               JOIN product_snapshot_ p ON p.jpin = sp.jpin
             WHERE l.status = 'ACTIVE'
             AND   l.channel_id = 'ORGPROF-1304473229'
             AND   selling_price > 0
             GROUP BY 1) sp ON sp.jpin = p.jpin
WHERE ca.distributed IS FALSE)

SELECT *
FROM (SELECT DISTINCT p1.jpin,
             'Same PV  and Browse Pivot and Same grammage' AS reason,
             ROW_NUMBER() OVER (PARTITION BY p1.jpin ORDER BY ABS(p1.sp - p2.sp)) AS priority,
             p2.jpin related_jpin
      FROM product_details_ p1
        JOIN product_details_ p2
          ON p1.pvid = p2.pvid
         AND (p1.browsenode = p2.browsenode or p1.browsenode is null)
         AND p1.jpin != p2.jpin
         AND p1.deadweight = p2.deadweight
      UNION ALL
      (SELECT DISTINCT p1.jpin,
             'Other PV and Same BP and Same category and same grammage' AS reason,
             ROW_NUMBER() OVER (PARTITION BY p1.jpin ORDER BY ABS(p1.sp - p2.sp)) +1000 AS priority,
             p2.jpin related_jpin
      FROM product_details_ p1
        JOIN product_details_ p2
          ON p1.pvid != p2.pvid
         AND p1.browsenode = p2.browsenode
         AND p1.jpin != p2.jpin
         AND p1.deadweight = p2.deadweight)
      UNION ALL
      (SELECT DISTINCT p1.jpin,
             'Other BP and Same PV and same grammage' AS reason,
             ROW_NUMBER() OVER (PARTITION BY p1.jpin ORDER BY ABS(p1.sp - p2.sp)) +2000 AS priority,
             p2.jpin related_jpin
      FROM product_details_ p1
        JOIN product_details_ p2
          ON p1.pvid = p2.pvid
         AND p1.browsenode != p2.browsenode
         AND p1.jpin != p2.jpin
         AND p1.deadweight = p2.deadweight)
      UNION ALL
      (SELECT DISTINCT p1.jpin,
             'Same PV and Browse Pivot and different grammage' AS reason,
             ROW_NUMBER() OVER (PARTITION BY p1.jpin ORDER BY ABS(p1.sp - p2.sp)) +3000 AS priority,
             p2.jpin related_jpin
      FROM product_details_ p1
        JOIN product_details_ p2
          ON p1.pvid = p2.pvid
         AND (p1.browsenode = p2.browsenode or p1.browsenode is null)
         AND p1.jpin != p2.jpin
         AND p1.deadweight != p2.deadweight)
      UNION ALL
      (SELECT DISTINCT p1.jpin,
             'Other PV and Same BP and Same category and different grammage' AS reason,
             ROW_NUMBER() OVER (PARTITION BY p1.jpin ORDER BY ABS(p1.sp - p2.sp)) +4000 AS priority,
             p2.jpin related_jpin
      FROM product_details_ p1
        JOIN product_details_ p2
          ON p1.pvid != p2.pvid
         AND p1.browsenode = p2.browsenode
         AND p1.jpin != p2.jpin
         AND p1.deadweight != p2.deadweight)
      UNION ALL
      (SELECT DISTINCT p1.jpin,
             'Other BP and Same PV and diff grammage' AS reason,
             ROW_NUMBER() OVER (PARTITION BY p1.jpin ORDER BY ABS(p1.sp - p2.sp)) +5000 AS priority,
             p2.jpin related_jpin
      FROM product_details_ p1
        JOIN product_details_ p2
          ON p1.pvid = p2.pvid
         AND p1.browsenode != p2.browsenode
         AND p1.jpin != p2.jpin
         AND p1.deadweight != p2.deadweight)
      UNION ALL
      (SELECT DISTINCT p1.jpin,
             'Other PV and Same category and same grammage' AS reason,
             6000 AS priority,
             p2.jpin related_jpin
      FROM product_details_ p1
        JOIN product_details_ p2
          ON p1.pvid != p2.pvid
         AND p1.cat = p2.cat
         AND p1.jpin != p2.jpin
         AND p1.deadweight = p2.deadweight)
      UNION ALL
      (SELECT DISTINCT p1.jpin,
             'Other PV and Same category and different grammage' AS reason,
             7000 AS priority,
             p2.jpin related_jpin
      FROM product_details_ p1
        JOIN product_details_ p2
          ON p1.pvid != p2.pvid
         AND p1.cat = p2.cat
         AND p1.jpin != p2.jpin
         AND p1.deadweight != p2.deadweight)
         UNION ALL
      (SELECT DISTINCT p1.jpin,
             'Same PV and Browse Pivot and smaller grammage' AS reason,
             ROW_NUMBER() OVER (PARTITION BY p1.jpin ORDER BY ABS(p1.sp - p2.sp)) +8000 AS priority,
             p2.jpin related_jpin
      FROM product_details_ p1
        JOIN product_details1 p2
          ON p1.pvid = p2.pvid
         AND (p1.browsenode = p2.browsenode or p1.browsenode is null)
         AND p1.jpin != p2.jpin
         AND p1.deadweight != p2.deadweight)
      UNION ALL
      (SELECT DISTINCT p1.jpin,
             'Other PV and Same BP and Same category and smaller grammage' AS reason,
             ROW_NUMBER() OVER (PARTITION BY p1.jpin ORDER BY ABS(p1.sp - p2.sp)) +9000 AS priority,
             p2.jpin related_jpin
      FROM product_details_ p1
        JOIN product_details1 p2
          ON p1.pvid != p2.pvid
         AND p1.browsenode = p2.browsenode
         AND p1.jpin != p2.jpin
         AND p1.deadweight != p2.deadweight)
      UNION ALL
      (SELECT DISTINCT p1.jpin,
             'Other BP and Same PV and smaller grammage' AS reason,
             ROW_NUMBER() OVER (PARTITION BY p1.jpin ORDER BY ABS(p1.sp - p2.sp)) +10000 AS priority,
             p2.jpin related_jpin
      FROM product_details_ p1
        JOIN product_details1 p2
          ON p1.pvid = p2.pvid
         AND p1.browsenode != p2.browsenode
         AND p1.jpin != p2.jpin
         AND p1.deadweight != p2.deadweight));
\o
grant all on all_products_relation to yash;
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/similarProductLog
