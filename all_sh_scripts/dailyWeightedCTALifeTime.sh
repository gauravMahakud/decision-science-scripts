#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/dailyWeightedCTALog
insert into daily_weighted_cta_lifetime
WITH temp1_ AS
(
SELECT DISTINCT cm.bid,
       DATE AS visit_date,
       onboarddatenew,
       RANK() OVER (PARTITION BY bid ORDER BY bid,visit_date) AS rank_r
FROM daily_cta dcta
  JOIN customer_snapshot_ c ON c.customerid = dcta.uid
  JOIN customer_milestone_ cm ON cm.bid = c.businessid 
       where   (c.status = 'ACTIVE' OR c.status = 'ONHOLD') and c.istestcustomer is false
and date <= current_date and date >='2016-04-01'
),
temp2 AS
(
  SELECT a.bid,
         b.visit_date AS date_1,
         a.visit_date AS date_2,
         (a.visit_date - b.visit_date) AS difference,
         RANK() OVER (PARTITION BY a.bid ORDER BY b.visit_date DESC) AS rnk,
         ((CURRENT_DATE+1)- a.onboarddatenew) AS days_in_system,
         (2 *LN(2)*1.00 /((CURRENT_DATE+1)- a.onboarddatenew)) AS alpha,
         (2.73) ^((-2*LN(2)*1.00 /((CURRENT_DATE+1)- a.onboarddatenew))*(RANK() OVER (PARTITION BY a.bid ORDER BY b.visit_date DESC))) AS expalphat,
         (((0.5) ^(RANK() OVER (PARTITION BY a.bid ORDER BY b.visit_date DESC)))*1.00)*(a.visit_date - b.visit_date) AS weights_diff
  FROM temp1_ a
    LEFT JOIN temp1_ b
           ON a.bid = b.bid
          AND a.rank_r = (b.rank_r +1)
  WHERE a.visit_date - b.visit_date IS NOT NULL
  GROUP BY 1,
           2,
           3,
           a.onboarddatenew
  ORDER BY 1,
           2
),
temp3 AS
(
  SELECT bid,
         date_1,
         date_2,
         difference,
         rnk,
         expalphat,
         (SUM(expalphat) OVER (PARTITION BY bid)) AS total_sum,
         (expalphat*1.00 /(SUM(expalphat) OVER (PARTITION BY bid))) AS exp_weights,
         (expalphat*1.00 /(SUM(expalphat) OVER (PARTITION BY bid)))*difference AS weighted_days
  FROM temp2
  ORDER BY 1,
           2
)
SELECT (CURRENT_DATE+1) AS date_calc,
       a.bid as businessid,
       SUM(weighted_days) AS weighted_days_cta 
FROM temp3 a
GROUP BY 1,
         2;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/dailyWeightedCTALog
