#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/oosSalesLog
DELETE
FROM temp_oos_last_selling_price
WHERE calendardate = CURRENT_DATE;

INSERT INTO temp_oos_last_selling_price
SELECT calendardate,
       jpin,
       MIN(order_item_amount / ord.quantity)
FROM calendar ca
  JOIN bolt_order_item_snapshot ord
    ON ca.calendardate - DATE (ord.src_created_time) > 0
   AND ca.calendardate - DATE (ord.src_created_time) < 120
  JOIN customer_snapshot_ c ON c.customerid = ord.buyer_id
  JOIN listing_snapshot li ON li.listing_id = ord.listing_id
  JOIN sellerproduct_snapshot sp ON sp.sp_id = li.sp_id
WHERE c.istestcustomer IS FALSE
AND   order_item_amount > 0
AND   (c.status = 'ACTIVE')
AND   ca.calendardate = CURRENT_DATE
GROUP BY 1,
         2;

DELETE
FROM temp_oos_customer_wise_avg_quantity
WHERE calendardate = CURRENT_DATE;

INSERT INTO temp_oos_customer_wise_avg_quantity
SELECT calendardate,
       customerid,
       jpin,
       AVG(quantity)
FROM (SELECT calendardate,
             c.customerid,
             jpin,
             DENSE_RANK() OVER (PARTITION BY calendardate,customerid,jpin ORDER BY DATE (ord.src_created_time) DESC) AS ranking,
             ord.quantity
      FROM calendar ca
        JOIN bolt_order_item_snapshot ord
          ON ca.calendardate - DATE (ord.src_created_time) <= 120
         AND ca.calendardate - DATE (ord.src_created_time) >= 0
        JOIN customer_snapshot_ c ON c.customerid = ord.buyer_id
        JOIN listing_snapshot li ON li.listing_id = ord.listing_id
        JOIN sellerproduct_snapshot sp ON sp.sp_id = li.sp_id
      WHERE c.istestcustomer IS FALSE
      AND   order_item_amount > 0
      AND   (c.status = 'ACTIVE')
      AND   ca.calendardate = CURRENT_DATE)
WHERE ranking <= 7
GROUP BY 1,
         2,
         3;

DELETE
FROM temp_oos_avg_quantity
WHERE calendardate = CURRENT_DATE;

INSERT INTO temp_oos_avg_quantity
SELECT calendardate,
       jpin,
       AVG(quantity)
FROM (SELECT calendardate,
             jpin,
             DENSE_RANK() OVER (PARTITION BY calendardate,jpin ORDER BY DATE (ord.src_created_time) DESC) AS ranking,
             ord.quantity
      FROM calendar ca
        JOIN bolt_order_item_snapshot ord
          ON ca.calendardate - DATE (ord.src_created_time) <= 120
         AND ca.calendardate - DATE (ord.src_created_time) >= 0
        JOIN customer_snapshot_ c ON c.customerid = ord.buyer_id
        JOIN listing_snapshot li ON li.listing_id = ord.listing_id
        JOIN sellerproduct_snapshot sp ON sp.sp_id = li.sp_id
      WHERE c.istestcustomer IS FALSE
      AND   order_item_amount > 0
      AND   (c.status = 'ACTIVE')
      AND   ca.calendardate = CURRENT_DATE)
WHERE ranking <= 7
GROUP BY 1,
         2;


DELETE
FROM all_view_events_test
WHERE event_date = CURRENT_DATE;

INSERT INTO all_view_events_test
SELECT DISTINCT event_date,
       jpin,
       CASE
         WHEN state LIKE '%true%' THEN 'true'
         ELSE 'false'
       END AS state,
       uid,
       views,
       MAX(ets)
FROM (SELECT DATE (ets +INTERVAL '5:30:00') AS event_date,
             regexp_substr(TRIM(SPLIT_PART(B.pidarr,',',CONVERT(INTEGER,num.number +1))),'J[^"]*') AS JPIN,
             regexp_substr(TRIM(SPLIT_PART(B.oosarr,',',CONVERT(INTEGER,num.number +1))),'[^"]*') AS state,
             uid,
             'impression' AS views,
             ets +INTERVAL '5:30:00' AS ets
      FROM numbers num
        INNER JOIN customer_app_events B ON num.number <= REGEXP_COUNT (B.pidarr,',')
      WHERE en = 'ON_IMPRESSIONS_PRODUCT_LIST_SCREEN'
      AND   DATE (ets +INTERVAL '5:30:00') = CURRENT_DATE)
GROUP BY 1,
         2,
         3,
         4,
         5;

INSERT INTO all_view_events_test
SELECT DISTINCT DATE (ets +INTERVAL '5:30:00'),
       pid,
       CASE
         WHEN iots IS TRUE THEN 'true'
         ELSE 'false'
       END 
,
       uid,
       'ppv',
       MAX(ets +INTERVAL '5:30:00')
FROM customer_app_events
WHERE en = 'ON_LOAD_PRODUCT_SCREEN'
AND   DATE (ets +INTERVAL '5:30:00') =current_date
GROUP BY 1,
         2,
         3,
         4,
         5;

COMMIT;

DELETE
FROM all_orders
WHERE DATE = CURRENT_DATE;

INSERT INTO all_orders
SELECT DATE (ord.src_created_time),
       jpin,
       customerid,
       SUM(ord.quantity),
       MAX(ord.src_created_time) AS last_order_time
FROM bolt_order_item_snapshot ord
  JOIN customer_snapshot_ c ON c.customerid = ord.buyer_id
  JOIN listing_snapshot li ON li.listing_id = ord.listing_id
  JOIN sellerproduct_snapshot sp ON sp.sp_id = li.sp_id
WHERE c.istestcustomer IS FALSE
AND   order_item_amount > 0
AND   (c.status = 'ACTIVE')
AND   DATE (ord.src_created_time) = CURRENT_DATE
GROUP BY 1,
         2,
         3;

COMMIT;

DELETE
FROM temp_oos_customer_avg_conversion
WHERE calendardate = CURRENT_DATE ;

INSERT INTO temp_oos_customer_avg_conversion
SELECT calendardate,
       uid,
       jpin,
       views,
       COUNT(DISTINCT (DATE))*1.00 / COUNT(DISTINCT (event_date)) AS conv
FROM (SELECT DISTINCT calendardate,
             uid,
             e.jpin,
             views,
             o.date,
             e.event_date,
             DENSE_RANK() OVER (PARTITION BY calendardate,uid,e.jpin,views ORDER BY e.event_date DESC) AS rankings
      FROM calendar c
        JOIN all_view_events_test e
          ON c.calendardate - e.event_date <= 120
         AND c.calendardate - e.event_date >= 0
        LEFT JOIN all_orders o
               ON e.event_date = o.date
              AND e.uid = o.customerid
              AND e.jpin = o.jpin
      WHERE state = 'false'
      AND   calendardate = CURRENT_DATE)
WHERE rankings <= 7
GROUP BY 1,
         2,
         3,
         4;

COMMIT;

DELETE
FROM temp_oos_avg_conversion
WHERE calendardate = CURRENT_DATE;

INSERT INTO temp_oos_avg_conversion
SELECT calendardate,
       jpin,
       views,
       AVG(conv)
FROM (SELECT DISTINCT calendardate,
             jpin,
             views,
             conv,
             DENSE_RANK() OVER (PARTITION BY calendardate,jpin,views ORDER BY event_date DESC) AS rankings
      FROM calendar c
        JOIN (SELECT e.event_date,
                     e.jpin,
                     e.views,
                     COUNT(DISTINCT (o.customerid))*1.00 / COUNT(DISTINCT (e.uid)) AS conv
              FROM all_view_events_test e
                LEFT JOIN all_orders o
                       ON e.event_date = o.date
                      AND e.uid = o.customerid
                      AND e.jpin = o.jpin
              WHERE state LIKE '%false%'
              GROUP BY 1,
                       2,
                       3
              HAVING COUNT(DISTINCT (e.uid)) > 0) co
          ON c.calendardate - co.event_date <= 120
         AND c.calendardate - co.event_date >= 0
      WHERE calendardate = CURRENT_DATE )
WHERE rankings <= 7
GROUP BY 1,
         2,
         3;

COMMIT;

DELETE
FROM oos_lost_revenue
WHERE DATE = CURRENT_DATE;

INSERT INTO oos_lost_revenue
SELECT event_date AS DATE,
       jpin,
       title,
       uid,
       SUM(CASE WHEN views = 'impression' THEN SUM END) AS revenue_lost_impression_only,
       SUM(CASE WHEN views = 'ppv' THEN SUM END) AS revenue_lost_ppv_only,
       SUM(SUM) AS revenue_lost_impression,
       SUM(quantity) AS total_lost_sales_quantity
FROM (SELECT *,
             ROW_NUMBER() OVER (PARTITION BY event_date,jpin,uid ORDER BY views DESC) AS flag
      FROM (SELECT e.event_date,
                   e.jpin,
                   p.title,
                   e.uid,
                   e.views,
                   SUM(CASE WHEN sp.min IS NULL THEN 0 ELSE sp.min END*CASE WHEN cq.avg IS NULL THEN avgq.avg ELSE cq.avg END*CASE WHEN cc.conv IS NULL THEN avgc.avg ELSE cc.conv END),
                   SUM(CASE WHEN cq.avg IS NULL THEN avgq.avg ELSE cq.avg END*CASE WHEN cc.conv IS NULL THEN avgc.avg ELSE cc.conv END) AS quantity
            FROM all_view_events_test e
              LEFT JOIN all_orders o
                     ON e.uid = o.customerid
                    AND e.jpin = o.jpin
                    AND e.event_date = o.date
              JOIN customer_snapshot_ c ON c.customerid = e.uid
              JOIN product_snapshot_ p ON p.jpin = e.jpin
              LEFT JOIN temp_oos_last_selling_price sp
                     ON e.event_date = sp.calendardate
                    AND e.jpin = sp.jpin
              LEFT JOIN temp_oos_customer_wise_avg_quantity cq
                     ON cq.calendardate = e.event_date
                    AND cq.customerid = e.uid
                    AND cq.jpin = e.jpin
              LEFT JOIN temp_oos_avg_quantity avgq
                     ON e.event_date = avgq.calendardate
                    AND e.jpin = avgq.jpin
              LEFT JOIN temp_oos_customer_avg_conversion cc
                     ON cc.calendardate = e.event_date
                    AND cc.uid = e.uid
                    AND cc.jpin = e.jpin
                    AND cc.views = e.views
              LEFT JOIN temp_oos_avg_conversion avgc
                     ON avgc.calendardate = e.event_date
                    AND avgc.jpin = e.jpin
                    AND e.views = avgc.views
            WHERE e.state = 'true'
            AND   c.istestcustomer IS FALSE
            AND   c.status = 'ACTIVE'
            AND   e.event_date =current_date
            AND   (o.last_order_time IS NULL OR e.view_time > o.last_order_time)
            GROUP BY 1,
                     2,
                     3,
                     4,
                     5))
WHERE flag = 1
GROUP BY 1,
         2,
         3,
         4;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/oosSalesLog
