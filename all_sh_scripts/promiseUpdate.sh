SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/promiseupdatelog

INSERT INTO daily_lm_capacity
SELECT calendardate AS promise_date,
       hubid,
       metric,
       capacity,
       buffer_capacity
FROM calendar
  JOIN last_mile_capacity_weekday lm ON lm.dow = DATE_PART ('weekday',calendardate)
WHERE calendardate <= CURRENT_DATE +15
AND   calendardate >= CURRENT_DATE
AND   calendardate||hubid||metric NOT IN (SELECT DISTINCT promise_date||hubid||metric FROM daily_lm_capacity);
COMMIT;

EOF
echo "Ran summary script on" `date` >> ~/cronScripts/promiseupdatelog



