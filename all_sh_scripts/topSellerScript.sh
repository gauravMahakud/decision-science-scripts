#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/topSellerLog
drop table if exists temphomepageload_tp;
select ce.ets,eid,sid
into temphomepageload_tp
from customer_app_events ce
join customer_snapshot_ c on c.customerid=ce.uid
where c.istestcustomer is false and en='ON_LOAD_BROWSE_SCREEN' and enid='HomePage';

drop table if exists temptopbrand;
select distinct ets,sid,enid
into temptopbrand
from customer_app_events 
where en='ON_CLICK_BROWSE_ITEM' and entype='PRODUCT';

drop table if exists top_sellers_orders;
select distinct case when bo.cartitemid is not null then bo.order_item_id end as ordered
into top_sellers_orders
from customer_app_events ce
join customer_snapshot_ cs on cs.customerid=ce.uid
join (select br.sid,br.ets,min(hp.ets) 
      from temptopbrand br
      join temphomepageload_tp hp on hp.sid=br.sid and hp.ets>br.ets
      group by 1,2) fi on fi.sid=ce.sid and ce.ets>fi.ets and ce.ets<fi.min
LEFT JOIN cartitem_snapshot_ crt on crt.cartid=ce.crtid and crt.jpin=ce.pid and ce.en='ON_ADD_CART_ITEM'
LEFT JOIN bolt_order_item_snapshot bo
on bo.cartitemid = crt.cartitemid;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/topSellerLog
