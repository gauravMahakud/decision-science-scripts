#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/oosProductMoreThanHourLog
drop table if exists temp_listing_oos;
DROP TABLE if exists woohoo;
drop table if exists temp_listing_oos_1;
drop table if exists tot_listing_oos;

SELECT listing_id,
       available_in_lots - blocked_from_sale INTO temp_listing_oos
FROM listing_inventory_snapshot
WHERE DATE (src_last_updated_time) < CURRENT_DATE
AND   status = 'ACTIVE'
AND   available_in_lots - blocked_from_sale <= 0;

SELECT l1.listing_id,
       l1.src_last_updated_time,
       l2.src_last_updated_time time_next INTO TEMP woohoo
FROM listing_inventory_audit l1
  LEFT JOIN listing_inventory_audit l2
         ON l1.listing_id = l2.listing_id
        AND l1.version +1 = l2.version
WHERE DATE (l1.src_last_updated_time) = CURRENT_DATE
AND   l1.status = 'ACTIVE'
AND   l1.available_in_lots - l1.blocked_from_sale = 0;

SELECT listing_id,
       SUM(EXTRACT(epoch FROM ((CASE WHEN time_next IS NULL THEN getdate () +INTERVAL '5:30:00' ELSE time_next END) - src_last_updated_time))) / 3600 INTO temp_listing_oos_1
FROM woohoo
GROUP BY 1
HAVING SUM(EXTRACT(epoch FROM ((CASE WHEN time_next IS NULL THEN getdate () +INTERVAL '5:30:00' ELSE time_next END) - src_last_updated_time))) / 3600 >= 0.5;

SELECT *INTO tot_listing_oos
FROM temp_listing_oos;

INSERT INTO tot_listing_oos
SELECT DISTINCT listing_id
FROM temp_listing_oos_1;

insert into daily_oos_for_more_than_an_hour
SELECT p.jpin,
       p.title,
       SUM(CASE WHEN t.listing_id IS NULL THEN 1 ELSE 0 END),current_date as date 
FROM product_snapshot_ p
  JOIN sellerproduct_snapshot sp ON sp.jpin = p.jpin
  JOIN listing_snapshot li ON li.sp_id = sp.sp_id
  LEFT JOIN tot_listing_oos t ON t.listing_id = li.listing_id
WHERE p.catalogstatus = 'Active' and channel_id = 'ORGPROF-1304473229' and owner_id in(SELECT DISTINCT seller_id FROM bolt_order_item_snapshot ord
  JOIN customer_snapshot_ c ON ord.buyer_id = c.customerid
WHERE c.istestcustomer IS FALSE
AND   order_item_amount > 0
AND   ord.quantity > ord.cancelled_units + ord.returned_units + ord.return_requested_quantity
AND   (c.status = 'ACTIVE' OR c.status = 'ONHOLD'))
AND   li.status = 'ACTIVE'
GROUP BY 1,
         2
HAVING SUM(CASE WHEN t.listing_id IS NULL THEN 1 ELSE 0 END) = 0;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/oosProductMoreThanHourLog
