#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/customerAffinityForProductLog
truncate business_product_affinity_score;
insert INTO business_product_affinity_score
WITH meta_info AS
(
  SELECT v.*,
         p.pvid,
         o.order_dates,
         o.last_order
  FROM (SELECT businessid,
               pid AS jpin,
               SUM(view_dates) AS view_dates,
               MAX(last_seen) AS last_seen
        FROM customer_snapshot_ c
          JOIN (SELECT uid,
                       pid,
                       COUNT(DISTINCT (DATE (ets +INTERVAL '5:30:00'))) AS view_dates,
                       MAX(ets) AS last_seen
                FROM customer_app_events
                WHERE en = 'ON_LOAD_PRODUCT_SCREEN'
                AND   DATE (ets +INTERVAL '5:30:00') <= CURRENT_DATE
                GROUP BY 1,
                         2) v ON v.uid = c.customerid
        GROUP BY 1,
                 2) v
    LEFT JOIN (SELECT c.businessid,
                      sp.jpin,
                      COUNT(DISTINCT (DATE (bo.src_created_time))) AS order_dates,
                      MAX(bo.src_created_time) AS last_order
               FROM bolt_order_item_snapshot bo
                 JOIN customer_snapshot_ c ON c.customerid = bo.buyer_id
                 JOIN business_snapshot b ON b.businessid = c.businessid
                 JOIN listing_snapshot li ON li.listing_id = bo.listing_id
                 JOIN sellerproduct_snapshot sp ON sp.sp_id = li.sp_id
               WHERE quantity - cancelled_units - returned_units - return_requested_quantity > 0
               AND   order_item_amount > 0
               GROUP BY 1,
                        2) o
           ON o.businessid = v.businessid
          AND o.jpin = v.jpin
    JOIN product_snapshot_ p ON p.jpin = v.jpin
),
meta_segmentation AS
(
  SELECT m.businessid,
         m.jpin,
         m.order_dates,
         m.view_dates,
         EXTRACT(epoch FROM getdate () - last_seen) / 60 AS time_since_last_view,
         CASE
           WHEN CURRENT_DATE- last_order >= avg_tbo - STDDEV_POP AND CURRENT_DATE- last_order <= avg_tbo + STDDEV_POP THEN 1
           WHEN CURRENT_DATE- last_order >= avg_tbo + STDDEV_POP AND CURRENT_DATE- last_order <= avg_tbo +3*STDDEV_POP THEN 2
           WHEN CURRENT_DATE- last_order <= avg_tbo - STDDEV_POP THEN 3
         END AS order_recency_score,
         CASE
           WHEN EXTRACT(epoch FROM getdate () - last_seen) / 3600 <= 24*3 THEN 1
           WHEN EXTRACT(epoch FROM getdate () - last_seen) / 3600 > 24*3 AND EXTRACT(epoch FROM getdate () - last_seen) / 3600 <= 24*7 THEN 2
           WHEN EXTRACT(epoch FROM getdate () - last_seen) / 3600 > 24*7 AND EXTRACT(epoch FROM getdate () - last_seen) / 3600 <= 24*30 THEN 3
           WHEN EXTRACT(epoch FROM getdate () - last_seen) / 3600 > 24*30 THEN 4
         END AS view_recency_score
  FROM meta_info m
    LEFT JOIN business_jpin_mtbo mt
           ON mt.businessid = m.businessid
          AND mt.jpin = m.jpin
),
sample AS
(
  SELECT businessid,
         jpin
  FROM business_snapshot b
    JOIN product_snapshot_ p ON 1 = 1
  WHERE p.catalogstatus = 'Active'
)
SELECT d.*,
       p.title,
       ROW_NUMBER() OVER (PARTITION BY businessid ORDER BY 0.6*order_recency_rank +0.4*view_recency_rank,0.75*order_frequency_rank +0.25*view_frequency_rank) 
       
FROM (SELECT s.*,
             order_dates,
             view_dates,
             order_recency_score,
             view_recency_score, case when order_dates is null and view_dates is null then 'Default' when order_dates is null then 'View Only' else 'Orders and View' end as data_type,
             RANK() OVER (PARTITION BY s.businessid ORDER BY order_recency_score) AS order_recency_rank,
             RANK() OVER (PARTITION BY s.businessid ORDER BY order_recency_score) AS view_recency_rank,
             RANK() OVER (PARTITION BY s.businessid ORDER BY case when order_dates is null then 0 else order_dates end DESC) AS order_frequency_rank,
             RANK() OVER (PARTITION BY s.businessid ORDER BY case when view_dates is null then 0 else view_dates end DESC) AS view_frequency_rank
      FROM sample s
        LEFT JOIN meta_segmentation m
               ON m.businessid = s.businessid
              AND m.jpin = s.jpin) d
  JOIN product_snapshot_ p ON p.jpin = d.jpin;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/customerAffinityForProductLog
