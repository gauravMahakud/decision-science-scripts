HELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/offerCartNoOfferLog



insert into offer_cart_no_offer_order
  SELECT distinct getdate() + interval '5:30:00' as update_timestamp, c.firstname,
               c.businessid,ls.purchase_listing_id,
               c.phonenumber,bs.displayname,
               cis.cartitemid,
               cis.srccreatedtime,
               cis.cartid,
               cis.status,
               cis.jpin,
               ps.title,
               cis.referenceselectedquantity,
               cis.referencefinalpriceperunit,
               cis.referenceselectedquantity*cis.referencefinalpriceperunit AS total_price,
               EXTRACT(epoch FROM (cis.srccreatedtime - bs.srccreatedtime)) AS time_since_onboard,
               ROW_NUMBER() OVER (PARTITION BY c.businessid ORDER BY cis.referenceselectedquantity*cis.referencefinalpriceperunit DESC) AS rnk,
               EXTRACT(epoch FROM ((getdate () +INTERVAL '5:30' - cis.srccreatedtime))) AS time_in_cart,
               cis.offerids

        FROM cartitem_snapshot_ cis
          JOIN cart_snapshot_ ord ON ord.cartid = cis.cartid
          JOIN customer_snapshot_ c ON c.customerid = ord.customerid
          JOIN business_snapshot bs ON c.businessid = bs.businessid
          JOIN customer_milestone_ cm ON cm.bid = c.businessid
          JOIN product_snapshot_ ps ON ps.jpin = cis.jpin
         JOIN lot_snapshot ls ON ls.listing_id = cis.listingid
         join listing_snapshot lis on lis.listing_id=ls.purchase_listing_id
        WHERE cis.status = 'active'
        AND   c.status = 'ACTIVE'
        AND   c.istestcustomer IS FALSE
        AND  cis.srccreatedtime >= CURRENT_DATE -1 + interval '20:01:00'
        and cm.last_order_date !=CURRENT_DATE
        And c.businessid not like '%sajal%' and c.businessid not like '%Puja%' and c.businessid not like '%tech%'
;
      
  commit;









EOF
echo "Ran summary script on" `date` >> ~/offerCartNoOfferLog

