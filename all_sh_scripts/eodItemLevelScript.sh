#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/eodItemLevelLog
insert into eod_item_level
with store_details as 
(SELECT DATE (m._metadata__timestamp),
       m.store_id,
       delivery_status,
       reason_code_store_level
FROM last_mile_back_end_ m
  JOIN (SELECT DATE (_metadata__timestamp),store_id,
               MAX(_metadata_file_modified_ts)
        FROM last_mile_back_end_
        WHERE DATE (_metadata__timestamp) = CURRENT_DATE 
        AND   _metadata_file_name = 'Universal Delivery App/Delivery Window Confirmation'
        and store_id like 'BZID-%'
        GROUP BY 1,2) mlm ON mlm.date = DATE (m._metadata__timestamp)
   AND mlm.max = m._metadata_file_modified_ts
   and mlm.store_id=m.store_id
  JOIN (SELECT DATE (_metadata__timestamp),
               MAX(_metadata_file_modified_ts)
        FROM last_mile_back_end_
        WHERE DATE (_metadata__timestamp) = CURRENT_DATE
        AND   _metadata_file_name = 'Universal Delivery App/Delivery Window Confirmation'
        GROUP BY 1) m1
    ON m1.date = mlm.date
   AND extract(epoch from m1.max -mlm.max)*1.00/60<60
where m._metadata_file_name = 'Universal Delivery App/Delivery Window Confirmation'),
item_details as 
(SELECT DATE (m._metadata__timestamp),
       m.item_status,
       m.item_id,
       m.store_id,
       m.total_item_to_be_delivered,
       reattempt_qty,
       return_qty,
       damage_qty,
       missing_qty,
       mrp_cashback,
       weight_cashback,
       reason_code_item_level,
       trip_number
FROM last_mile_back_end_ m
  JOIN (SELECT DATE (_metadata__timestamp),item_id,
               MAX(_metadata_file_modified_ts)
        FROM last_mile_back_end_
        WHERE DATE (_metadata__timestamp) = CURRENT_DATE
        AND   _metadata_file_name = 'Universal Delivery App/Master Delivery Sheet'
        GROUP BY 1,2) mlm ON mlm.date = DATE (m._metadata__timestamp)
   AND mlm.max = m._metadata_file_modified_ts
   and mlm.item_id=m.item_id
  JOIN (SELECT DATE (_metadata__timestamp),
               MAX(_metadata_file_modified_ts)
        FROM last_mile_back_end_
        WHERE DATE (_metadata__timestamp) = CURRENT_DATE
        AND   _metadata_file_name = 'Universal Delivery App/Master Delivery Sheet'
        GROUP BY 1) m1
    ON m1.date = mlm.date
   AND extract(epoch from m1.max -mlm.max)*1.00/60<60
WHERE _metadata_file_name = 'Universal Delivery App/Master Delivery Sheet')
SELECT i.date,
       i.item_id,
       CASE
         WHEN s.delivery_status = 'Completely Delivered' THEN i.total_item_to_be_delivered
         WHEN s.delivery_status = 'Completely Reattempt' THEN 0
         WHEN s.delivery_status='Completely Returned' then 0
         when s.delivery_status='Partially Delivered' then i.total_item_to_be_delivered-isnull(reattempt_qty,0)-isnull(return_qty,0)
         else 0
       END,
       case when s.delivery_status = 'Completely Reattempt' then i.total_item_to_be_delivered
       when s.delivery_status='Partially Delivered' then isnull(reattempt_qty,0)
       else 0 end,
       case when s.delivery_status = 'Completely Returned' then i.total_item_to_be_delivered
       when s.delivery_status='Partially Delivered' then isnull(return_qty,0)
       else 0 end,
       isnull(damage_qty,0),
       isnull(missing_qty,0),
       isnull(weight_cashback,0),
       isnull(mrp_cashback,0),
       case WHEN s.delivery_status = 'Completely Delivered' THEN NULL
       when s.delivery_status='Partially Delivered' then i.reason_code_item_level
       else s.reason_code_store_level end,
       i.trip_number, getdate()
FROM item_details i JOIN store_details s ON i.store_id = s.store_id AND i.date = s.date
where item_id  like 'BLT%';
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/eodItemLevelLog
