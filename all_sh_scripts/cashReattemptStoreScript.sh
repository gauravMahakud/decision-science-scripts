#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/cashReattemptStoreLog
drop table if exists cash_reattempt_store;
with
  meta as (
    select distinct
      case when date(ord.src_created_time)<'2017-02-02'
then (CASE 
         WHEN DATE_PART('weekday',ord.src_created_time) != 6 AND DATE_PART('weekday',ord.src_created_time) != 5 AND (DATE_PART('hour',ord.src_created_time) > 15 or (DATE_PART('hour',ord.src_created_time) = 15 and DATE_PART('minute',ord.src_created_time) >10)) THEN date(ord.src_created_time) +2
         WHEN DATE_PART('weekday',ord.src_created_time) != 6 AND DATE_PART('weekday',ord.src_created_time) != 5 AND DATE_PART('hour',ord.src_created_time) < 15 or (DATE_PART('hour',ord.src_created_time) = 15 and DATE_PART('minute',ord.src_created_time) <=10) THEN date(ord.src_created_time) +1
         WHEN DATE_PART('weekday',ord.src_created_time) = 6 THEN date(ord.src_created_time) +2
         WHEN DATE_PART('weekday',ord.src_created_time) = 5 THEN date(ord.src_created_time) +1
       END )
else (CASE /*CPD defination*/ 
         WHEN DATE_PART('hour',ord.src_created_time) > 15 or (DATE_PART('hour',ord.src_created_time) = 15 and DATE_PART('minute',ord.src_created_time) >10) THEN date(ord.src_created_time) +2
         else date(ord.src_created_time) +1 end) end
AS deliverydate, c.businessid as bid
      , reason_code
      , attempt_date
    from
      bolt_order_item_snapshot ord
join customer_snapshot_ c on ord.buyer_id=c.customerid
left join store_name s on s.customerid=c.customerid
left join business_snapshot bs on bs.businessid=c.businessid
      left join eod_item_level scd on
        scd.orderitem_id = ord.order_item_id 
    where c.istestcustomer is false and  order_item_amount>0 and ord.quantity>ord.cancelled_units+ord.returned_units+ord.return_requested_quantity and (c.status='ACTIVE' or  c.status='ONHOLD')
  ),
overall as(
select
  bid
  , count(distinct(case when reason_code like '%ash%' and reason_code like '%ot%' and reason_code like '%vailable%' then deliverydate end)
)
/ convert(decimal(10, 2), count(distinct(deliverydate))) as cash_not_available_pct_overall, count(distinct(deliverydate)) as attempts_overall
from
meta
group by
1), 
thirty_days as
(select
  bid, count(distinct(case when deliverydate >= current_date -30 and reason_code like '%ash%' and reason_code like '%ot%' and reason_code like '%vailable%' then deliverydate end)
)
/ convert(decimal(10,2), count(distinct(case when deliverydate >= current_date -30 then deliverydate else current_date end)))
as cash_not_available_pct_30_days,count(distinct(case when deliverydate >= current_date -30 then deliverydate else current_date end))  as attempts_30_days
from 
meta
group by
1),
meta2 as
(select distinct
      case when date(ord.src_created_time)<'2017-02-02'
then (CASE 
         WHEN DATE_PART('weekday',ord.src_created_time) != 6 AND DATE_PART('weekday',ord.src_created_time) != 5 AND (DATE_PART('hour',ord.src_created_time) > 15 or (DATE_PART('hour',ord.src_created_time) = 15 and DATE_PART('minute',ord.src_created_time) >10)) THEN date(ord.src_created_time) +2
         WHEN DATE_PART('weekday',ord.src_created_time) != 6 AND DATE_PART('weekday',ord.src_created_time) != 5 AND DATE_PART('hour',ord.src_created_time) < 15 or (DATE_PART('hour',ord.src_created_time) = 15 and DATE_PART('minute',ord.src_created_time) <=10) THEN date(ord.src_created_time) +1
         WHEN DATE_PART('weekday',ord.src_created_time) = 6 THEN date(ord.src_created_time) +2
         WHEN DATE_PART('weekday',ord.src_created_time) = 5 THEN date(ord.src_created_time) +1
       END )
else (CASE /*CPD defination*/ 
         WHEN DATE_PART('hour',ord.src_created_time) > 15 or (DATE_PART('hour',ord.src_created_time) = 15 and DATE_PART('minute',ord.src_created_time) >10) THEN date(ord.src_created_time) +2
         else date(ord.src_created_time) +1 end) end
AS deliverydate, c.businessid as bid,
     sum((ord.quantity-ord.cancelled_units-ord.returned_units-ord.return_requested_quantity)*ord.order_item_amount/ord.quantity) as order_value
    from
     bolt_order_item_snapshot ord
join customer_snapshot_ c on ord.buyer_id=c.customerid
left join store_name s on s.customerid=c.customerid
left join business_snapshot bs on bs.businessid=c.businessid
        where c.istestcustomer is false and  order_item_amount>0 and ord.quantity>ord.cancelled_units+ord.returned_units+ord.return_requested_quantity and (c.status='ACTIVE' or  c.status='ONHOLD')
        and ord.src_created_time >= '2017-02-28'
        and date(ord.src_created_time) < current_date
group by 1,2),
order_value as (select bid,sum(case when deliverydate >=current_date-30 then order_value end) as last_30_days_gmv,
                sum(order_value) as overall_gmv
                from meta2
                group by 1)

select cm.bid as businessid,cm.storename,o.cash_not_available_pct_overall,o.attempts_overall,td.cash_not_available_pct_30_days,td.attempts_30_days,ov.last_30_days_gmv,ov.overall_gmv
into cash_reattempt_store
from (select distinct businessid as bid,displayname as storename
      from business_snapshot)cm 
left join overall o on o.bid=cm.bid
left join thirty_days td on td.bid=cm.bid
left join order_value ov on ov.bid=cm.bid;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/cashReattemptStoreLog
