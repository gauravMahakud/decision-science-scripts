SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/jpin_band_classification

insert into jpin_band_classification
WITH base_data AS
(
  SELECT DATE (ord.src_created_time) - CONVERT(INT,DATE_PART('Day',DATE (ord.src_created_time))) +1 order_month,
         p.jpin,
         p.title,
         category_name,
         pv.pvname,
         p.pvid,
         COUNT(DISTINCT (c.businessid)) AS ordering_customers,
         SUM((ord.quantity - ord.cancelled_units - ord.returned_units - ord.return_requested_quantity)*(ord.order_item_amount / ord.quantity)) AS net_gmv
  FROM bolt_order_item_snapshot ord
    JOIN customer_snapshot_ c ON c.customerid = ord.buyer_id
    LEFT JOIN business_snapshot s ON s.businessid = c.businessid
    LEFT JOIN customer_milestone_ m ON m.bid = s.businessid
    LEFT JOIN listing_snapshot li ON li.listing_id = ord.listing_id
    LEFT JOIN sellerproduct_snapshot sp ON sp.sp_id = li.sp_id
    LEFT JOIN product_snapshot_ p ON p.jpin = sp.jpin
    LEFT JOIN msku msku ON msku.jpin = p.jpin
    LEFT JOIN category cat ON p.pvid = cat.pvid
    LEFT JOIN brand_snapshot_ b ON b.brandid = p.brandid
    LEFT JOIN (SELECT DISTINCT (pvname), pvid FROM productvertical_snapshot_) pv ON pv.pvid = p.pvid
    LEFT JOIN manufacturer_snapshot ms ON ms.manufacturerid = b.manufacturerid
  WHERE c.istestcustomer = 'false'
  AND   ord.quantity - ord.cancelled_units - ord.returned_units - ord.return_requested_quantity > 0
  AND   order_item_amount > 0
  AND   DATE (ord.src_created_time) >= date(date_trunc('month',current_date - 3))
  AND   DATE (ord.src_created_time) < date(date_trunc('month',current_date + 3))
  AND   category_name NOT IN ('Freebie-Food','Freebie-Non Food','Skyfall Project')
  GROUP BY 1,
           2,
           3,
           4,
           5,
           6
),
pv_data AS
(
  SELECT order_month,
         pvid,
         pvname,
         SUM(net_gmv) AS pv_gmv
  FROM base_data
  GROUP BY 1,
           2,
           3
),
jt_gmv AS
(
  SELECT order_month,
         SUM(net_gmv) AS jt_gmv
  FROM base_data
  GROUP BY 1
),
rank_calc AS
(
  SELECT a.order_month,
         a.jpin,
         a.title,
         a.category_name,
         a.ordering_customers,
         a.net_gmv,
         b.pv_gmv,
         a.net_gmv*1.0 / b.pv_gmv AS pct_pv_gmv,
         c.jt_gmv,
         SUM(net_gmv) OVER (PARTITION BY a.order_month ORDER BY a.net_gmv DESC ROWS UNBOUNDED PRECEDING) AS cumulative_gmv,
         CASE
           WHEN (SUM(net_gmv) OVER (PARTITION BY a.order_month ORDER BY a.net_gmv DESC ROWS UNBOUNDED PRECEDING))*1.0 / c.jt_gmv <= 0.8 THEN 1
           ELSE 0
         END AS gmv_rank,
         CASE
           WHEN a.ordering_customers >= 100 THEN 1
           ELSE 0
         END AS customers_rank,
         CASE
           WHEN a.net_gmv*1.0 / b.pv_gmv >= 0.05 THEN 1
           ELSE 0
         END AS pv_rank
  FROM base_data a
    JOIN pv_data b
      ON a.order_month = b.order_month
     AND a.pvid = b.pvid
    JOIN jt_gmv c ON a.order_month = c.order_month
  GROUP BY 1,
           2,
           3,
           4,
           5,
           6,
           7,
           8,
           9
)
SELECT DATE (dateadd ('month',1,order_month)) AS classification_month,
       jpin,
       title,
       net_gmv,
       ordering_customers,
       CASE
         WHEN gmv_rank + customers_rank + pv_rank = 3 THEN 'Band 1'
         WHEN gmv_rank + customers_rank + pv_rank = 2 THEN 'Band 2'
         ELSE 'Band 3'
       END AS jpin_band
       
FROM rank_calc;
commit;

EOF
echo "Ran summary script on" `date` >> ~/cronScripts/jpin_band_classification







