#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin transaction;
\o | cat >> ~/cronScripts/orderSummaryLog
delete from orders_summary_;
insert into orders_summary_
select DATE(o.srccreatedtime) as order_date,
o.srccreatedtime as order_placed_timestamp,
ord.orderid as orderid, ord.orderitemid as order_item_id,
c.firstname as customer_name, 
case when s.storename is not null then s.storename else c.firstname end as store_name, s.tier, ord.jpin as JPIN, 
p.title as product_title, 
b.displaytitle as brand_name, 
pv.pvname as PV_name,
ord.quantity, 
ord.statusstring as orderitem_status, 
o.statusstring as order_status,
o.srccreatedtime as order_created_time, 
cat.category_name, 
sum(convert(decimal(10,2),ord.quantity*convert(decimal(10,2),sca.deadweight)))  as Orderitem_weight,
SUM(convert(decimal(10,2),o.orderamount)) as total_order_amount,
sum(convert(decimal(10,2),ord.quantity*ord.finalpriceperunit)) as orderitem_amount
from order_snapshot_ o 
    join customer_snapshot_ c on c.customerid = o.customerid 
    left join store_name s on o.customerid = s.customerid
    join orderitem_snapshot_ ord on ord.orderid = o.orderid
    left join product_snapshot_ p on p.jpin=ord.jpin
    left join category cat on p.pvid = cat.pvid     
    left join brand_snapshot_ b on b.brandid=p.brandid
    left join (select distinct(pvid),pvname from productvertical_snapshot_) pv on pv.pvid=p.pvid
join supplychainattributes_snapshot_ sca on sca.jpin=p.jpin
where c.istestcustomer = 'false'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16;
commit;
begin transaction;
insert into product_list_view
select ce.eid,
       ce.sid,
       ce.uid,
       ce.ets,
       ce.cn,
       l.lotid,
       current_timestamp as createdtime
from customer_app_events ce 
join lot_snapshot_ l on ce.lidarr like '%'+l.lotid+'%'
join (select max(ets) from product_list_view) last_update on ce.ets>last_update.max
where en='ON_LOAD_PRODUCT_LIST_SCREEN';
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/orderSummaryLog
