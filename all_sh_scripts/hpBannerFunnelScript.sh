#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/hpbannerfunnelLog


insert into hp_banner_funnel
WITH hp_banner_impr AS
(
  SELECT DISTINCT eid,
         ets +INTERVAL '5:30:00' AS ts,
         DATE (ets +INTERVAL '5:30:00'),
         enidarr,
         cardidxarr,
         uid
  FROM customer_app_events
  WHERE en = 'ON_IMPRESSION_BROWSE_SCREEN'
  AND   grpid in ('UINODE-11386163283', 'UINODE-11386154371')
  AND   DATE (ets +INTERVAL '5:30:00')  = current_date
),
hp_banner_clicks AS
(
  SELECT 
         DATE (ets +INTERVAL '5:30:00'),
         enid,
         cardindx,
         max(extract(hour from ets +INTERVAL '5:30:00'))  last_click_hour,
         count(distinct c.businessid) as bid_count
  FROM customer_app_events a
  join customer_snapshot_ c on c.customerid = a.uid and c.istestcustomer is false and c.businessid not like '%uja%' and c.businessid not like '%ech%' and c.businessid not like '%sajal%'
  WHERE en = 'ON_CLICK_BROWSE_ITEM'
  AND    grpid in ('UINODE-11386163283', 'UINODE-11386154371')
  AND   DATE (ets +INTERVAL '5:30:00') = current_date
  group by 1,2,3
),
hp_banner_impr_uinode AS
(
  SELECT DATE,
         REPLACE(REPLACE(regexp_substr (SPLIT_PART(enidarr,',',CAST(NUMBER+ 1 AS INT)),'[C|U|n][^"]*'),']',''),'[','') AS enid,
         REPLACE(REPLACE(SPLIT_PART(cardidxarr,',',CAST(NUMBER+ 1 AS INT)),']',''),'[','') AS cardidx,
         count(distinct businessid) as bid_count,
         max(extract(hour from ts)) as last_impr_hour
  FROM (SELECT *
        FROM hp_banner_impr hpb
          JOIN numbers n ON n.number < REGEXP_COUNT (enidarr,',') + 1
       ) a
  join customer_snapshot_ c on c.customerid = a.uid and c.istestcustomer is false and c.businessid not like '%uja%' and c.businessid not like '%ech%' and c.businessid not like '%sajal%'
  group by 1,2,3
),
impr_click_funnel
as
(
select hpbi.date, hpbi.enid, hpbi.cardidx, hpbi.bid_count as impr_bid_count, hpbi.last_impr_hour, hpbc.bid_count as click_bid_count, hpbc.last_click_hour
from hp_banner_impr_uinode hpbi
left join hp_banner_clicks hpbc on hpbc.enid = hpbi.enid and hpbc.date = hpbi.date and hpbi.cardidx = hpbc.cardindx
where hpbi.enid like '%CMPGN%'
group by 1,2,3,4,5,6,7
),
gmv_clicks
as
(select cae.date, cae.cardindx, cae.enid, ca.campaignname, count(distinct cae.businessid) as clicks,  sum(isnull(caa.ppvs,0)) as ppvs, sum(isnull(caa.gmv,0)) as gmv, 
       sum(isnull(caa.bid_count,0)) as bid_count, sum(isnull(caa.units,0)) as units, max(extract(hour from ts)) as last_hour_click
from
(
select distinct date(ets + interval '5:30:00'), cardindx, enid, sid, ets + interval '5:30:00' as ts, eid, c.businessid
from customer_app_events cae
join customer_snapshot_ c on c.customerid = cae.uid and c.istestcustomer is false and c.businessid not like '%uja%' and c.businessid not like '%ech%' and c.businessid not like '%sajal%'
where en = 'ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154371' and date(ets + interval '5:30:00') = current_date and enid like '%CMPGN%'
) cae
left join campaign_snapshot_ ca on ca.campaignid = cae.enid
left join
(
SELECT campaignid,
              campaignname,
      internalcampaignname,
             DATE (ets),
             e.sid,
             max(case when e.state = 'Product Page View' then e.ets end) as ppv_ts,
             COUNT(DISTINCT CASE WHEN e.state = 'Product Page View' THEN businessid END) ppvs,             
             COUNT(DISTINCT CASE WHEN ord.order_item_id IS NOT NULL THEN c.businessid END) AS bid_count,
             SUM(CASE WHEN ord.order_item_id IS NOT NULL THEN ord.order_item_amount ELSE 0 END) AS gmv,
             SUM(CASE WHEN ord.order_item_id IS NOT NULL THEN ord.quantity ELSE 0 END) AS units
      FROM (SELECT campaignid,campaignname,internalcampaignname,
                   regexp_substr(TRIM(SPLIT_PART(B.campaignlistingidlist,',',cast(num.number +1 as INTEGER))),'L[^"]*') AS listing_id
            FROM numbers num
              INNER JOIN campaign_snapshot_ B ON num.number <= REGEXP_COUNT (B.campaignlistingidlist,',')
           ) ca
        JOIN listing_snapshot li ON li.listing_id = ca.listing_id
        JOIN sellerproduct_snapshot sp ON sp.sp_id = li.sp_id
        JOIN event_wise_target_source_new e ON e.jpin = sp.jpin and e.source_state = 'HomePage Banner'
        JOIN customer_snapshot_ c ON c.customerid = e.uid
        LEFT JOIN (SELECT cai.cartitemid,
                          cai.jpin,
                          ord.quantity,
                          ord.order_item_amount,
                          ord.order_item_id, ord.src_created_time
                   FROM cartitem_snapshot_ cai
                     JOIN bolt_order_item_v2_snapshot ord ON ord.cartitemid = cai.cartitemid
                   WHERE DATE (ord.src_created_time) =current_date) ord
               ON ord.cartitemid = e.target_identifier
              AND ord.jpin = e.jpin
      WHERE DATE (ets) = current_date
      
     
      GROUP BY 1,
               2,
               3,4,5
) caa on caa.campaignid = cae.enid and cae.date = caa.date and cae.sid = caa.sid and cae.ts < caa.ppv_ts
group by 1,2,3,4)

select icf.date, icf.enid as campaignid, ca.campaignname, icf.cardidx, icf.impr_bid_count, icf.click_bid_count, gmvc.ppvs, gmvc.bid_count, gmvc.gmv, gmvc.units, icf.last_impr_hour,
       icf.last_click_hour, multi.numbers_bzid as number_of_customers_with_multiple_slots_view
from impr_click_funnel icf
left join gmv_clicks gmvc on gmvc.enid = icf.enid and gmvc.date = icf.date and gmvc.cardindx = icf.cardidx
left join campaign_snapshot_ ca on ca.campaignid = icf.enid
left join 
(
select enid, count(distinct businessid) as numbers_bzid
from
(
select businessid, enid, count(distinct cardindx), count(distinct sid), count(distinct ts)
from
(
select distinct date(ets + interval '5:30:00'), cardindx, enid, sid, ets + interval '5:30:00' as ts, eid, c.businessid
from customer_app_events cae
join customer_snapshot_ c on c.customerid = cae.uid and c.istestcustomer is false and c.businessid not like '%uja%' and c.businessid not like '%ech%' and c.businessid not like '%sajal%'
where en = 'ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154371' and date(ets + interval '5:30:00') = current_date and enid like '%CMPGN%'
)
group by 1,2  
having count(distinct cardindx) > 1 
)
group by 1
) multi on multi.enid = icf.enid
;

\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/hpbannerfunnelLog


