#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/categoryPageConversionLog
DROP TABLE if exists temphomeload;

SELECT ce.ets,
       eid,
       sid INTO temphomeload
FROM customer_app_events ce
  JOIN customer_snapshot_ c ON c.customerid = ce.uid
WHERE c.istestcustomer IS FALSE
AND   en = 'ON_LOAD_BROWSE_SCREEN'
AND   enid = 'HomePage'
AND   DATE (ce.ets +INTERVAL '05:30:00') = CURRENT_DATE;

DROP TABLE if exists tempclick;

SELECT ets,
       sid,
       enid INTO tempclick
FROM customer_app_events
WHERE en = 'ON_CLICK_BROWSE_ITEM'
AND   penid = 'HomePage'
AND   (entype = 'CATEGORY' OR entype = 'PRODUCTVERTICAL')
AND   DATE (ets +INTERVAL '05:30:00') = CURRENT_DATE;

SELECT DATE (convert_timezone ('GMT','NEWZONE -5:30',ce.ets)) AS event_date,
       COUNT(DISTINCT (CASE WHEN ce.etype != 'BG' AND en NOT LIKE 'PUSH_%' THEN s.bid END)) AS came_to_page,
       COUNT(DISTINCT (CASE WHEN en = 'ON_LOAD_PRODUCT_LIST_SCREEN' THEN s.bid END)) AS opened_product_list,
       COUNT(DISTINCT (CASE WHEN en = 'ON_LOAD_PRODUCT_SCREEN' THEN s.bid END)) AS opened_product_view,
       COUNT(DISTINCT (CASE WHEN en = 'ON_ADD_CART_ITEM' THEN s.bid END)) AS added_to_cart,
       COUNT(DISTINCT (CASE WHEN en = 'ON_TAP_GO_TO_CART' OR en = 'ON_TAP_CART_BAR' THEN s.bid END)) AS went_to_cart,
       COUNT(DISTINCT (CASE WHEN bo.cartitemid IS NOT NULL THEN s.bid END)) AS ordered,
       CONVERT(DECIMAL(10,2),COUNT(DISTINCT (CASE WHEN bo.cartitemid IS NOT NULL THEN s.bid END))) / CONVERT(DECIMAL(10,2),COUNT(DISTINCT (CASE WHEN ce.etype != 'BG' AND en NOT LIKE 'PUSH_%' THEN s.bid END))) AS conversion,
       CONVERT(DECIMAL(10,2),COUNT(DISTINCT (CASE WHEN en = 'ON_LOAD_PRODUCT_SCREEN' THEN s.bid END))) / CONVERT(DECIMAL(10,2),COUNT(DISTINCT (CASE WHEN en = 'ON_LOAD_PRODUCT_LIST_SCREEN' THEN s.bid END))) AS plv_to_product_conversion
into Category_page_conversion
FROM (SELECT ets,
             uid,
             en,
             crtid,
             etype,
             pid,
             sid
      FROM customer_app_events
      WHERE en = 'LAUNCH_APP'
      OR    en = 'ON_LOAD_HOME_SCREEN'
      OR    en = 'ON_LOAD_PRODUCT_LIST_SCREEN'
      OR    en = 'ON_LOAD_PRODUCT_SCREEN'
      OR    en = 'ON_ADD_CART_ITEM'
      OR    en = 'ON_TAP_GO_TO_CART'
      OR    en = 'ON_TAP_CART_BAR'
      OR    en = 'ON_SUCCESS_ORDER'
      AND   DATE (ets +INTERVAL '05:30:00') = CURRENT_DATE) ce
  JOIN store_name s ON s.customerid = ce.uid
  JOIN (SELECT ca.sid,
               ca.ets,
               MIN(hp.ets)
        FROM tempclick ca
          JOIN temphomeload hp
            ON hp.sid = ca.sid
           AND hp.ets > ca.ets
        GROUP BY 1,
                 2) fi
    ON fi.sid = ce.sid
   AND ce.ets > fi.ets
   AND ce.ets < fi.min
  LEFT JOIN cartitem_snapshot_ crt
         ON crt.cartid = ce.crtid
        AND crt.jpin = ce.pid
        AND ce.en = 'ON_ADD_CART_ITEM'
  LEFT JOIN bolt_order_item_snapshot bo ON bo.cartitemid = crt.cartitemid
GROUP BY 1;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/categoryPageConversionLog
