#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/topBrandsConversionLog
DROP TABLE if exists temphomepage_tp;

SELECT ce.ets,
       eid,
       sid INTO temphomepage_tp
FROM customer_app_events ce
  JOIN customer_snapshot_ c ON c.customerid = ce.uid
WHERE c.istestcustomer IS FALSE
AND   en = 'ON_LOAD_BROWSE_SCREEN'
AND   enid = 'HomePage'
AND   DATE (ce.ets +INTERVAL '05:30:00') = CURRENT_DATE;

DROP TABLE if exists temptopbrnd;

SELECT DISTINCT ets,
       sid,
       enid INTO temptopbrnd
FROM customer_app_events
WHERE en = 'ON_CLICK_BROWSE_ITEM'
AND   penid = 'HomePage'
AND   grpid = 'UINODE-11386154396'
AND   DATE (ets +INTERVAL '05:30:00') = CURRENT_DATE;

SELECT DATE (convert_timezone ('GMT','NEWZONE -5:30',ce.ets)) AS event_date,
       COUNT(DISTINCT (CASE WHEN ce.etype != 'BG' AND en NOT LIKE 'PUSH_%' THEN s.bid END)) AS came_to_page,
       COUNT(DISTINCT (CASE WHEN en = 'ON_LOAD_PRODUCT_LIST_SCREEN' THEN s.bid END)) AS opened_product_list,
       COUNT(DISTINCT (CASE WHEN en = 'ON_LOAD_PRODUCT_SCREEN' THEN s.bid END)) AS opened_product_view,
       COUNT(DISTINCT (CASE WHEN en = 'ON_ADD_CART_ITEM' THEN s.bid END)) AS added_to_cart,
       COUNT(DISTINCT (CASE WHEN en = 'ON_TAP_GO_TO_CART' OR en = 'ON_TAP_CART_BAR' THEN s.bid END)) AS went_to_cart,
       COUNT(DISTINCT (CASE WHEN bo.cartitemid IS NOT NULL THEN s.bid END)) AS ordered,
       CONVERT(DECIMAL(10,2),COUNT(DISTINCT (CASE WHEN bo.cartitemid IS NOT NULL THEN s.bid END))) / CONVERT(DECIMAL(10,2),COUNT(DISTINCT (CASE WHEN ce.etype != 'BG' AND en NOT LIKE 'PUSH_%' THEN s.bid END))) AS conversion
into Top_brands_conversion
FROM customer_app_events ce
  JOIN store_name s ON s.customerid = ce.uid
  JOIN (SELECT br.sid,
               br.ets,
               MIN(hp.ets)
        FROM temptopbrnd br
          JOIN temphomepage_tp hp
            ON hp.sid = br.sid
           AND hp.ets > br.ets
        GROUP BY 1,
                 2) fi
    ON fi.sid = ce.sid
   AND ce.ets > fi.ets
   AND ce.ets < fi.min
  LEFT JOIN cartitem_snapshot_ crt
         ON crt.cartid = ce.crtid
        AND crt.jpin = ce.pid
        AND ce.en = 'ON_ADD_CART_ITEM'
  LEFT JOIN bolt_order_item_snapshot bo ON bo.cartitemid = crt.cartitemid
WHERE DATE (ce.ets +INTERVAL '05:30:00') = CURRENT_DATE
GROUP BY 1;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/topBrandsConversionLog
