#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/businessJpinmtboLog
DROP TABLE if exists business_jpin_mtbo;

SELECT businessid,
       jpin,
       AVG(tbo) AS avg_tbo,
       stddev_pop(tbo) INTO business_jpin_mtbo
FROM (SELECT businessid,
             jpin,
             order_date - LAG(order_date,1) OVER (PARTITION BY businessid,jpin ORDER BY order_date) AS tbo,
             order_date
      FROM (SELECT DISTINCT DATE (ord.src_created_time) AS order_date,
                   c.businessid,
                   sp.jpin
            FROM customer_snapshot_ c
              JOIN bolt_order_item_snapshot ord ON c.customerid = ord.buyer_id
              JOIN listing_snapshot li ON li.listing_id = ord.listing_id
              JOIN sellerproduct_snapshot sp ON sp.sp_id = li.sp_id
            WHERE (ord.quantity > ord.cancelled_units + ord.returned_units + ord.return_requested_quantity)
            AND   order_item_amount > 0))
GROUP BY 1,
         2
HAVING COUNT(DISTINCT (order_date)) >= 4;
DROP TABLE if exists pv_level_top_jpin;

SELECT jpin,
       COUNT AS customer_count INTO pv_level_top_jpin
FROM (SELECT pvid,
             jpin,
             COUNT,
             ROW_NUMBER() OVER (PARTITION BY pvid ORDER BY COUNT DESC) AS ranking
      FROM (SELECT pvid,
                   p.jpin,
                   COUNT(DISTINCT businessid)
            FROM bolt_order_item_snapshot bo
              JOIN customer_snapshot_ c ON c.customerid = bo.buyer_id
              JOIN listing_snapshot li ON li.listing_id = bo.listing_id
              JOIN sellerproduct_snapshot sp ON sp.sp_id = li.sp_id
              JOIN product_snapshot_ p ON p.jpin = sp.jpin
            WHERE quantity - cancelled_units - returned_units - return_requested_quantity > 0
            AND   DATE (bo.src_created_time) >= CURRENT_DATE -30
            GROUP BY 1,
                     2))
WHERE ranking = 1 LIMIT 30;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/businessJpinmtboLog
