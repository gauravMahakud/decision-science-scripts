#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/orderItemBeforePromiseLog
DELETE
FROM order_item_before_promise
WHERE DATE (updated_promise_time) = CURRENT_DATE+1;

INSERT INTO order_item_before_promise
SELECT ord.eventid,
       ord.order_item_id,
       ord.order_id,
       ord.buyer_id,
       ord.seller_id,
       ord.listing_id,
       ord.quantity,
       ord.order_item_amount,
       ord.shipping_charges,
       ord.selling_price,
       ord.created_units,
       ord.ready_to_ship_units,
       ord.in_transit_units,
       ord.delivered_units,
       ord.cancelled_units,
       ord.returned_units,
       ord.order_item_status,
       ord.order_item_status_remark,
       ord.version,
       ord.src_created_time,
       ord.last_updated_by,
       ord.src_last_updated_time,
       ord.rscreatedtime,
       ord.rs_last_updated_time,
       ord.order_placed_timestamp,
       ord.confirmedunitquantity,
       ord.underprocessunitquantity,
       ord.mrp,
       ord.vat,
       ord.shipped_unit_quantity,
       ord.return_requested_quantity,
       ord.invoicenumbers,
       ord.requestid,
       ord.boltordertype,
       ord.cartitemid,
       ord.status_update_history,
       ord.tax_master,
       ord.invoice_item_qty_map,
       ord.applied_jtofferids,
       ord.failed_parent_order_item_id,
       ord.is_replacement,
       ord.returned_order_item_id,
       ord.rporeasoncode,
       pr.updated_promise_time,
       businessid
FROM bolt_order_item_v2_snapshot ord
  JOIN promise_snapshot pr ON pr.promised_entity_id = ord.order_item_id
  JOIN customer_snapshot_ c ON c.customerid = ord.buyer_id
WHERE DATE (pr.updated_promise_time) = CURRENT_DATE+1;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/orderItemBeforePromiseLog
