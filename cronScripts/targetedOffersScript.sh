#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/targetedOffersLog
drop table if exists targeted_offers_perf;
WITH pos1 AS
(
  SELECT bu.jtofferid,
         b.name,
         bu.bzid,
         bu.offerbusinessstatus,
         bu.starttime,
         bu.endtime,
         li.listingid,
         sp.jpin,
         ps.title,
         upgrade_date
  FROM jtoffer_business_snapshot bu
    JOIN jtoffer_listing_snapshot li ON li.jtofferid = bu.jtofferid
    JOIN listing_snapshot l ON l.listing_id = li.listingid
    JOIN sellerproduct_snapshot sp ON sp.sp_id = l.sp_id
    JOIN product_snapshot_ ps ON sp.jpin = ps.jpin
    LEFT JOIN (SELECT DISTINCT businessid,
                      MIN(DATE (ets +INTERVAL '5:30')) AS upgrade_date
               FROM customer_app_events cae
                 JOIN customer_snapshot_ c ON c.customerid = cae.uid
               WHERE app_version >= '1.53'
               AND   uid IS NOT NULL
               AND   businessid IS NOT NULL
               AND   DATE (ets +INTERVAL '5:30') > '2017-09-26'
               AND   DATE (ets +INTERVAL '5:30') <= CURRENT_DATE
               GROUP BY 1) ce ON ce.businessid = bu.bzid
    LEFT JOIN jtoffer_snapshot a ON a.jtoffer_id = bu.jtofferid
    LEFT JOIN business_target_group_snapshot b ON a.bztargetgroupid = b.target_group_id
  WHERE l.channel_id = 'ORGPROF-1304473229'
),
pos2 AS
(
  SELECT DATE (convert_timezone ('GMT','NEWZONE -5:30',ce.ets)) AS event_date,
         cs.businessid,
         fi.enid,
         COUNT(DISTINCT (CASE WHEN ce.etype != 'BG' AND en NOT LIKE 'PUSH_%' THEN cs.businessid END)) AS came_to_page,
         COUNT(DISTINCT (CASE WHEN en = 'ON_LOAD_PRODUCT_SCREEN' THEN cs.businessid END)) AS opened_product_view,
         COUNT(DISTINCT (CASE WHEN en = 'ON_ADD_CART_ITEM' THEN cs.businessid END)) AS added_to_cart,
         COUNT(DISTINCT (CASE WHEN en = 'ON_TAP_GO_TO_CART' OR en = 'ON_TAP_CART_BAR' THEN cs.businessid END)) AS went_to_cart,
         COUNT(DISTINCT (CASE WHEN bo.cartitemid IS NOT NULL THEN cs.businessid END)) AS ordered
  -- CONVERT(DECIMAL(10,2),COUNT(DISTINCT (CASE WHEN bo.cartitemid IS NOT NULL THEN cs.businessid END))) / CONVERT(DECIMAL(10,2),COUNT(DISTINCT (CASE WHEN ce.etype != 'BG' AND en NOT LIKE 'PUSH_%' THEN cs.businessid END))) AS conversion
         FROM (SELECT etype,
                      en,
                      uid,
                      ets,
                      crtid,
                      pid,
                      sid
               FROM customer_app_events
               WHERE DATE (ets +INTERVAL '05:30:00') >= DATE (CURRENT_DATE) -30
               --       AND   etype <> 'BG'
               AND   en IN ('ON_LOAD_PRODUCT_LIST_SCREEN','ON_LOAD_PRODUCT_SCREEN','ON_ADD_CART_ITEM','ON_TAP_GO_TO_CART','ON_TAP_CART_BAR')) ce
    JOIN customer_snapshot_ cs ON cs.customerid = ce.uid
    JOIN (SELECT br.sid,
                 br.ets,
                 br.enid,
                 MIN(hp.ets)
          FROM (SELECT ets,
                       sid,
                       enid
                FROM customer_app_events
                WHERE en = 'ON_CLICK_BROWSE_ITEM'
                AND   penid = 'HomePage'
                AND   grpid = 'UINODE-Offers4U'
                AND   DATE (ets +INTERVAL '05:30:00') >= DATE (CURRENT_DATE) -30) br
            JOIN (SELECT ce.ets,
                         eid,
                         sid
                  FROM customer_app_events ce
                    JOIN customer_snapshot_ c ON c.customerid = ce.uid
                  WHERE c.istestcustomer IS FALSE
                  AND   en = 'ON_LOAD_BROWSE_SCREEN'
                  AND   DATE (ets +INTERVAL '05:30:00') >= DATE (CURRENT_DATE) -30
                  AND   enid = 'HomePage') hp
              ON hp.sid = br.sid
             AND hp.ets > br.ets
          GROUP BY 1,
                   2,
                   3) fi
      ON fi.sid = ce.sid
     AND ce.ets > fi.ets
     AND ce.ets < fi.min
    LEFT JOIN cartitem_snapshot_ crt
           ON crt.cartid = ce.crtid
          AND crt.jpin = ce.pid
          AND ce.en = 'ON_ADD_CART_ITEM'
    LEFT JOIN bolt_order_item_snapshot bo ON bo.cartitemid = crt.cartitemid
  GROUP BY 1,
           2,
           3
),
pos3 AS
(
  SELECT a.*,
         b.*,
         d.cta,
         d.saw_offers,
         e.jpin_viewed
  FROM pos1 a
    LEFT JOIN pos2 b
           ON a.bzid = b.businessid
          AND a.jpin = b.enid
          AND b.event_date >= DATE (a.starttime)
          AND b.event_date <= DATE (a.endtime)
    LEFT JOIN (SELECT businessid,
                      DATE (ets +INTERVAL '5:30') AS event_date,
                      (CASE WHEN ce.etype != 'BG' AND en NOT LIKE 'PUSH_%' THEN '1' END) AS cta,
                      (CASE WHEN en = 'ON_IMPRESSION_BROWSE_SCREEN' AND penid = 'HomePage' AND grpname = 'Offers' THEN '1' END) AS saw_offers
               FROM customer_app_events ce
                 JOIN customer_snapshot_ c ON c.customerid = ce.uid
               WHERE DATE (ets +INTERVAL '5:30') > '2017-10-26'
               AND   DATE (ets +INTERVAL '5:30') <= CURRENT_DATE
               AND   ce.app_version >= '1.53'
               AND   c.businessid IS NOT NULL
               GROUP BY 1,
                        2,
                        3,
                        4) d
           ON d.businessid = a.bzid
          AND d.event_date >= DATE (a.starttime)
          AND d.event_date <= DATE (a.endtime)
    LEFT JOIN (SELECT ets,
                      businessid,
                      json_extract_array_element_text(enidarr,CONVERT(INT,n.number)) AS jpin_viewed
               FROM (SELECT ets,
                            businessid,
                            enidarr,
                            JSON_ARRAY_LENGTH(enidarr) AS len
                     FROM customer_app_events cae
                       JOIN customer_snapshot_ c ON c.customerid = cae.uid
                     WHERE en = 'ON_IMPRESSION_BROWSE_SCREEN'
                     AND   penid = 'HomePage'
                     AND   grpname = 'Offers'
                     AND   businessid IS NOT NULL) o
                 JOIN numbers n ON n.number < o.len) e
           ON e.businessid = a.bzid
          AND e.jpin_viewed = a.jpin
          AND DATE (e.ets) >= DATE (a.starttime)
          AND DATE (e.ets) <= DATE (a.endtime)
)
SELECT jtofferid,
       name,
       jpin,
       title,
       COUNT(DISTINCT (bzid)) AS targeted_customers,
       COUNT(DISTINCT (CASE WHEN upgrade_date IS NOT NULL THEN bzid END)) AS eligible_customers,
       COUNT(DISTINCT (CASE WHEN cta = 1 THEN bzid END)) AS came_to_app,
       COUNT(DISTINCT (CASE WHEN saw_offers = 1 THEN bzid END)) AS saw_offers,
       COUNT(DISTINCT (CASE WHEN jpin_viewed IS NOT NULL THEN bzid END)) AS saw_product,
       COUNT(DISTINCT (CASE WHEN opened_product_view = 1 THEN bzid END)) AS clicked_ppv,
       COUNT(DISTINCT (CASE WHEN added_to_cart = 1 THEN bzid END)) AS added_to_cart,
       COUNT(DISTINCT (CASE WHEN ordered = 1 THEN bzid END)) AS ordered,
       CASE
         WHEN COUNT(DISTINCT (bzid)) = 0 THEN 0
         ELSE COUNT(DISTINCT (
           CASE
             WHEN upgrade_date IS NOT NULL THEN bzid
           END 
         ))*1.0 / COUNT(DISTINCT (bzid))
       END AS pct_eligible,
       CASE
         WHEN COUNT(DISTINCT (
           CASE
             WHEN upgrade_date IS NOT NULL THEN bzid
           END 
         )) = 0 THEN 0
         ELSE COUNT(DISTINCT (
           CASE
             WHEN cta = 1 THEN bzid
           END 
         ))*1.0 / COUNT(DISTINCT (
           CASE
             WHEN upgrade_date IS NOT NULL THEN bzid
           END 
         ))
       END AS pct_cta,
       CASE
         WHEN COUNT(DISTINCT (
           CASE
             WHEN cta = 1 THEN bzid
           END 
         )) = 0 THEN 0
         ELSE COUNT(DISTINCT (
           CASE
             WHEN saw_offers = 1 THEN bzid
           END 
         ))*1.0 / COUNT(DISTINCT (
           CASE
             WHEN cta = 1 THEN bzid
           END 
         ))
       END AS pct_saw_offers,
 CASE
         WHEN COUNT(DISTINCT (
           CASE
             WHEN saw_offers = 1 THEN bzid
           END 
         )) = 0 THEN 0
         ELSE COUNT(DISTINCT (
          case when jpin_viewed IS NOT NULL THEN bzid
           END 
         ))*1.0 / COUNT(DISTINCT (
           CASE
             WHEN saw_offers = 1 THEN bzid
           END 
         ))
       END AS pct_saw_product,       

CASE
         WHEN COUNT(DISTINCT (
           CASE
             WHEN jpin_viewed IS NOT NULL THEN bzid
           END 
         )) = 0 THEN 0
         ELSE COUNT(DISTINCT (
           CASE
             WHEN opened_product_view = 1 THEN bzid
           END 
         ))*1.0 / COUNT(DISTINCT (
           CASE
             WHEN jpin_viewed IS NOT NULL THEN bzid
           END 
         ))
       END AS pct_ppv,
       CASE
         WHEN COUNT(DISTINCT (
           CASE
             WHEN opened_product_view = 1 THEN bzid
           END 
         )) = 0 THEN 0
         ELSE COUNT(DISTINCT (
           CASE
             WHEN added_to_cart = 1 THEN bzid
           END 
         ))*1.0 / COUNT(DISTINCT (
           CASE
             WHEN opened_product_view = 1 THEN bzid
           END 
         ))
       END AS pct_atc,
       CASE
         WHEN COUNT(DISTINCT (
           CASE
             WHEN opened_product_view = 1 THEN bzid
           END 
         )) = 0 THEN 0
         ELSE COUNT(DISTINCT (
           CASE
             WHEN ordered = 1 THEN bzid
           END 
         ))*1.0 / COUNT(DISTINCT (
           CASE
             WHEN opened_product_view = 1 THEN bzid
           END 
         ))
       END AS pct_ordered
       
into targeted_offers_perf
FROM pos3
WHERE jtofferid != 'JTOFR-1304453550'
AND   name NOT IN ('','Test-Grp')
GROUP BY 1,
         2,
         3,
         4;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/targetedOffersLog
