#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/dailyStockStateLog
insert into daily_stock_state
SELECT DISTINCT CURRENT_DATE,
       p.jpin,
       'In Stock' AS stock_state
FROM product_snapshot_ p
  JOIN sellerproduct_snapshot sp ON sp.jpin = p.jpin
  JOIN listing_snapshot li ON li.sp_id = sp.sp_id
  JOIN listing_inventory_snapshot inv ON inv.listing_id = li.listing_id
  JOIN org_profile_snapshot org ON org.org_profile_id = li.owner_id
WHERE p.catalogstatus = 'Active'
AND   channel_id = 'ORGPROF-1304473229'
AND   org.test_user is false
AND   li.status = 'ACTIVE' and inv.available_in_lots - blocked_from_sale > 0;
commit;
begin;
insert into daily_stock_state
SELECT DISTINCT CURRENT_DATE,
       p.jpin,
       'OOS' AS stock_state
FROM product_snapshot_ p
  JOIN sellerproduct_snapshot sp ON sp.jpin = p.jpin
  JOIN listing_snapshot li ON li.sp_id = sp.sp_id
  JOIN listing_inventory_snapshot inv ON inv.listing_id = li.listing_id
  JOIN org_profile_snapshot org ON org.org_profile_id = li.owner_id
  left join daily_stock_state f on f.date=current_date and f.jpin=p.jpin
WHERE p.catalogstatus = 'Active'
AND   channel_id = 'ORGPROF-1304473229'
AND   org.test_user is false
AND   li.status = 'ACTIVE'
and f.jpin is null;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/dailyStockStateLog
