#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/fillRateLog
insert into daily_order_item_wise_fill_rate
SELECT aman.*,
       d.jpin,
       d.title,
       e.jit_purchase_order_item_id as jw_order_item_id,f.jit_purchase_order_item_id as vendor_order_item_id
       
FROM ( WITH orderitem_reason_code
AS
(SELECT order_item_id,
       quantity AS quantity_ordered,
       JSON_EXTRACT_PATH_TEXT(changes,'reasonCode') AS status_remark,
       JSON_EXTRACT_PATH_TEXT(changes,'status') AS status,
       JSON_EXTRACT_PATH_TEXT(changes,'quantity') AS quantity
FROM (SELECT o.order_item_id,
             o.quantity,
             json_extract_array_element_text(status_update_history,CONVERT(INT,n.number)) AS changes
      FROM (SELECT order_item_id,
                   quantity,
                   status_update_history,
                   JSON_ARRAY_LENGTH(status_update_history) AS len
            FROM bolt_order_item_snapshot
            WHERE boltordertype = 'MARKETPLACE'
            AND   DATE (src_created_time) >= '2017-11-01') o
        JOIN numbers n ON n.number <= o.len)),outstanding_orders AS (SELECT o.order_item_id,
                                                                            o.quantity_ordered,
                                                                            SUM(CASE WHEN c.isfillrate IS FALSE THEN CONVERT(INT,o.quantity) ELSE 0 END) AS customer_cancelled_units,o.quantity_ordered - SUM (CASE WHEN c.isfillrate IS FALSE THEN CONVERT (INT,o.quantity) ELSE 0 END) AS net_order_quantity 
     FROM orderitem_reason_code o
  LEFT JOIN customer_cancellation_reasons c ON c.status_remark = o.status_remark
GROUP BY 1,
         2)
SELECT DATE (pr.updated_promise_time),
       ord.order_item_id,
       ord.quantity,
       ord.listing_id,
       o.quantity AS plan_quantity,
       o.trip_ref_number,
       o.assigneeid,
       CASE
         WHEN reason IN ('WRONG_MRP','STORE_OUT_OF_SERVICE_AREA','QUALITY_ISSUE','INVOICE_NOT_AVAILABLE','OTHER','DAMAGED','WEIGHT_MISMATCH','EXPIRED','MISSING','LOCATION_NOT_FOUND','DELAY_IN_DELIVERY') THEN reason
         WHEN o.order_item_id IS NULL AND ord1.net_order_quantity > 0 THEN 'ITEM NOT MARKED RTS'
         WHEN o.payment_id IS NULL AND ord1.net_order_quantity > 0 THEN 'ITEM NOT SHIPPED FROM FC'
       END AS reason
FROM bolt_order_item_snapshot ord
  JOIN customer_snapshot_ c ON ord.buyer_id = c.customerid
  LEFT JOIN business_snapshot bs ON bs.businessid = c.businessid
  JOIN promise_snapshot pr ON pr.promised_entity_id = ord.order_item_id
  JOIN outstanding_orders ord1 ON ord1.order_item_id = ord.order_item_id
  LEFT JOIN (SELECT DATE (a.start_timestamp),
                    s.shipment_item_entity_id AS order_item_id,
                    CONCAT('TRITM-',e.transit_item_id) AS transit_item_id,
                    h.payment_id,
                    j.mapping_id,
                    j.quantity,
                    j.delta_type,
                    j.amount_difference,
                    j.reason,
                    a.assigneeid,
                    a.trip_ref_number
             FROM trip a
               JOIN trip_node b ON b.trip_id = a.trip_id
               JOIN shipment c
                 ON (a.trip_id = c.trip_id
                AND c.drop_node_id = b.node_id)
               JOIN transit_plan d ON c.shipment_id = d.shipment_id
               JOIN transit_item e ON e.transit_item_id = d.transit_item_id
               JOIN shipment_item s ON s.shipment_item_id = e.shipment_item_id
               LEFT JOIN (SELECT DISTINCT a.trip_id,
                                 c.location_id,
                                 c.name AS hub_name
                          FROM trip a
                            JOIN trip_node b ON b.trip_id = a.trip_id
                            JOIN location c ON b.location_id = c.location_id
                          WHERE b.node_type = 'BEGIN') g ON a.trip_id = g.trip_id
               LEFT JOIN payment h ON h.trip_node_id = CONCAT ('TRN-',b.node_id)
               LEFT JOIN payment_entity_mapping i
                      ON (i.sub_entity_id = CONCAT ('TRITM-',e.transit_item_id)
                     AND i.payment_id = h.payment_id)
               LEFT JOIN delta_item j ON j.mapping_id = i.mapping_id
             WHERE e.transit_item_status != 'DELEGATED'
             AND   i.deleted_at IS NULL) o
         ON o.order_item_id = ord.order_item_id
        AND DATE (pr.updated_promise_time) = o.date
  LEFT JOIN (SELECT DATE (ets),
                    biz_id AS businessid,
                    MIN(ets) AS TIME
             FROM last_mile_app_events
             WHERE events = 'ON_SUBMIT_CHECK_IN'
             GROUP BY 1,
                      2) ci
         ON ci.businessid = c.businessid
        AND ci.date = DATE (pr.updated_promise_time)
WHERE DATE (pr.updated_promise_time)= CURRENT_DATE
AND   c.istestcustomer IS FALSE) aman 
LEFT JOIN listing_snapshot b ON b.listing_id = aman.listing_id 
LEFT JOIN sellerproduct_snapshot c ON c.sp_id = b.sp_id 
LEFT JOIN product_snapshot_ d ON d.jpin = c.jpin 
LEFT JOIN fulfilment_item_snapshot e ON e.order_item_id = aman.order_item_id 
left join fulfilment_item_snapshot f on f.order_item_id=e.jit_purchase_order_item_id
WHERE aman.reason IS NOT NULL AND (e.type = 'JIT'
OR    e.fulfilment_item_id IS NULL)  and f.quantity>0;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/fillRateLog
