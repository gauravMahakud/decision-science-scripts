#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/homePageBannerViewLog
drop
  table
    if exists lol_hd;

select
  ce.ets
  , eid
  , sid into
    lol_hd
  from
    customer_app_events ce
    join customer_snapshot_ c on
      c.customerid = ce.uid
  where
    c.istestcustomer is false
    and en = 'ON_LOAD_BROWSE_SCREEN'
    and enid = 'HomePage'
    and date(ce.ets+interval'05:30:00') = current_date;

drop
  table
    if exists temphpb;

select
  ets
  , sid
  , enid 
  into
    temphpb
  from
    customer_app_events
  where
    en = 'ON_CLICK_BROWSE_ITEM'
    and penid = 'HomePage'
    and grpid = 'UINODE-11386154371'
    and date(ets+interval'05:30:00') = current_Date;

select
  date(convert_timezone('GMT', 'NEWZONE -5:30', ce.ets)) as event_date
  , fi.enid
  , count(distinct(s.bid)) as clicked_banner
  , count(distinct(case when en = 'ON_LOAD_PRODUCT_LIST_SCREEN' then s.bid end)
)
as opened_product_list
, count(distinct(case when en = 'ON_LOAD_PRODUCT_SCREEN' then s.bid end)
)
as opened_product_view
, count(distinct(case when en = 'ON_ADD_CART_ITEM' then s.bid end)
)
as added_to_cart
, count(distinct(case when en = 'ON_TAP_GO_TO_CART' or en = 'ON_TAP_CART_BAR' then s.bid end)
)
as went_to_cart
, count(distinct(case when bo.cartitemid is not null then s.bid end)
)
as ordered
, convert(decimal(10, 2), count(distinct(case when bo.cartitemid is not null then s.bid end))
)
/ convert(decimal(10, 2), count(distinct(s.bid))) as conversion
into Home_page_banner_conversion
from
(
  select
    ets
    , uid
    , en
    , crtid
    , etype
    , pid
    , sid
  from
    customer_app_events
  where
    (en = 'LAUNCH_APP'
    or en = 'ON_LOAD_HOME_SCREEN'
    or en = 'ON_LOAD_PRODUCT_LIST_SCREEN'
    or en = 'ON_LOAD_PRODUCT_SCREEN'
    or en = 'ON_ADD_CART_ITEM'
    or en = 'ON_TAP_GO_TO_CART'
    or en = 'ON_TAP_CART_BAR'
    or en = 'ON_SUCCESS_ORDER'
    or en like 'ON_IMPRESSION%') and date(ets+interval'05:30:00') = current_date
)
ce
join store_name s on
  s.customerid = ce.uid
join(
  select
    mc.sid
    , mc.ets
    , mc.enid
    , min(hp.ets)
  from
    temphpb mc
    left join lol_hd hp on
      hp.sid = mc.sid
      and hp.ets > mc.ets
  group by
    1
    , 2
    , 3)
fi on
  fi.sid = ce.sid
  and ce.ets > fi.ets
  and ce.ets < fi.min
left join cartitem_snapshot_ crt on
  crt.cartid = ce.crtid
  and crt.jpin = ce.pid
  and ce.en = 'ON_ADD_CART_ITEM'
left join bolt_order_item_snapshot bo on
  bo.cartitemid = crt.cartitemid
group by 1,2;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/homePageBannerViewLog
