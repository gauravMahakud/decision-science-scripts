

echo "Ran summary script on" `date` >> ~/cronScripts/fillRateLogTH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/fillRateLog


INSERT INTO last_mile_fill_rate
SELECT DISTINCT ord.*,
       ch.hubid,
       fc.facility_id,
       fc.facility_name,
       dp.*,
       lm.*
FROM (SELECT o.order_item_id,
             o.quantity_ordered,
             buyer_id,
             listing_id,
             left(seller_type,16),
             order_item_status_remark,
             cancelled_units,
             price_per_unit,
             businessid,
             promise_date,
             SUM(CASE WHEN cancellation_type = 'CUSTOMER' AND status = 'CANCELLED' THEN CONVERT(INT,o.quantity) ELSE 0 END) AS cust_can,
             SUM(CASE WHEN c.isfillrate IS FALSE AND status = 'CANCELLED' THEN CONVERT(INT,o.quantity) ELSE 0 END) AS customer_cancelled_units,
             SUM(CASE WHEN c.cancellation_type = 'JT' AND status = 'CANCELLED' THEN CONVERT(INT,o.quantity) ELSE 0 END) AS jt_cancellations,
             o.quantity_ordered - SUM(CASE WHEN c.isfillrate IS FALSE THEN CONVERT(INT,o.quantity) ELSE 0 END) AS net_order_quantity
      FROM (SELECT order_item_id,
                   listing_id,
                   buyer_id,
                   seller_type,
                   cancelled_units,
                   price_per_unit,
                   quantity AS quantity_ordered,
                   order_item_status_remark,
                   businessid,
                   promise_date,
                   JSON_EXTRACT_PATH_TEXT(changes,'reasonCode') AS status_remark,
                   JSON_EXTRACT_PATH_TEXT(changes,'status') AS status,
                   JSON_EXTRACT_PATH_TEXT(changes,'quantity') AS quantity,
                   JSON_EXTRACT_PATH_TEXT(changes,'timestamp') AS updated_timestamp
            FROM (SELECT o.order_item_id,
                         o.quantity,
                         price_per_unit,
                         listing_id,
                         buyer_id,
                         seller_type,
                         cancelled_units,
                         order_item_status_remark,
                         businessid,
                         promise_date,
                         json_extract_array_element_text(status_update_history,CONVERT(INT,n.number)) AS changes
                  FROM (SELECT ord.order_item_id,
                               ord.quantity,
                               ord.listing_id,
                               ord.businessid,
                               DATE (ord.updated_promise_time) AS promise_date,
                               ord.buyer_id,
                               ord.cancelled_units,
                               (ord.order_item_amount + ord.shipping_charges) / ord.quantity AS price_per_unit,
                               CASE
                                 WHEN ord.seller_id IN ('ORGPROF-1304473231','ORGPROF-1304473270','ORGPROF-1304473266','ORGPROF-1304473271') THEN 'Strategic Sellers'
                                 ELSE 'Boltlite sellers'
                               END AS seller_type,
                               ord.status_update_history,
                               ord.order_item_status_remark,
                               JSON_ARRAY_LENGTH(ord.status_update_history) AS len
                        FROM order_item_before_promise ord
                          LEFT JOIN (SELECT DISTINCT failed_parent_order_item_id
                                     FROM bolt_order_item_v2_snapshot ord
                                     WHERE failed_parent_order_item_id IS NOT NULL
                                     AND   order_item_status != 'CANCELLED'
                                     AND   status_update_history NOT LIKE '%INTERNAL_ERROR%') fo ON ord.order_item_id = fo.failed_parent_order_item_id
                        WHERE boltordertype = 'MARKETPLACE'
                        AND   DATE (ord.src_created_time) >= '2017-11-01'
                        AND   fo.failed_parent_order_item_id IS NULL) o
                    JOIN numbers n
                      ON n.number < o.len
                      OR o.len = 0)) o
        LEFT JOIN customer_cancellation_reasons c ON c.status_remark = o.status_remark
      GROUP BY 1,
               2,
               3,
               4,
               5,
               6,
               7,
               8,
               9,
               10) ord
  LEFT JOIN customer_hub_snapshot ch ON ch.customerid = ord.buyer_id
  LEFT JOIN (SELECT distinct ord.order_item_id,
                    s.facility_id,
                    f.name AS facility_name
             FROM bolt_order_item_v2_snapshot ord
               JOIN fulfilment_item_snapshot fu ON fu.order_item_id = ord.order_item_id
               JOIN fulfilment_item_snapshot fujw ON fu.jit_purchase_order_item_id = fujw.order_item_id and fujw.type='PHYSICAL'
               LEFT JOIN dw_bradman.inventory_item ii ON fujw.fulfilment_item_id = ii.creator_process_ref_id
               LEFT JOIN dw_bradman.space s ON s.id = ii.space_id
               LEFT JOIN facility f ON f.id = s.facility_id) fc ON fc.order_item_id = ord.order_item_id
  LEFT JOIN (SELECT si.shipment_item_entity_id,
                    DATE (CASE WHEN t.start_timestamp IS NULL THEN si.expected_delivery_time ELSE t.start_timestamp END) AS expected_date,
                    ti.transit_item_id AS transit_item_id,
                    ti.deleted_at,
                    ti.src_created_time AS transit_item_created_time,
                    tp.transit_plan_id,
                    tp.in_trip_transit_item_status,
                    drop_node_id,
                    tt.operation_timestamp AS checkin_time
             FROM dw_shipment.shipment_item si
               JOIN dw_shipment.transit_item ti ON si.shipment_item_id = ti.shipment_item_id
               LEFT JOIN dw_shipment.transit_plan tp ON tp.transit_item_id = ti.transit_item_id
               LEFT JOIN dw_shipment.shipment s ON s.shipment_id = tp.shipment_id
               LEFT JOIN dw_shipment.trip t ON t.trip_id = s.trip_id
               LEFT JOIN dw_shipment.trip_tracker tt
                      ON tt.node_id = s.drop_node_id
                     AND (tt.deleted_at IS NULL)
                     AND tt.trip_tracking_operation = 'TRIP_NODE_CHECKIN'
             WHERE (ti.deleted_at IS NULL OR DATE (ti.deleted_at) > DATE (ti.expected_delivery_time))
             AND   (ti.transit_item_status not in ('DELEGATED','RECEIVED','CROSSED') OR DATE (ti.src_last_updated_time) > DATE (ti.expected_delivery_time))
             AND   DATE (ti.src_created_time) <= DATE (ti.expected_delivery_time)) dp
         ON dp.shipment_item_entity_id = ord.order_item_id
        AND dp.expected_date = ord.promise_date
  LEFT JOIN (SELECT m.mapping_id AS mapping_id,
                    m.entity_id,
                    m.sub_entity_id,
                    m.expected_amount,
                    m.entity_type,
                    di.delta_item_id,
                    di.delta_type,
                    di.reason,
                    di.quantity,
                    Di.created_time + interval '5:30' AS delta_item_created_time
             FROM payment_entity_mapping m
               LEFT JOIN delta_item di ON di.mapping_id = m.mapping_id
             WHERE m.deleted_at IS NULL
             AND   di.deleted_at IS NULL) lm ON lm.sub_entity_id = dp.transit_item_id
WHERE (lm.delta_item_created_time IS NULL OR DATE (lm.delta_item_created_time) = ord.promise_date)
AND   ord.promise_date = current_date
;
Commit;




EOF
echo "Ran summary script on" `date` >> ~/cronScripts/fillRateLog




