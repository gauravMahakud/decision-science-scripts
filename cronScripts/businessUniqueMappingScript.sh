SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/businessuniqueLog

INSERT INTO business_unique_id_mapping
SELECT businessid,
       'JT-' ||cast(RANDOM()*10 AS INT) ||cast(RANDOM()*10 AS INT) || CAST(RANDOM()*10 AS INT) ||cast(RANDOM()*10 AS INT) || RIGHT (businessid,10) AS unique_id
FROM business_snapshot
WHERE businessid NOT IN (SELECT DISTINCT businessid FROM business_unique_id_mapping);

COMMIT;

EOF
echo "Ran summary script on" `date` >> ~/cronScripts/businessuniqueLog


