SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/eventWiseTargetSourceV1


DELETE
FROM all_events_click_stream
WHERE DATE (ets) = CURRENT_DATE;

COMMIT;

INSERT INTO all_events_click_stream
SELECT sid,
       uid,
       ce.ets +INTERVAL '5:30:00' AS ets,
       eid,
       ltvid,
       CASE
         WHEN en LIKE 'PUSH%' OR en = 'ON_LOAD_INAPP_NOTIFICATION_SCREEN' THEN LEFT (cmpid,24)
         ELSE enid
       END AS enid,
       grpid,
       cardindx,
       enidarr,
       cardidxarr,
       pidarr,
       CASE
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386154371' THEN 'HomePage Banner'
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386154396' THEN 'Top Brands'
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386155111' THEN 'All time Special Price'
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'OffersPage' THEN 'Offers Page'
         WHEN en = 'ON_LOAD_ALL_BRANDS_SCREEN' THEN 'All Brands Page'
         WHEN en = 'ON_APP_CAME_TO_FG' AND lnchrfrl = 'DEEP_LINK' THEN 'SMS or Whatsapp'
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND grpid = 'UINODE-11386152667' THEN 'Category Page Banner-Rice'
         WHEN en = 'ON_SUBMIT_SEARCH_QUERY' OR en = 'ON_SUBMIT_AUTO_SUGGEST' THEN 'Search'
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND grpid = 'UINODE-11386155282' THEN 'Jumbotail Exclusive'
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' THEN 'Category Page'
         WHEN en = 'PUSH_NOTIFICATION_OPEN'  THEN 'Push Notification'
         WHEN en IN ('ON_SUBMIT_SEARCH_QUERY','ON_SUBMIT_AUTO_SUGGEST','ON_SUBMIT_RECENT_SEARCH') THEN 'Search'
         WHEN en = 'ON_LOAD_INAPP_NOTIFICATION_SCREEN' THEN 'In-App_notification'
         ELSE 'unknown'
       END AS state,
       'source' AS state_type,
       url
FROM customer_app_events ce
WHERE ((en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386154371') OR (en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386154396') OR (en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'OffersPage') OR (en = 'ON_LOAD_ALL_BRANDS_SCREEN') OR (en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage') OR (en = 'PUSH_NOTIFICATION_OPEN' ) OR (en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386155111') OR (en = 'ON_SUBMIT_SEARCH_QUERY' OR en = 'ON_SUBMIT_AUTO_SUGGEST') OR en IN ('ON_SUBMIT_SEARCH_QUERY','ON_SUBMIT_AUTO_SUGGEST','ON_SUBMIT_RECENT_SEARCH') OR en = 'ON_LOAD_INAPP_NOTIFICATION_SCREEN' OR (en = 'ON_CLICK_BROWSE_ITEM' AND grpid = 'UINODE-11386152667') OR (en = 'ON_APP_CAME_TO_FG' AND lnchrfrl = 'DEEP_LINK'))
AND   DATE (ets +INTERVAL '5:30:00') = CURRENT_DATE;

COMMIT;

INSERT INTO all_events_click_stream
SELECT sid,
       uid,
       ce.ets +INTERVAL '5:30:00',
       eid,
       ltvid,
       enid,
       grpid,
       cardindx,
       enidarr,
       cardidxarr,
       pidarr,
       CASE
         WHEN en = 'ON_LOAD_PRODUCT_LIST_SCREEN' THEN 'PLV'
         ELSE 'POR'
       END 
,
       'target',
       url
FROM customer_app_events ce
WHERE (en = 'ON_LOAD_PRODUCT_LIST_SCREEN' OR en = 'ON_LOAD_POST_ORDER_RECOMMENDATION_LIST')
AND   DATE (ets +INTERVAL '5:30:00') = CURRENT_DATE;

COMMIT;

DELETE
FROM event_wise_target_source_v1
WHERE DATE (ets) = CURRENT_DATE;

COMMIT;

INSERT INTO event_wise_target_source_v1
WITH all_events_updated
AS
(SELECT *,
       ROW_NUMBER() OVER (PARTITION BY uid,state_type ORDER BY ets) AS state_rank,
       ROW_NUMBER() OVER (PARTITION BY uid ORDER BY ets) AS event_rank
FROM all_events_click_stream),target_events AS (SELECT * FROM all_events_updated WHERE state_type = 'target'), 
source_events AS 
(SELECT * FROM all_events_updated WHERE state_type = 'source')
 
SELECT t.sid AS target_sid,
       t.uid,
       t.ets,
       t.eid AS target_eid,
       t.ltvid AS target_ltvid,
       'Product List View' AS target_state,
       CASE
         WHEN t.state = 'POR' THEN 'Post Order recommendation'
         ELSE s.state
       END AS source_state,
       s.eid AS source_eid,
       s.ltvid AS source_ltvid,
       s.enid AS source_enid,
       s.grpid AS source_grpid,
       s.cardindx AS source_cardindx,
       s.enidarr AS source_enidarr,
       s.cardidxarr AS source_cardindxarr,
       s.pidarr AS source_pidarr ,
       NULL,
       TRIM(s.url)
FROM target_events t
  LEFT JOIN source_events s
         ON t.uid = s.uid
        AND t.event_rank - t.state_rank = s.state_rank;

DELETE
FROM all_events_click_stream_
WHERE DATE (ets) = CURRENT_DATE;

commit;

INSERT INTO all_events_click_stream_
SELECT sid,
       uid,
       ce.ets +INTERVAL '5:30:00' AS ets,
       eid,
       pid,
       ltvid,
       CASE
         WHEN en LIKE 'PUSH%' OR en = 'ON_LOAD_INAPP_NOTIFICATION_SCREEN' THEN LEFT (cmpid,24)
When en=’ON_CLICK_BROWSE_ITEM’ and grpid  in ( 'UINODE-similarProduct',’UINODE-compProduct’) then penid
         ELSE enid
       END AS enid,
       grpid,
       cardindx,
       enidarr,
       cardidxarr,
       pidarr,
       CASE
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386154371' THEN 'HomePage Banner'
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386154396' THEN 'Top Brands'
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386155111' THEN 'All time Special Price'
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'OffersPage' THEN 'Offers Page'
         WHEN en = 'ON_APP_CAME_TO_FG' AND lnchrfrl = 'DEEP_LINK' THEN 'SMS or Whatsapp'
         WHEN en = 'ON_LOAD_ALL_BRANDS_SCREEN' THEN 'All Brands Page'
         WHEN en = 'ON_SUBMIT_SEARCH_QUERY' OR en = 'ON_SUBMIT_AUTO_SUGGEST' OR en = 'ON_SUBMIT_RECENT_SEARCH' THEN 'Search'
         WHEN en = 'PUSH_NOTIFICATION_OPEN'  THEN 'Push Notification'
         WHEN en = 'ON_SUCCESS_SCAN' THEN 'Scanned Image'
         WHEN en = 'ON_LOAD_MY_ORDERS_SCREEN' THEN 'Order History'
         WHEN grpid = 'UINODE-11386154067' AND enid LIKE 'JPIN-%' AND en = 'ON_CLICK_BROWSE_ITEM' THEN 'Top Sellers'
         WHEN grpid = 'UINODE-11386155173' AND enid LIKE 'JPIN-%' AND en = 'ON_CLICK_BROWSE_ITEM' THEN 'Products for you'
         WHEN grpid = 'UINODE-Offers4U' AND enid LIKE 'JPIN-%' AND en = 'ON_CLICK_BROWSE_ITEM' THEN 'Offers for you'
         WHEN grpid = 'UINODE-similarProduct' AND enid LIKE 'JPIN-%' AND en = 'ON_CLICK_BROWSE_ITEM' THEN 'Similar Products'
         WHEN grpid = 'UINODE-newLaunch' AND enid LIKE 'JPIN-%' AND en = 'ON_CLICK_BROWSE_ITEM' THEN 'New Launch HP Module'
         WHEN grpid = 'UINODE-backInStock' AND enid LIKE 'JPIN-%' AND en = 'ON_CLICK_BROWSE_ITEM' THEN 'Back in Stock HP Module'
         WHEN grpid = 'UINODE-bestPrice' AND enid LIKE 'JPIN-%' AND en = 'ON_CLICK_BROWSE_ITEM' THEN 'Best Price HP Module'
         WHEN grpid = 'UINODE-11386155282' AND enid LIKE 'JPIN-%' AND en = 'ON_CLICK_BROWSE_ITEM' THEN 'Jumbotail Exclusive'
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND grpid = 'UINODE-11386152667' THEN 'Category Page Banner-Rice'
         WHEN grpid = 'UINODE-compProduct' AND enid LIKE 'JPIN-%' AND en = 'ON_CLICK_BROWSE_ITEM' THEN 'Complementary Products'
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' THEN 'Category Page'
         ELSE 'unknown'
       END AS state,
       'source' AS state_type,
       url
FROM customer_app_events ce
WHERE ((en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386154371') OR (en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386154396') OR (en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'OffersPage') OR (en = 'ON_LOAD_ALL_BRANDS_SCREEN') OR (en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage') OR (en = 'PUSH_NOTIFICATION_OPEN' ) OR (en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386155111') OR (en = 'ON_SUCCESS_SCAN') OR (en = 'ON_LOAD_MY_ORDERS_SCREEN') OR (en = 'ON_CLICK_BROWSE_ITEM' AND grpid = 'UINODE-similarProduct') OR (en = 'ON_SUBMIT_SEARCH_QUERY' OR en = 'ON_SUBMIT_AUTO_SUGGEST' OR en = 'ON_SUBMIT_RECENT_SEARCH') OR (en = 'ON_CLICK_BROWSE_ITEM' AND grpid = 'UINODE-11386152667') OR (grpid = 'UINODE-compProduct' AND enid LIKE 'JPIN-%' AND en = 'ON_CLICK_BROWSE_ITEM') OR (en = 'ON_APP_CAME_TO_FG' AND lnchrfrl = 'DEEP_LINK'))
AND   DATE (ets +INTERVAL '5:30:00') = CURRENT_DATE;

COMMIT;

INSERT INTO all_events_click_stream_
SELECT sid,
       uid,
       ce.ets +INTERVAL '5:30:00' AS ets,
       eid,
       pid,
       ltvid,
       CASE
         WHEN en LIKE 'PUSH%' OR en = 'ON_LOAD_INAPP_NOTIFICATION_SCREEN' THEN LEFT (cmpid,24)
         ELSE enid
       END AS enid,
       grpid,
       cardindx,
       enidarr,
       cardidxarr,
       pidarr,
       'Product Page Screen' AS state,
       'target' AS state_type,
       url
FROM customer_app_events ce
WHERE en = 'ON_LOAD_PRODUCT_SCREEN'
AND   DATE (ets +INTERVAL '5:30:00') = CURRENT_DATE;

COMMIT;

INSERT INTO event_wise_target_source_v1
WITH all_events_updated
AS
(SELECT *,
       ROW_NUMBER() OVER (PARTITION BY uid,state_type ORDER BY ets) AS state_rank,
       ROW_NUMBER() OVER (PARTITION BY uid ORDER BY ets) AS event_rank
FROM all_events_click_stream_),target_events AS (SELECT * FROM all_events_updated WHERE state_type = 'target' and date(ets)=current_date), 
source_events AS (SELECT * FROM all_events_updated WHERE state_type = 'source')

SELECT t.sid AS target_sid,
       t.uid,
       t.ets,
       t.eid AS target_eid,
       t.ltvid AS target_ltvid,
       'Product Page View' AS target_state,
       CASE
         WHEN t.state = 'POR' THEN 'Post Order recommendation'
         WHEN b.target_ltvid IS NOT NULL THEN b.source_state
         ELSE s.state
       END AS source_state,
       CASE WHEN b.target_ltvid IS NOT NULL THEN b.source_eid
         ELSE s.eid END AS source_eid,
       b.source_ltvid AS source_ltvid,
        CASE WHEN b.target_ltvid IS NOT NULL THEN b.source_enid
         ELSE s.enid END AS source_enid,
       CASE WHEN b.target_ltvid IS NOT NULL THEN b.source_grpid
         ELSE s.grpid END AS source_grpid,
       CASE WHEN b.target_ltvid IS NOT NULL THEN b.source_cardindx
         ELSE s.cardindx END AS source_cardindx, 
       CASE WHEN b.target_ltvid IS NOT NULL THEN b.source_enidarr
         ELSE s.enidarr END AS source_enidarr,
       CASE WHEN b.target_ltvid IS NOT NULL THEN b.source_cardindxarr
         ELSE s.cardidxarr END AS source_cardindxarr,
       CASE WHEN b.target_ltvid IS NOT NULL THEN b.source_pidarr
         ELSE s.pidarr END AS source_pidarr ,
       t.pid AS target_pid,
       CASE WHEN b.target_ltvid IS NOT NULL THEN b.source_url
         ELSE s.url END AS source_url
FROM target_events t
LEFT JOIN event_wise_target_source_v1 b ON t.ltvid = b.target_ltvid
  LEFT JOIN source_events s
         ON t.uid = s.uid
        AND t.event_rank - t.state_rank = s.state_rank;

COMMIT;


EOF
echo "Ran summary script on" `date` >> ~/cronScripts/eventWiseTargetSourceV1


