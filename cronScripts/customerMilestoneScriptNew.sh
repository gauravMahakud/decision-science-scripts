#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF


cat >> ~/cronScripts/customerMilestoneLog

begin;
delete from customer_milestone2_;

insert into customer_milestone2_
select distinct s.businessid as bid,
s.onboarddatenew,extract(month from s.onboarddatenew) as onboarded_month_number,
to_char(s.onboarddatenew, 'Month') as onboarded_month,
oa.first_order_date,
oa.first_order_date-s.onboarddatenew as days_to_first_order,
act.date as activation_date,
act.date-s.onboarddatenew as days_to_activation,
oa.last_order_date,
current_date-oa.last_order_date as days_since_last_order
from business_snapshot s 
left join(select s.businessid as bid,
          date(min(o.src_created_time)) as first_order_date,
          date(max(o.src_created_time)) as last_order_date,
          count(distinct(o.src_created_time)) as number_of_order_dates
          from bolt_order_item_snapshot o 
          join customer_snapshot_ c on c.customerid=o.buyer_id
          join business_snapshot s on s.businessid=c.businessid
          where c.istestcustomer is false and  order_item_amount>0 and o.quantity>o.cancelled_units+o.returned_units+o.return_requested_quantity
          group by 1) oa on oa.bid=s.businessid
          
left join (select distinct s.businessid as bid,date(o.src_created_time), dense_rank() over (partition by s.businessid order by date(o.src_created_time)) as rank
          from bolt_order_item_snapshot o 
          join customer_snapshot_ c on c.customerid=o.buyer_id
          join business_snapshot s on s.businessid=c.businessid
          where c.istestcustomer is false and  order_item_amount>0 and o.quantity>o.cancelled_units+o.returned_units+o.return_requested_quantity) act on act.bid=s.businessid and act.rank=5;

          
insert into customer_state_audit_
select current_timestamp as update_timestamp,
current_date as effective_date,
cm.bid,
case when first_order_date is null then 'ONBOARDED' 
     when activation_date is not null and lo.last_order_date<=current_date-30 then 'CHURNED'
     when activation_date is not null and lo.last_order_date<=current_date-10 then 'ABOUT TO CHURN'
     when current_date>=first_order_date and (activation_date is null or current_date<activation_date) then 'ONBOARDED ORDERED ONCE' 
     WHEN CURRENT_DATE>=activation_date then 'ACTIVATED' end as customer_state
from 
customer_milestone2_ cm 
left join (select distinct s.businessid as bid, date(max(o.src_created_time)) as last_order_date
            from bolt_order_item_snapshot o 
            join customer_snapshot_ c on c.customerid=o.buyer_id
            join business_snapshot s on s.businessid=c.businessid
            where c.istestcustomer is false and  order_item_amount>0 and o.quantity>o.cancelled_units+o.returned_units+o.return_requested_quantity
            group by 1)lo on lo.bid=cm.bid;
commit;






EOF
echo "Ran the customer query on" `date` >> ~/cronScripts/customerMilestoneLog
