#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/eventStateLog
insert into event_customer_state_mapping_
select eid,uid,s.bid,customer_state,current_timestamp as updated_timestamp
from customer_app_events ce
join store_name s on s.customerid=ce.uid
join (select bid,case when first_order_date is null then 'ONBOARDED' 
     when activation_date is not null and last_order_date<=current_date-30 then 'CHURNED'
     when activation_date is not null and last_order_date<=current_date-10 then 'ABOUT TO CHURN'
     when current_date>=first_order_date and (activation_date is null or current_date<activation_date) then 'ONBOARDED ORDERED ONCE' 
     WHEN CURRENT_DATE>=activation_date then 'ACTIVATED' end as customer_state
     from customer_milestone_ cm )cm on cm.bid=s.bid
join (select max(updated_timestamp) from event_customer_state_mapping_) fil on ce.ets> fil.max;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/eventStateLog
