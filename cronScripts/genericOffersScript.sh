#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/genericOffersLog
drop table if exists generic_offers_perf;
WITH pos1 AS
(
SELECT jos.jtoffer_id,
       jos.status,
       jos.starttime,
       (CASE WHEN jos.status = 'ACTIVE' THEN jos.endtime WHEN jos.status = 'INACTIVE' THEN a.max_inactive_ts END) AS end_time,
       json_extract_array_element_text(jos.listingids,0) AS offer_listing_id,
       ps.jpin,
       ps.title
FROM jtoffer_snapshot jos
  JOIN listing_snapshot l ON l.listing_id = json_extract_array_element_text (jos.listingids,0)
  JOIN sellerproduct_snapshot sp ON sp.sp_id = l.sp_id
  JOIN product_snapshot_ ps ON sp.jpin = ps.jpin
  LEFT JOIN (SELECT jtoffer_id,
                    MAX(inactive_time) AS max_inactive_ts
             FROM (SELECT a.jtoffer_id,
                          b.srclastupdatedtime AS inactive_time
                   FROM jtoffer_audit a
                     JOIN jtoffer_audit b
                       ON a.jtoffer_id = b.jtoffer_id
                      AND a.status = 'ACTIVE'
                      AND b.status = 'INACTIVE'
                      AND a.version = b.version - 1
                   ORDER BY 1)
             GROUP BY 1) a ON a.jtoffer_id = jos.jtoffer_id
WHERE bztargetgroupid = 'NONE'
),
pos2 AS
(
  SELECT--DATE (convert_timezone ('GMT','NEWZONE -5:30',ce.ets)) AS event_date,
         --    cs.businessid,
         fi.enid,
         COUNT(DISTINCT (CASE WHEN ce.etype != 'BG' AND en NOT LIKE 'PUSH_%' THEN cs.businessid END)) AS came_to_page,
         COUNT(DISTINCT (CASE WHEN en = 'ON_LOAD_PRODUCT_SCREEN' THEN cs.businessid END)) AS opened_product_view,
         COUNT(DISTINCT (CASE WHEN en = 'ON_ADD_CART_ITEM' THEN cs.businessid END)) AS added_to_cart,
         COUNT(DISTINCT (CASE WHEN en = 'ON_TAP_GO_TO_CART' OR en = 'ON_TAP_CART_BAR' THEN cs.businessid END)) AS went_to_cart,
         COUNT(DISTINCT (CASE WHEN bo.cartitemid IS NOT NULL THEN cs.businessid END)) AS ordered
  -- CONVERT(DECIMAL(10,2),COUNT(DISTINCT (CASE WHEN bo.cartitemid IS NOT NULL THEN cs.businessid END))) / CONVERT(DECIMAL(10,2),COUNT(DISTINCT (CASE WHEN ce.etype != 'BG' AND en NOT LIKE 'PUSH_%' THEN cs.businessid END))) AS conversion
         FROM (SELECT etype,
                      en,
                      uid,
                      ets,
                      crtid,
                      pid,
                      sid
               FROM customer_app_events
               WHERE DATE (ets +INTERVAL '05:30:00') >= DATE (CURRENT_DATE) -30
               --       AND   etype <> 'BG'
               AND   en IN ('ON_LOAD_PRODUCT_LIST_SCREEN','ON_LOAD_PRODUCT_SCREEN','ON_ADD_CART_ITEM','ON_TAP_GO_TO_CART','ON_TAP_CART_BAR')) ce
    JOIN customer_snapshot_ cs ON cs.customerid = ce.uid
    JOIN (SELECT br.sid,
                 br.ets,
                 br.enid,
                 MIN(hp.ets)
          FROM (SELECT ets,
                       sid,
                       enid
                FROM customer_app_events
                WHERE en = 'ON_CLICK_BROWSE_ITEM'
                AND   penid = 'HomePage'
                AND   grpid = 'UINODE-Offers4U'
                AND   DATE (ets +INTERVAL '05:30:00') >= DATE (CURRENT_DATE) -30) br
            JOIN (SELECT ce.ets,
                         eid,
                         sid
                  FROM customer_app_events ce
                    JOIN customer_snapshot_ c ON c.customerid = ce.uid
                  WHERE c.istestcustomer IS FALSE
                  AND   en = 'ON_LOAD_BROWSE_SCREEN'
                  AND   DATE (ets +INTERVAL '05:30:00') >= DATE (CURRENT_DATE) -30
                  AND   enid = 'HomePage') hp
              ON hp.sid = br.sid
             AND hp.ets > br.ets
          GROUP BY 1,
                   2,
                   3) fi
      ON fi.sid = ce.sid
     AND ce.ets > fi.ets
     AND ce.ets < fi.min
    LEFT JOIN cartitem_snapshot_ crt
           ON crt.cartid = ce.crtid
          AND crt.jpin = ce.pid
          AND ce.en = 'ON_ADD_CART_ITEM'
    LEFT JOIN bolt_order_item_snapshot bo ON bo.cartitemid = crt.cartitemid
  GROUP BY 1
),
pos3 AS
(
  SELECT a.*, b.* ,count(distinct(d.businessid)) as product_viewed
  FROM pos1 a LEFT JOIN pos2 b ON a.jpin = b.enid
  left join (SELECT ets,
       businessid,
       json_extract_array_element_text(enidarr,CONVERT(INT,n.number)) AS jpin_viewed
FROM (SELECT ets,
             businessid,
             enidarr,
             JSON_ARRAY_LENGTH(enidarr) AS len
      FROM customer_app_events cae join customer_snapshot_ c on c.customerid=cae.uid
      WHERE en = 'ON_IMPRESSION_BROWSE_SCREEN'
      AND   penid = 'HomePage'
      AND   grpname = 'Offers' 
--and uid='JC-1202545539'      
) o
  JOIN numbers n ON n.number < o.len
 ) d on d.jpin_viewed=a.jpin and a.starttime <=d.ets and a.end_time>=d.ets
 group by 1,2,3,4,5,6,7,8,9,10,11,12,13
)

SELECT a.jtoffer_id,a.jpin,
       a.title,
       a.starttime,a.end_time, case when a.end_time >  getdate()+interval'5:30' then 'Active' else 'Expired' end as offer_status,
       COUNT(DISTINCT (CASE WHEN c.cta = 1 THEN c.businessid END)) AS cta,
       COUNT(DISTINCT (CASE WHEN c.saw_offers = 1 THEN c.businessid END)) AS saw_offers,product_viewed,
       opened_product_view as ppv_clicked,
       added_to_cart,
       ordered,
      case when COUNT(DISTINCT (CASE WHEN c.cta = 1 THEN c.businessid END))=0 then 0 else  COUNT(DISTINCT (CASE WHEN c.saw_offers = 1 THEN c.businessid END))*1.0 / COUNT(DISTINCT (CASE WHEN c.cta = 1 THEN c.businessid END)) end AS pct_saw_offers,
      case when COUNT(DISTINCT (CASE WHEN c.saw_offers = 1 THEN c.businessid END))=0 then 0 else  product_viewed*1.0 / COUNT(DISTINCT (CASE WHEN c.saw_offers = 1 THEN c.businessid END)) end AS pct_saw_product,
      case when product_viewed=0 then 0 else opened_product_view*1.0 / product_viewed end AS pct_ppv,
       case when opened_product_view=0 then 0 else added_to_cart*1.0 / opened_product_view end AS pct_atc,
       case when added_to_cart=0 then 0 else ordered*1.0 / added_to_cart end AS pct_ordered
       
into generic_offfers_perf
FROM pos3 a
  LEFT JOIN (SELECT businessid,
                    DATE (ets +INTERVAL '5:30') AS event_date,
                    (CASE WHEN ce.etype != 'BG' AND en NOT LIKE 'PUSH_%' THEN '1' END) AS cta,
                    (CASE WHEN en = 'ON_IMPRESSION_BROWSE_SCREEN' AND penid = 'HomePage' AND grpname = 'Offers' THEN '1' END) AS saw_offers
             FROM customer_app_events ce
               JOIN customer_snapshot_ c ON c.customerid = ce.uid
             WHERE DATE (ets +INTERVAL '5:30') > '2017-10-26'
             AND   DATE (ets +INTERVAL '5:30') <= CURRENT_DATE
             AND   ce.app_version >= '1.53'
             AND   c.businessid IS NOT NULL
             GROUP BY 1,
                      2,
                      3,
                      4) c ON DATE (a.starttime) <= c.event_date and DATE (a.end_time) >= c.event_date
WHERE enid IS NOT NULL

GROUP BY 1,
         2,
         3,
         4,5
         ,9,10,11,12;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/genericOffersLog
