#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/hourlyConversionLog
DELETE
FROM conversion_events_current_date
WHERE DATE= CURRENT_DATE ;

DROP TABLE if exists splitted_plv_plvi;

SELECT DISTINCT ets,
       uid,
       en,
       lt AS list_title,
       pidarr AS products_in_list,
       regexp_substr(TRIM(SPLIT_PART(B.pidarr,',',CONVERT(INTEGER,num.number +1))),'J[^"]*') AS JPIN INTO splitted_plv_plvi
FROM numbers num
  INNER JOIN customer_app_events B ON num.number <= REGEXP_COUNT (B.pidarr,',')
WHERE (en = 'ON_LOAD_PRODUCT_LIST_SCREEN' OR en = 'ON_IMPRESSIONS_PRODUCT_LIST_SCREEN')
AND   DATE (convert_timezone('GMT','NEWZONE -5:30',ets)) = CURRENT_DATE
AND   pidarr IS NOT NULL;

INSERT INTO conversion_events_current_date
SELECT DATE (convert_timezone ('GMT','NEWZONE -5:30',ce.ets)),
       s.bid,
       uid,
       en,
       p.jpin,
       pvname,
       category_name,
       MIN(ce.ets) AS first_time
FROM splitted_plv_plvi ce
  JOIN customer_snapshot_ c ON c.customerid = ce.uid
  LEFT JOIN store_name s ON s.customerid = ce.uid
  JOIN product_snapshot_ p ON p.jpin = ce.jpin
  JOIN (SELECT DISTINCT pvid, pvname FROM productvertical_snapshot_) pv ON pv.pvid = p.pvid
  JOIN category cat ON cat.pvid = p.pvid
WHERE en = 'ON_LOAD_PRODUCT_LIST_SCREEN'
AND   c.status = 'ACTIVE'
AND   c.istestcustomer IS FALSE
GROUP BY 1,
         2,
         3,
         4,
         5,
         6,
         7;

INSERT INTO conversion_events_current_date
SELECT DATE (convert_timezone ('GMT','NEWZONE -5:30',ce.ets)),
       s.bid,
       uid,
       en,
       p.jpin,
       pvname,
       category_name,
       MIN(ce.ets) AS first_time
FROM splitted_plv_plvi ce
  JOIN customer_snapshot_ c ON c.customerid = ce.uid
  LEFT JOIN store_name s ON s.customerid = ce.uid
  JOIN product_snapshot_ p ON p.jpin = ce.jpin
  JOIN (SELECT DISTINCT pvid, pvname FROM productvertical_snapshot_) pv ON pv.pvid = p.pvid
  JOIN category cat ON cat.pvid = p.pvid
WHERE en = 'ON_IMPRESSIONS_PRODUCT_LIST_SCREEN'
AND   DATE (convert_timezone('GMT','NEWZONE -5:30',ce.ets)) = CURRENT_DATE 
AND   c.status = 'ACTIVE'
AND   c.istestcustomer IS FALSE
GROUP BY 1,
         2,
         3,
         4,
         5,
         6,
         7;

INSERT INTO conversion_events_current_date
SELECT DATE (convert_timezone ('GMT','NEWZONE -5:30',ce.ets)),
       s.bid,
       uid,
       en,
       jpin,
       pvname,
       category_name,
       MIN(ce.ets) AS first_time
FROM customer_app_events ce
  JOIN customer_snapshot_ c ON c.customerid = ce.uid
  LEFT JOIN store_name s ON s.customerid = ce.uid
  JOIN product_snapshot_ p ON p.jpin = ce.pid
  JOIN (SELECT DISTINCT pvid, pvname FROM productvertical_snapshot_) pv ON pv.pvid = p.pvid
  JOIN category cat ON cat.pvid = p.pvid
WHERE en = 'ON_LOAD_PRODUCT_SCREEN'
AND   DATE (convert_timezone('GMT','NEWZONE -5:30',ce.ets)) = CURRENT_DATE 
AND   c.status = 'ACTIVE'
AND   c.istestcustomer IS FALSE
GROUP BY 1,
         2,
         3,
         4,
         5,
         6,
         7;

INSERT INTO conversion_events_current_date
SELECT DATE (convert_timezone ('GMT','NEWZONE -5:30',ce.ets)),
       s.bid,
       uid,
       en,
       jpin,
       pvname,
       category_name,
       MIN(ce.ets) AS first_time
FROM customer_app_events ce
  JOIN customer_snapshot_ c ON c.customerid = ce.uid
  LEFT JOIN store_name s ON s.customerid = ce.uid
  JOIN product_snapshot_ p ON p.jpin = ce.pid
  JOIN (SELECT DISTINCT pvid, pvname FROM productvertical_snapshot_) pv ON pv.pvid = p.pvid
  JOIN category cat ON cat.pvid = p.pvid
WHERE en = 'ON_ADD_CART_ITEM'
AND   DATE (convert_timezone('GMT','NEWZONE -5:30',ce.ets)) = CURRENT_DATE 
AND   c.status = 'ACTIVE'
AND   c.istestcustomer IS FALSE
GROUP BY 1,
         2,
         3,
         4,
         5,
         6,
         7;

INSERT INTO conversion_events_current_date
SELECT DATE (convert_timezone ('GMT','NEWZONE -5:30',ce.ets)),
       s.bid,
       uid,
       en,
       p.jpin,
       pvname,
       category_name,
       MIN(ce.ets) AS first_time
FROM customer_app_events ce
  JOIN customer_snapshot_ c ON c.customerid = ce.uid
  LEFT JOIN store_name s ON s.customerid = ce.uid
  JOIN bolt_order_item_snapshot bo ON ce.orid = bo.order_id
  JOIN listing_snapshot li ON li.listing_id = bo.listing_id
  JOIN sellerproduct_snapshot sp ON sp.sp_id = li.sp_id
  JOIN product_snapshot_ p ON p.jpin = sp.jpin
  JOIN (SELECT DISTINCT pvid, pvname FROM productvertical_snapshot_) pv ON pv.pvid = p.pvid
  JOIN category cat ON cat.pvid = p.pvid
WHERE en = 'ON_SUCCESS_ORDER'
AND   DATE (convert_timezone('GMT','NEWZONE -5:30',ce.ets)) = CURRENT_DATE
AND   c.status = 'ACTIVE'
AND   c.istestcustomer IS FALSE
GROUP BY 1,
         2,
         3,
         4,
         5,
         6,
         7;

DELETE
FROM daily_came_to_app
WHERE DATE = CURRENT_DATE;

INSERT INTO daily_came_to_app
SELECT DATE (convert_timezone ('GMT','NEWZONE -5:30',ce.ets)),
       s.bid,
       uid,
       MIN(ets) AS min_time
FROM customer_app_events ce
  JOIN customer_snapshot_ c ON c.customerid = ce.uid
  LEFT JOIN store_name s ON s.customerid = ce.uid
WHERE ce.etype != 'BG'
AND   en NOT LIKE 'PUSH_%'
AND   DATE (convert_timezone('GMT','NEWZONE -5:30',ce.ets)) = CURRENT_DATE 
AND   c.status = 'ACTIVE'
AND   c.istestcustomer IS FALSE
GROUP BY 1,
         2,
         3;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/hourlyConversionLog
