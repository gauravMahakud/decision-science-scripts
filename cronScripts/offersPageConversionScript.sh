#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/offersPageConversionLog
SELECT DATE (convert_timezone ('GMT','NEWZONE -5:30',ce.ets)) AS event_date,
       COUNT(DISTINCT (CASE WHEN ce.etype != 'BG' AND en NOT LIKE 'PUSH_%' THEN s.bid END)) AS came_to_page,
       COUNT(DISTINCT (CASE WHEN en = 'ON_LOAD_PRODUCT_LIST_SCREEN' THEN s.bid END)) AS opened_product_list,
       COUNT(DISTINCT (CASE WHEN en = 'ON_LOAD_PRODUCT_SCREEN' THEN s.bid END)) AS opened_product_view,
       COUNT(DISTINCT (CASE WHEN en = 'ON_ADD_CART_ITEM' THEN s.bid END)) AS added_to_cart,
       COUNT(DISTINCT (CASE WHEN en = 'ON_TAP_GO_TO_CART' OR en = 'ON_TAP_CART_BAR' THEN s.bid END)) AS went_to_cart,
       COUNT(DISTINCT (CASE WHEN bo.cartitemid IS NOT NULL THEN s.bid END)) AS ordered,
       CONVERT(DECIMAL(10,2),COUNT(DISTINCT (CASE WHEN bo.cartitemid IS NOT NULL THEN s.bid END))) / CONVERT(DECIMAL(10,2),COUNT(DISTINCT (CASE WHEN ce.etype != 'BG' AND en NOT LIKE 'PUSH_%' THEN s.bid END))) AS conversion
into Offers_page_conversion
FROM (SELECT etype,
             en,
             uid,
             ets,
             crtid,
             pid,
             sid
      FROM customer_app_events
      WHERE DATE (ets +INTERVAL '05:30:00') = DATE (CURRENT_DATE) 
--       AND   etype <> 'BG'
      AND   en IN ('ON_LOAD_PRODUCT_LIST_SCREEN','ON_LOAD_PRODUCT_SCREEN','ON_ADD_CART_ITEM','ON_TAP_GO_TO_CART','ON_TAP_CART_BAR')) ce
  JOIN store_name s ON s.customerid = ce.uid
  JOIN (SELECT br.sid,
               br.ets,
               MIN(hp.ets)
        FROM (SELECT ets,
                     sid,
                     enid
              FROM customer_app_events
              WHERE en = 'ON_LOAD_BROWSE_SCREEN'
              AND   enid = 'OffersPage' and DATE(ets +INTERVAL '05:30:00') = DATE(CURRENT_DATE)) br
          JOIN (SELECT ce.ets,
                       eid,
                       sid
                FROM customer_app_events ce
                  JOIN customer_snapshot_ c ON c.customerid = ce.uid
                WHERE c.istestcustomer IS FALSE
                AND   en = 'ON_LOAD_BROWSE_SCREEN' and DATE(ets +INTERVAL '05:30:00')=DATE (CURRENT_DATE)
                AND   enid = 'HomePage') hp
            ON hp.sid = br.sid
           AND hp.ets > br.ets
        GROUP BY 1,
                 2) fi
    ON fi.sid = ce.sid
   AND ce.ets > fi.ets
   AND ce.ets < fi.min
  LEFT JOIN cartitem_snapshot_ crt
         ON crt.cartid = ce.crtid
        AND crt.jpin = ce.pid
        AND ce.en = 'ON_ADD_CART_ITEM'
  LEFT JOIN bolt_order_item_snapshot bo ON bo.cartitemid = crt.cartitemid
GROUP BY 1;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/offersPageConversionLog
