#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/dailyActiveJpinsLog
INSERT INTO daily_active_jpins
SELECT DISTINCT DATE (CURRENT_TIMESTAMP+INTERVAL '5:30'),
       (ajt.jpin),
       ajt.title,
       null,
       ajt.pvname,
       ajt.pvid
FROM (SELECT sp.jpin,
             p.title,
             pvs.pvname,
             pvs.pvid,
             COUNT(DISTINCT (li.listing_id))
      FROM listing_inventory_snapshot li
        JOIN listing_snapshot l ON li.listing_id = l.listing_id
        JOIN sellerproduct_snapshot sp ON sp.sp_id = l.sp_id
        JOIN product_snapshot_ p ON p.jpin = sp.jpin
        JOIN (SELECT DISTINCT pvid, pvname FROM productvertical_snapshot_) pvs ON p.pvid = pvs.pvid
      --left join org_profile_snapshot ops on sp.org_id = ops.org_profile_id
      WHERE li.status = 'ACTIVE'
      AND   l.channel_id = 'ORGPROF-1304473229'
      AND   p.catalogstatus = 'Active'
      AND   l.owner_id IN (SELECT DISTINCT seller_id
                           FROM bolt_order_item_snapshot ord
                             JOIN customer_snapshot_ c ON ord.buyer_id = c.customerid
                             LEFT JOIN business_snapshot bs ON bs.businessid = c.businessid
                           WHERE c.istestcustomer IS FALSE
                           AND   order_item_amount > 0
                           AND   ord.quantity > ord.cancelled_units + ord.returned_units + ord.return_requested_quantity
                           AND   (c.status = 'ACTIVE' OR c.status = 'ONHOLD')
                           GROUP BY 1)
      GROUP BY 1,
               2,
               3,
               4) ajt
  --LEFT JOIN jpin_kam_mapping jkm ON ajt.jpin = jkm.jpin;
  ;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/dailyActiveJpinsLog
