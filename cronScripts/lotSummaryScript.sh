#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/lotSummaryLog 
delete from lot_summary_;
insert into lot_summary_
select l.lotid,
l.jpin,
p.title as product_title,
p.barcode,
p.pvid,
pv.pvname as pv_name,
p.brandid,
b.displaytitle as brand_name,
p.productlineid,
pl.productlinename as productline_name,
cat.category_name,
p.catalogstatus,
l.isactive,
l.maxorderquantity,
l.minorderquantity,
l.minorderquantitymultiples,
l.mrp,
l.sellerid,
s.name as seller_name,
l.srccreatedtime,
l.srclastupdatedtime,
lsp.sellingprice,
la.availablequantity
from lot_snapshot_ l
join lotavailability_snapshot_ la on la.lotid=l.lotid
join lotsellingprice_snapshot_ lsp on lsp.lotid=l.lotid
join product_snapshot_ p on p.jpin=l.jpin
left join category cat on p.pvid = cat.pvid     
left join brand_snapshot_ b on b.brandid=p.brandid
left join (select distinct(pvid),pvname from productvertical_snapshot_) pv on pv.pvid=p.pvid
left join productline_snapshot_ pl on pl.productlineid=p.productlineid
join seller_snapshot_ s on s.sellerid=l.sellerid;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/lotSummaryLog
