#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/mtboLog
DROP TABLE if exists mtbo_bid_30_days;

DROP TABLE if exists mtbo_bid_till_date;

WITH temp1_ AS
(
  SELECT bid ,
         order_date,
         RANK() OVER (ORDER BY bid,order_date) AS rank_r
  FROM (SELECT DISTINCT businessid AS bid,
               DATE (ord.src_created_time) AS order_date
        FROM bolt_order_item_snapshot ord
          JOIN customer_snapshot_ c ON c.customerid = ord.buyer_id
        WHERE c.istestcustomer IS FALSE
        AND   ord.quantity>ord.cancelled_units+ord.returned_units+ord.return_requested_quantity and (c.status='ACTIVE' or  c.status='ONHOLD'))
)
SELECT a.bid,
       AVG((a.order_date - b.order_date)) AS TBO INTO mtbo_bid_till_date
FROM temp1_ a
  LEFT JOIN temp1_ b
         ON a.bid = b.bid
        AND a.rank_r = (b.rank_r +1)
WHERE a.order_date - b.order_date IS NOT NULL
GROUP BY 1;

WITH temp1_ AS
(
  SELECT bid,
         order_date,
         RANK() OVER (ORDER BY bid,order_date) AS rank_r
  FROM (SELECT DISTINCT businessid AS bid,
               DATE (ord.src_created_time) AS order_date
        FROM bolt_order_item_snapshot ord
          JOIN customer_snapshot_ c ON c.customerid = ord.buyer_id
        WHERE c.istestcustomer IS FALSE
        AND   ord.quantity>ord.cancelled_units+ord.returned_units+ord.return_requested_quantity and (c.status='ACTIVE' or  c.status='ONHOLD'))

)
SELECT a.bid,
       AVG((a.order_date - b.order_date)) AS TBO INTO mtbo_bid_30_days
FROM temp1_ a
  LEFT JOIN temp1_ b
         ON a.bid = b.bid
        AND a.rank_r = (b.rank_r +1)
WHERE a.order_date - b.order_date IS NOT NULL
AND   a.order_date >= CURRENT_DATE -30
GROUP BY 1;
\o
grant all on mtbo_bid_till_date to public;
grant all on mtbo_bid_30_days to public;
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/mtboLog


