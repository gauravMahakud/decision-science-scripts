import requests
import json
headers = {"Authorization": "Bearer ya29.GltjBiFCXClNiPFg0aaJ_rbd9Iw1piL79xm7LZFJEgUhu_Qm9_06uzyvBlv-IyAOTHVl6g4jNJ7cvYqx7RXtqUFip6m5ihvBS5Un3HEFPwSiKSUowQKBdjpNQHRD"} #put ur access token after the word 'Bearer '
para = {
    "name": "test.txt", #file name to be uploaded
    "parents": ["1Q8Rd_2PSawNinTW2EJUBoaf2Uo7kCqo9"] # make a folder on drive in which you want to upload files; then open that folder; the last thing in present url will be folder id
}
files = {
    'data': ('metadata', json.dumps(para), 'application/json; charset=UTF-8'),
    'file': ('text/txt',open("./test.txt", "rb")) # replace 'application/zip' by 'image/png' for png images; similarly 'image/jpeg' (also replace your file name)
}
r = requests.post(
    "https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart",
    headers=headers,
    files=files
)
print(r.text)
