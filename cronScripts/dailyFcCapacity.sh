SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/dailyFcCapacity


insert into daily_fc_capacity 
select calendardate,facility,capacity_type, capacity, buffer_capacity
from calendar c 
join fc_capacity_thresholds  on 1=1
where calendardate>current_date and calendardate<=current_date+15 and concat(calendardate,facility) not in (select distinct concat(processing_date,facility_id) from daily_fc_capacity) ;
Commit;




EOF
echo "Ran summary script on" `date` >> ~/cronScripts/dailyFcCapacity



