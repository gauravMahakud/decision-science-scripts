#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/productSummaryLog
delete from product_summary_;
insert into product_summary_
select p.jpin,
p.title as product_title,
p.barcode,
p.pvid,
pv.pvname as pv_name,
p.brandid,
b.displaytitle as brand_name,
p.productlineid,
pl.productlinename as productline_name,
cat.category_name,
p.catalogstatus,
p.srccreatedtime
from product_snapshot_ p
    left join category cat on p.pvid = cat.pvid     
    left join brand_snapshot_ b on b.brandid=p.brandid
    left join (select distinct(pvid),pvname from productvertical_snapshot_) pv on pv.pvid=p.pvid
    left join productline_snapshot_ pl on pl.productlineid=p.productlineid;
\o	
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/productSummaryLog
