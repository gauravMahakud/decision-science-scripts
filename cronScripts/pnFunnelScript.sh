SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/pnFunnelLog



insert into pn_funnel

with pn_events_display_cmpgn
as
(
select distinct date(ets + interval '5:30:00'), ets + interval '5:30:00' as ts, eid, c.phonenumber, c.businessid, url,
       regexp_substr (url, '[C|U][^"]*') as cmp
from customer_app_events cae
join customer_snapshot_ c on c.customerid = cae.uid and c.istestcustomer is false and c.businessid not like '%ech%' AND c.businessid not like '%uja%' and c.businessid not like '%sajal%'
where cae.en = 'PUSH_NOTIFICATION_DISPLAYED' and date(ets + interval '5:30:00') = current_date and regexp_substr (url, '[C|U][^"]*') like '%CMPGN%'
),
pn_events_display_uin
as
(
select distinct date(ets + interval '5:30:00'), ets + interval '5:30:00' as ts, eid, c.phonenumber, c.businessid, url,
       regexp_substr (url, '[C|U][^"]*') as uin
from customer_app_events cae
join customer_snapshot_ c on c.customerid = cae.uid and c.istestcustomer is false and c.businessid not like '%ech%' AND c.businessid not like '%uja%' and c.businessid not like '%sajal%'
where cae.en = 'PUSH_NOTIFICATION_DISPLAYED' and date(ets + interval '5:30:00') = current_date and regexp_substr (url, '[C|U][^"]*') like '%UINODE%'
),
cta_pn
as
(
select distinct date(ets + interval '5:30:00'), ets + interval '5:30:00' as ts, eid, phonenumber, businessid, url
from customer_app_events cae
join customer_snapshot_ c on c.customerid = cae.uid and c.istestcustomer is false and c.businessid not like '%ech%' AND c.businessid not like '%uja%' and c.businessid not like '%sajal%'
where cae.en = 'PUSH_NOTIFICATION_OPEN' and date(ets + interval '5:30:00') = current_date)
,
pn_dismiss
as
(
select distinct date(ets + interval '5:30:00'), ets + interval '5:30:00' as ts, eid, phonenumber, businessid, url
from customer_app_events cae
join customer_snapshot_ c on c.customerid = cae.uid and c.istestcustomer is false and c.businessid not like '%ech%' AND c.businessid not like '%uja%' and c.businessid not like '%sajal%'
where cae.en = 'PUSH_NOTIFICATION_DISMISSED' and date(ets + interval '5:30:00') = current_date
)

select camp.date, split_part(camp.cmp, '&',1) as cmpid, camp.campaignname, count(distinct camp.businessid) as display_business, count(distinct camp.phonenumber) as display_jc, 
       count(distinct case when cp.phonenumber is not null then camp.businessid end) as click_business, count(distinct case when cp.phonenumber is not null then camp.phonenumber end)  as click_jc,
       count(distinct case when pd.phonenumber is not null then camp.businessid end) as dismiss_business, count(distinct case when pd.phonenumber is not null then camp.phonenumber end) as dismiss_jc,
       min(camp.ts) as min_display_time, max(camp.ts) as max_display_time, min(cp.ts) as min_click_time, max(cp.ts) as max_click_time, min(pd.ts) as min_dismiss_time,
       max(pd.ts) as max_dismiss_time

from
(
select pedc.*, cs.internalcampaignname as campaignname
from pn_events_display_cmpgn pedc
left join campaign_snapshot_ cs on cs.campaignid = split_part(pedc.cmp, '&',1)
union
select pedu.*, uin.internalname as campaignname
from pn_events_display_uin pedu
left join uinode_snapshot_ uin on uin.uinodeid = split_part(pedu.uin, '&',1)
) camp
left join cta_pn cp on cp.url = camp.url and cp.phonenumber = camp.phonenumber
left join pn_dismiss pd on pd.url = camp.url and pd.phonenumber = camp.phonenumber
group by 1,2,3
;

commit;


\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/pnFunnelLog

