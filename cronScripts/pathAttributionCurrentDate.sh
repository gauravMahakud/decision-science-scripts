SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/pathAttributionCurrentDateLog

drop table if exists all_events_pa_new;

SELECT sid,
       uid,
       ce.ets +INTERVAL '5:30:00' AS ets,
       eid,
       ltvid,
       CASE
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386154371' THEN 'HomePage Banner'
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386160562' THEN 'Top Market Traders'
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386154396' THEN 'Top Brands'
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386155111' THEN 'All time Special Price'
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'OffersPage' THEN 'Offers Page'
         WHEN en = 'ON_LOAD_ALL_BRANDS_SCREEN' THEN 'All Brands Page'
                 WHEN en='ON_SUBMIT_SEARCH_QUERY' or en='ON_SUBMIT_AUTO_SUGGEST' then 'Search'
                 WHEN en='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386155282' then 'Jumbotail Exclusive'
                 when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159035' then 'Cat Pg Bnr-Flours'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159032' then 'Cat Pg Bnr-Pulses'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159067' then 'Cat Pg Bnr-Salt'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159071' then 'Cat Pg Bnr-Rava/Sooji'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159066' then 'Cat Pg Bnr-Masalas'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159069' then 'Cat Pg Bnr-Whole Grains'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159068' then 'Cat Pg Bnr-Whole Spices'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159070' then 'Cat Pg Bnr-Dry Fruits'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386158804' then 'Cat Pg Bnr-Rice'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386158885' then 'Cat Pg Bnr-Oil'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159256' then 'Cat Pg Bnr-Ghee'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159175' then 'Cat Pg Bnr-Biscuits'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159193' then 'Cat Pg Bnr-BF Cereals'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159171' then 'Cat Pg Bnr-Fruit Juices'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159172' then 'Cat Pg Bnr-Snacks'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159174' then 'Cat Pg Bnr-Chocolates'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159192' then 'Cat Pg Bnr-Sauces'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159195' then 'Cat Pg Bnr-Ready2Cook'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159173' then 'Cat Pg Bnr-Tea'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159170' then 'Cat Pg Bnr-Coffee'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159194' then 'Cat Pg Bnr-OtherPckFood'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159183' then 'Cat Pg Bnr-Paste/brush'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159180' then 'Cat Pg Bnr-Soap/Shampoo'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386155520' then 'Cat Pg Bnr-HairOil/Gel'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386155733' then 'Cat Pg Bnr-Men Grooming'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154098' then 'Cat Pg Bnr-Fem-Hygiene'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154104' then 'Cat Pg Bnr-ToiletClean'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386155734' then 'Cat Pg Bnr-SurfaceClean'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154086' then 'Cat Pg Bnr-Detergents'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154105' then 'Cat Pg Bnr-Dishwashers'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154108' then 'Cat Pg Bnr-AirFreshen'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154106' then 'Cat Pg Bnr-Shoe Polish'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154109' then 'Cat Pg Bnr-Repellents'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154107' then 'Cat Pg Bnr-CleanAccess'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386152095' then 'Cat Pg Bnr-ScrubNSponge'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386152094' then 'Cat Pg Bnr-Agarbattis'
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' THEN 'Category Page'
         WHEN en = 'PUSH_NOTIFICATION_OPEN'  THEN 'Push Notification'
                 when en='ON_APP_CAME_TO_FG' and lnchrfrl ='DEEP_LINK' then 'SMS or Whatsapp'
         WHEN en in ('ON_SUBMIT_SEARCH_QUERY','ON_SUBMIT_AUTO_SUGGEST','ON_SUBMIT_RECENT_SEARCH') then 'Search'
         when en='ON_LOAD_INAPP_NOTIFICATION_SCREEN' then 'In-App_notification'
         ELSE 'unknown'
       END AS state,
       'source' AS state_type
       INTO all_events_pa_new
FROM customer_app_events ce
WHERE ((en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386154371')
OR (en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386154396')
OR (en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'OffersPage')
OR (en = 'ON_LOAD_ALL_BRANDS_SCREEN')
OR (en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage')
OR (en = 'PUSH_NOTIFICATION_OPEN' )
OR (en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386155111')
OR (en='ON_SUBMIT_SEARCH_QUERY' or en='ON_SUBMIT_AUTO_SUGGEST' or en='ON_SUBMIT_RECENT_SEARCH')
or en in ('ON_SUBMIT_SEARCH_QUERY','ON_SUBMIT_AUTO_SUGGEST')
or en='ON_LOAD_INAPP_NOTIFICATION_SCREEN'
or (en='ON_APP_CAME_TO_FG' and lnchrfrl ='DEEP_LINK')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159035')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159032')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159067')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159071')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159066')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159069')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159068')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159070')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386158804')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386158885')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159256')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159175')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159193')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159171')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159172')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159174')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159192')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159195')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159173')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159170')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159194')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159183')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159180')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386155520')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386155733')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154098')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154104')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386155734')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154086')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154105')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154108')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154106')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154109')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154107')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386152095')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386152094')
or (en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386160562'))
AND   DATE (ets +INTERVAL '5:30:00') = current_date ;

INSERT INTO all_events_pa_new
SELECT sid,
       uid,
       ce.ets +INTERVAL '5:30:00',
       eid,
       ltvid,
       CASE
         WHEN en = 'ON_LOAD_PRODUCT_LIST_SCREEN' THEN 'PLV'
         ELSE 'POR'
       END,
       'target'
       FROM customer_app_events ce
WHERE (en = 'ON_LOAD_PRODUCT_LIST_SCREEN' OR en = 'ON_LOAD_POST_ORDER_RECOMMENDATION_LIST')
AND   DATE (ets +INTERVAL '5:30:00') = current_date;

DELETE
FROM event_wise_target_source_today
WHERE DATE (ets) = current_date;

INSERT INTO event_wise_target_source_today
WITH all_events_updated
AS
(SELECT *,
       ROW_NUMBER() OVER (PARTITION BY uid,state_type ORDER BY ets) AS state_rank,
       ROW_NUMBER() OVER (PARTITION BY uid ORDER BY ets) AS event_rank
FROM all_events_pa_new),target_events AS (SELECT * FROM all_events_updated WHERE state_type = 'target'),
source_events AS (SELECT * FROM all_events_updated WHERE state_type = 'source')
SELECT t.sid,
       t.uid,
       t.ets,
       t.eid,
       t.ltvid AS target_identifier,
       'Product List View' AS state,
       CASE
         WHEN t.state = 'POR' THEN 'Post Order recommendation'
         ELSE s.state
       END AS source_state
FROM target_events t
  LEFT JOIN source_events s
         ON t.uid = s.uid
        AND t.event_rank - t.state_rank = s.state_rank;

DROP TABLE if exists all_events_pa_new_;

SELECT sid,
       uid,
       ce.ets +INTERVAL '5:30:00' as ets,
       eid,pid,
       ltvid as state,
       'target' as state_type INTO all_events_pa_new_
FROM customer_app_events ce
WHERE en = 'ON_LOAD_PRODUCT_SCREEN'
AND   DATE (ets +INTERVAL '5:30:00') = current_date;

INSERT INTO all_events_pa_new_
SELECT sid,
       uid,
       ce.ets +INTERVAL '5:30:00' AS ets,
       eid,pid,
       CASE
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386154371' THEN 'HomePage Banner'
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386160562' THEN 'Top Market Traders'
WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386154396' THEN 'Top Brands'
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386155111' THEN 'All time Special Price'
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'OffersPage' THEN 'Offers Page'
         WHEN en = 'ON_LOAD_ALL_BRANDS_SCREEN' THEN 'All Brands Page'
                 WHEN en='ON_SUBMIT_SEARCH_QUERY' or en='ON_SUBMIT_AUTO_SUGGEST' or en='ON_SUBMIT_RECENT_SEARCH' then 'Search'
         WHEN en = 'PUSH_NOTIFICATION_OPEN'  THEN 'Push Notification'
         when en='ON_SUCCESS_SCAN' then 'Scanned Image'
         when en='ON_LOAD_MY_ORDERS_SCREEN' then 'Order History'
         WHEN grpid = 'UINODE-11386154067' and enid LIKE 'JPIN-%' AND en = 'ON_CLICK_BROWSE_ITEM' THEN 'Top Sellers'
         WHEN grpid = 'UINODE-11386155173' and enid LIKE 'JPIN-%' AND en = 'ON_CLICK_BROWSE_ITEM' THEN 'Products for you'
         WHEN grpid = 'UINODE-Offers4U' and enid LIKE 'JPIN-%' AND en = 'ON_CLICK_BROWSE_ITEM' THEN 'Offers for you'
         WHEN grpid = 'UINODE-similarProduct' and enid LIKE 'JPIN-%' AND en = 'ON_CLICK_BROWSE_ITEM' THEN 'Similar Products'
         WHEN grpid = 'UINODE-newLaunch' and enid LIKE 'JPIN-%' AND en = 'ON_CLICK_BROWSE_ITEM' THEN 'New Launch HP Module'
         WHEN grpid = 'UINODE-backInStock' and enid LIKE 'JPIN-%' AND en = 'ON_CLICK_BROWSE_ITEM' THEN 'Back in Stock HP Module'
         WHEN grpid = 'UINODE-bestPrice' and enid LIKE 'JPIN-%' AND en = 'ON_CLICK_BROWSE_ITEM' THEN 'Best Price HP Module'
         WHEN grpid = 'UINODE-11386155282' and enid LIKE 'JPIN-%' AND en = 'ON_CLICK_BROWSE_ITEM' THEN 'Jumbotail Exclusive'
                 when en='ON_APP_CAME_TO_FG' and lnchrfrl ='DEEP_LINK' then 'SMS or Whatsapp'
                 when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159035' then 'Cat Pg Bnr-Flours'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159032' then 'Cat Pg Bnr-Pulses'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159067' then 'Cat Pg Bnr-Salt'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159071' then 'Cat Pg Bnr-Rava/Sooji'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159066' then 'Cat Pg Bnr-Masalas'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159069' then 'Cat Pg Bnr-Whole Grains'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159068' then 'Cat Pg Bnr-Whole Spices'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159070' then 'Cat Pg Bnr-Dry Fruits'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386158804' then 'Cat Pg Bnr-Rice'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386158885' then 'Cat Pg Bnr-Oil'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159256' then 'Cat Pg Bnr-Ghee'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159175' then 'Cat Pg Bnr-Biscuits'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159193' then 'Cat Pg Bnr-BF Cereals'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159171' then 'Cat Pg Bnr-Fruit Juices'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159172' then 'Cat Pg Bnr-Snacks'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159174' then 'Cat Pg Bnr-Chocolates'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159192' then 'Cat Pg Bnr-Sauces'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159195' then 'Cat Pg Bnr-Ready2Cook'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159173' then 'Cat Pg Bnr-Tea'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159170' then 'Cat Pg Bnr-Coffee'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159194' then 'Cat Pg Bnr-OtherPckFood'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159183' then 'Cat Pg Bnr-Paste/brush'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159180' then 'Cat Pg Bnr-Soap/Shampoo'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386155520' then 'Cat Pg Bnr-HairOil/Gel'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386155733' then 'Cat Pg Bnr-Men Grooming'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154098' then 'Cat Pg Bnr-Fem-Hygiene'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154104' then 'Cat Pg Bnr-ToiletClean'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386155734' then 'Cat Pg Bnr-SurfaceClean'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154086' then 'Cat Pg Bnr-Detergents'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154105' then 'Cat Pg Bnr-Dishwashers'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154108' then 'Cat Pg Bnr-AirFreshen'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154106' then 'Cat Pg Bnr-Shoe Polish'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154109' then 'Cat Pg Bnr-Repellents'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154107' then 'Cat Pg Bnr-CleanAccess'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386152095' then 'Cat Pg Bnr-ScrubNSponge'
when en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386152094' then 'Cat Pg Bnr-Agarbattis'
        WHEN grpid = 'UINODE-compProduct' AND enid LIKE 'JPIN-%' AND en = 'ON_CLICK_BROWSE_ITEM' THEN 'Complementary Products'
         WHEN en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' THEN 'Category Page'
                 ELSE 'unknown'
       END AS state,
       'source' AS state_type
FROM customer_app_events ce
WHERE ((en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386154371')
OR (en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386154396')
OR (en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'OffersPage')
OR (en = 'ON_LOAD_ALL_BRANDS_SCREEN')
OR (en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage')
OR (en = 'PUSH_NOTIFICATION_OPEN' )
OR (en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386155111')
OR (en='ON_SUCCESS_SCAN')
or (en='ON_LOAD_MY_ORDERS_SCREEN')
or (en = 'ON_CLICK_BROWSE_ITEM' and grpid ='UINODE-similarProduct' )
or (en='ON_SUBMIT_SEARCH_QUERY' or en='ON_SUBMIT_AUTO_SUGGEST' or en='ON_SUBMIT_RECENT_SEARCH')
or (en='ON_APP_CAME_TO_FG' and lnchrfrl ='DEEP_LINK' )
or (grpid = 'UINODE-compProduct' AND enid LIKE 'JPIN-%' AND en = 'ON_CLICK_BROWSE_ITEM')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159035')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159032')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159067')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159071')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159066')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159069')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159068')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159070')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386158804')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386158885')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159256')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159175')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159193')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159171')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159172')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159174')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159192')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159195')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159173')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159170')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159194')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159183')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386159180')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386155520')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386155733')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154098')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154104')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386155734')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154086')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154105')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154108')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154106')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154109')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386154107')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386152095')
or (en ='ON_CLICK_BROWSE_ITEM' and grpid = 'UINODE-11386152094')
or (en = 'ON_CLICK_BROWSE_ITEM' AND penid = 'HomePage' AND grpid = 'UINODE-11386160562')) AND   DATE (ets +INTERVAL '5:30:00') = current_date;




INSERT INTO event_wise_target_source_today
WITH all_events_updated
AS
(SELECT *,
       ROW_NUMBER() OVER (PARTITION BY uid,state_type ORDER BY ets) AS state_rank,
       ROW_NUMBER() OVER (PARTITION BY uid ORDER BY ets) AS event_rank
FROM all_events_pa_new_),target_events AS (SELECT * FROM all_events_updated WHERE state_type = 'target'),
source_events AS (SELECT * FROM all_events_updated WHERE state_type = 'source')
SELECT t.sid,
t.uid,
       t.ets,
       t.eid,
       t.pid AS target_identifier,
       'Product Page View' AS state,
       CASE
         WHEN b.target_identifier is not null then b.source_state
         ELSE s.state
       END AS source_state,t.pid
FROM target_events t
left join event_wise_target_source_today b ON t.state = b.target_identifier
  LEFT JOIN source_events s
         ON t.uid = s.uid
        AND t.event_rank - t.state_rank = s.state_rank;


INSERT INTO event_wise_target_source_today
SELECT DISTINCT sid,
       uid,
       ets,
       eid,
       cartitemid,
       'Add to Cart',
       NULL,
       pid
FROM (SELECT sid,
             uid,
             ets +INTERVAL '5:30:00' AS ets,
             eid,
             crtid,
             pid,
             DENSE_RANK() OVER (PARTITION BY crtid,pid ORDER BY ets) AS RANK
      FROM customer_app_events
      WHERE DATE (ets +INTERVAL '5:30:00') = current_date
      AND   en = 'ON_ADD_CART_ITEM') ca
  JOIN (SELECT cartid,
               jpin,
               cartitemid,
               status,
               DENSE_RANK() OVER (PARTITION BY cartid,jpin ORDER BY srccreatedtime) AS RANK
        FROM cartitem_snapshot_) cis
    ON cis.cartid = ca.crtid
   AND cis.jpin = ca.pid
   AND cis.rank = ca.rank;

INSERT INTO event_wise_target_source_today
WITH data_struc AS
(SELECT *,
 ROW_NUMBER() OVER (PARTITION BY uid,jpin,state ORDER BY ets) AS state_rank,
       ROW_NUMBER() OVER (PARTITION BY uid,jpin ORDER BY ets) AS event_rank
FROM event_wise_target_source_today
WHERE (state = 'Add to Cart' OR state = 'Product Page View') and date(ets)=current_date)
SELECT a.sid,
       a.uid,
       a.ets,
       a.eid,
       a.target_identifier,
       'Add to Cart',
       b.source_state,
       a.jpin
FROM (SELECT * FROM data_struc WHERE state = 'Add to Cart') a
  JOIN (SELECT * FROM data_struc WHERE state = 'Product Page View') b
    ON a.uid = b.uid
   AND a.jpin = b.jpin
   AND a.event_rank - a.state_rank = b.state_rank;

DELETE
FROM event_wise_target_source_today
WHERE state = 'Add to Cart'
AND   source_state IS NULL;

commit;



EOF
echo "Ran summary script on" `date` >> ~/cronScripts/pathAttributionCurrentDateLog

