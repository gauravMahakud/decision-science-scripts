#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/eodStoreLevelLog
insert into eod_store_level

SELECT DATE (_metadata__timestamp),
       m.store_id,
       store_name,
       mode_of_payment,
       cash,
       cheque,
       debit_or_credit_card,
       credit_ft_cash,
       net_banking,
       wallet,
       delivery_boy,
       hub,
       TO_TIMESTAMP(TO_CHAR(DATE (_metadata__timestamp),'YYYY-mm-dd') + ' ' + check_in,'YYYY-mm-dd hh:MI:ss TT'),
       change_timestamp,
       TO_TIMESTAMP(TO_CHAR(DATE (_metadata__timestamp),'YYYY-mm-dd') + ' ' + start_time,'YYYY-mm-dd hh:MI:ss TT'),
       TO_TIMESTAMP(TO_CHAR(DATE (_metadata__timestamp),'YYYY-mm-dd') + ' ' + END_time,'YYYY-mm-dd hh:MI:ss TT'),
       trip_number,
       CAST(SUBSTRING(delivery_order,1,CASE WHEN CHARINDEX ('.',delivery_order) -1 > 0 THEN CHARINDEX ('.',delivery_order) -1 ELSE NULL END) AS INT),
       CONVERT(FLOAT,SUBSTRING(change_latlong,1,CASE WHEN CHARINDEX (',',change_latlong) -1 > 0 THEN CHARINDEX (',',change_latlong) -1 ELSE NULL END)),
       CONVERT(FLOAT,SUBSTRING(change_latlong,CHARINDEX (',',change_latlong) +1,LENGTH(change_latlong) - CHARINDEX (',',change_latlong))),
       location_accuracy,
       fundscorner_pdc,
       fundscorner_cash,
       delivery_type,getdate(),NULL,NULL,NULL,NULL,NULL,capital_float,ezetap

FROM last_mile_back_end_ m
join (SELECT DATE (_metadata__timestamp),store_id,
               MAX(_metadata_file_modified_ts)
        FROM last_mile_back_end_
        WHERE DATE (_metadata__timestamp) = CURRENT_DATE
        AND   _metadata_file_name = 'Universal Delivery App/Delivery Window Confirmation'
        GROUP BY 1,2) mlm ON mlm.date = DATE (m._metadata__timestamp)
   AND mlm.max = m._metadata_file_modified_ts 
   and mlm.store_id=m.store_id
  JOIN (SELECT DATE (_metadata__timestamp),
               MAX(_metadata_file_modified_ts)
        FROM last_mile_back_end_
        WHERE DATE (_metadata__timestamp) = CURRENT_DATE
        AND   _metadata_file_name = 'Universal Delivery App/Delivery Window Confirmation'
        GROUP BY 1) m1
    ON m1.date = mlm.date
   AND extract(epoch from m1.max -mlm.max)*1.00/60<60
WHERE _metadata_file_name = 'Universal Delivery App/Delivery Window Confirmation'
AND   m.store_id LIKE 'BZID%' ;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/eodStoreLevelLog
