import requests
import json
import os
import re
pat = re.compile(r'.*Log')
filenames = []
result = []
for root, dirs, files in os.walk("/home/ec2-user/cronScripts"):
    for filename in files:
        filenames.append(filename)

for file in filenames:
    if re.findall(pat,file):
        result.append(re.findall(pat,file)[0])

for item in result:
    headers = {"Authorization": "Bearer ya29.GltjBto-TvpXS7MhrXy40S92aZtVplyq-wErPMQ7Uuv4BGIlr3jWLrcJkgSBinrt0bs_iaRGO5fOFZa2MkEbBiZ_fNPR1Wu9FWX7gBEtc6eKlPyw9l0PabCMSPdY"} #put ur access token after the word 'Bearer '
    para = {
    "name": f"{item}", #file name to be uploaded
    "parents": ["1Q8Rd_2PSawNinTW2EJUBoaf2Uo7kCqo9"] # make a folder on drive in which you want to upload files; then open that folder; the last thing in present url will be folder id
    }
    files = {
    'data': ('metadata', json.dumps(para), 'application/json; charset=UTF-8'),
    'file': ('text/txt',open(f"./{item}", "rb")) # replace 'application/zip' by 'image/png' for png images; similarly 'image/jpeg' (also replace your file name)
    }
    r = requests.post(
    "https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart",
    headers=headers,
    files=files
    )
    print(r.text)
