#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/calculateMarginLog
delete from margin_order_level_meta
where order_date>=current_date-15;
insert into margin_order_level_meta
SELECT *
FROM (SELECT order_item_id,
             order_date,
             AVG(selling_price) AS selling_price,
             AVG(selling_price_without_tax) AS selling_price_without_tax,
             SUM(initial_qty) AS quantity,
             AVG(unit_cost_price_without_tax) AS unit_cost_price_without_tax,
             SUM(CASE WHEN unit_cost_price_without_tax IS NULL THEN unit_cost_price_without_tax ELSE unit_cost_price_without_tax END*initial_qty) / SUM(initial_qty) AS weighted_avg_cost_price,
             SUM(weighted_avg_discount*initial_qty) / SUM(initial_qty) AS weighted_avg_discount,
             AVG(selling_price_without_tax) -(SUM(CASE WHEN unit_cost_price_without_tax IS NULL THEN unit_cost_price_without_tax ELSE unit_cost_price_without_tax END*initial_qty) / SUM(initial_qty)) +(SUM(weighted_avg_discount*initial_qty) / SUM(initial_qty)) AS per_unit_jw_margin
      FROM (SELECT b.*,
                   effective_selling_price /(1 + gst) AS selling_price_without_tax,
                   sum(isnull (CASE WHEN ma.method = 'M1' THEN unit_cost_price_without_tax*discount 
                   when ma.method = 'M3' then unit_cost_price*discount 
                   WHEN ma.method = 'M2' THEN discount*b.mrp /(1 + retail_margin) 
                   WHEN ma.method = 'M4' AND hurdle = 0 THEN discount / unit_quantum 
                   WHEN ma.method = 'M5' THEN GREATEST(discount - effective_selling_price,0) 
                   WHEN ma.method = 'M6' THEN b.mrp*discount ELSE 0 END,0)) AS weighted_avg_discount
            FROM (SELECT distinct ord.order_item_id,
                         li.mrp,
                         sp.jpin,
                         fu.fulfilment_item_id,
                         fu.quantity AS fulfilment_quantity,
                         ord.selling_price,
                         inv.origin_process_ref_id,
                         inv.left_qty as initial_qty,
                         ord.buyer_id,
                         inv.natural_id,
                         ord.order_item_amount / ord.quantity AS effective_selling_price,
                         CASE
                           WHEN cp.unit_cost_price IS NULL THEN cpr.unit_cost_price
                           ELSE cp.unit_cost_price
                         END AS unit_cost_price,
                         CASE
                           WHEN cp.unit_cost_price_without_tax IS NULL THEN cpr.unit_cost_price_without_tax
                           ELSE cp.unit_cost_price_without_tax
                         END AS unit_cost_price_without_tax,
                         DATE (ord.src_created_time) AS order_date,
                         CONVERT(DECIMAL(10,2),JSON_EXTRACT_PATH_TEXT(tax_master,'gstPercentage')) / 100 AS gst,
                         CASE
                           WHEN inv.natural_id IS NULL THEN 'No inventory Item in Bradman'
                           WHEN inv.origin_process_ref_id NOT LIKE 'MIITM%' THEN 'Inventory item not created by Material Inward'
                           WHEN mi.material_inwarding_item_id IS NULL THEN 'No material inward for the orgin process MI This is very weird can happen due to Redshift sync issue'
                           WHEN mi.inwardingtype = 'RETURN' THEN 'Return Inward'
                           WHEN cp.material_inwarding_item_id IS NULL THEN 'Payment Document not mapped'
                         END AS reason_code
                  FROM bolt_order_item_snapshot ord
                    JOIN listing_snapshot li ON li.listing_id = ord.listing_id
                    JOIN sellerproduct_snapshot sp ON sp.sp_id = li.sp_id
                    JOIN fulfilment_item_snapshot fu ON fu.order_item_id = ord.order_item_id
                    LEFT JOIN dw_bradman.inventory_item inv ON inv.creator_process_ref_id = fu.fulfilment_item_id
                    LEFT JOIN material_inwarding_item_snapshot mi ON mi.material_inwarding_item_id = inv.origin_process_ref_id
                    LEFT JOIN (SELECT plid.material_inwarding_item_id,
                                      SUM(prsn.invoicequantity*prsn.unit_cost_price) / SUM(prsn.invoicequantity) AS unit_cost_price,
                                      SUM(prsn.basic_cost_price) / SUM(prsn.invoicequantity) AS unit_cost_price_without_tax
                               FROM payment_document_line_item_snapshot plid
                                 JOIN payment_document_row_snapshot_ prsn
                                   ON prsn.payment_document_id = plid.payment_document_id
                                  AND prsn.row_no = plid.physicallinenumber
                                 JOIN payment_document_snapshot pd ON pd.payment_document_id = plid.payment_document_id
                                 JOIN document_snapshot_ doc ON doc.documentid = pd.document_id
                               WHERE plid.status != 'CANCELLED'
                               GROUP BY 1) cp ON mi.material_inwarding_item_id = cp.material_inwarding_item_id
                    LEFT JOIN (SELECT calendardate,
                                      doc.jpin,
                                      last_update AS document_date,
                                      AVG(unit_cost_price_without_tax) AS unit_cost_price_without_tax,
                                      AVG(unit_cost_price) AS unit_cost_price
                               FROM (SELECT calendardate,
                                            pdli.jpin,
                                            MAX(pd.document_date) AS last_update
                                     FROM calendar c
                                        JOIN payment_document_snapshot pd ON c.calendardate >= pd.document_date
                                        JOIN payment_document_row_snapshot_ pdr ON pdr.payment_document_id = pd.payment_document_id
                                        JOIN document_snapshot_ d ON d.documentid = pd.document_id
                                        JOIN payment_document_line_item_snapshot pdli ON pd.payment_document_id = pdli.payment_document_id
                                      WHERE calendardate <= CURRENT_DATE
                                     GROUP BY 1,
                                              2) doc
                                 JOIN (SELECT pdr.jpin,
                                              pd.document_date,
                                              d.documenturl,
                                              pdr.basic_cost_price,
                                              pdr.invoicequantity,
                                              pdr.basic_cost_price / pdr.invoicequantity AS unit_cost_price_without_tax,
                                              pdr.unit_cost_price
                                       FROM payment_document_snapshot pd
                                         JOIN document_snapshot_ d ON d.documentid = pd.document_id
                                         JOIN payment_document_row_snapshot_ pdr ON pdr.payment_document_id = pd.payment_document_id
                                         JOIN payment_document_line_item_snapshot pdli
                                           ON pd.payment_document_id = pdli.payment_document_id
                                          AND pdr.row_no = pdli.physicallinenumber) meta
                                   ON meta.jpin = doc.jpin
                                  AND meta.document_date = doc.last_update
                               GROUP BY 1,
                                        2,
                                        3) cpr
                           ON cpr.jpin = sp.jpin
                          AND cpr.calendardate = date(inv.src_created_time)
                  WHERE (inv.status = 'ACTIVE' OR inv.status IS NULL)
                  AND   ord.seller_id = 'ORGPROF-1304473228'
                  AND   fu.type != 'JIT'
                  AND   ord.quantity > 0
                  AND   ord.order_item_status != 'Cancelled') b
              LEFT JOIN (SELECT *
                         FROM margin_adjustments
                         WHERE method in ('M1','M2','M3','M4','M5','M6') and  _metadata_file_modified_ts = (SELECT MAX(_metadata_file_modified_ts)
                                                             FROM margin_adjustments 
                                                             where _metadata_file_name='Backend Margins and Schemes-FMCG/Final Sheet')
                                                             and  _metadata_file_name='Backend Margins and Schemes-FMCG/Final Sheet') ma
                     ON ma.jpin = b.jpin
                    AND b.order_date >= ma.start_date
                    AND b.order_date <= CASE WHEN ma.end_date IS NULL THEN CURRENT_DATE +1 ELSE end_date END
            group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17) b
      GROUP BY 1,
               2) jw
WHERE order_date >=current_date-15;

\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/calculateMarginLog
