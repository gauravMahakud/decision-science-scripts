SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/serviceRecoveryLog
INSERT INTO customer_service_recovery_backup
SELECT CURRENT_TIMESTAMP+INTERVAL '5:30:00' AS update_timestamp,
       DATE (p.updated_promise_time) AS promised_date,
       ffmp.order_item_id,
       prod.title,
       phonenumber,
       firstname,
       ordmp.quantity AS ordered_quantity,
       ordmp.ready_to_ship_units + ordmp.shipped_unit_quantity AS getting_delivered,
       ordmp.confirmedunitquantity,
       ordmp.cancelled_units,
       ordmp.delivered_units AS already_delivered,
       CASE
         WHEN DATE (p.promised_time) < DATE (p.updated_promise_time) AND DATE (p.updated_promise_time) > CURRENT_DATE AND DATE (p.promised_time) <= CURRENT_DATE AND ordmp.confirmedunitquantity > 0 THEN 'Promise Date Changed'
         ELSE action_basis_inventory_item
       END AS action_basis_inventory_item,
       ordmp.confirmedunitquantity AS actionable_qty,
       --sum(left_qty) as actionable_qty, 
       CASE
         WHEN action_basis_inventory_item LIKE '%tomorrow%' THEN 'SMS'
         WHEN action_basis_inventory_item LIKE '%Cancel%' AND (ordmp.ready_to_ship_units + ordmp.shipped_unit_quantity = 0) AND SUM(left_qty) > 0 THEN 'P0 Call'
         WHEN action_basis_inventory_item LIKE '%Cancel%' AND (ordmp.ready_to_ship_units + ordmp.shipped_unit_quantity > 0) AND SUM(left_qty) > 0 THEN 'P1 Call'
         ELSE 'No Outbound communication'
       END AS communication_mode
--, psas.primary_owner_email_id
       ,
       facility_name
--, sum(isnull(missing_quantity,0)) as missing_quantity
       FROM (SELECT *,
                    CASE
                      WHEN (state = 'FULFILMENT' AND status = 'ACTIVE') AND promised_date <= DATE (CURRENT_DATE+INTERVAL '5 hour 30 minute') - 5 THEN 'Order older than 5 days promise. Cancel order with ITEM_NOT_AVAILABLE_VENDOR'
                      WHEN (state = 'FULFILMENT' AND status = 'ACTIVE') AND left_qty != missing_quantity AND missing_marked_id IS NOT NULL THEN 'Multiple Issues with order; Item attempted but not found at FC; Cancel with ITEM_NOT_AVAILABLE_VENDOR'
                      WHEN (state = 'FULFILMENT' AND status = 'ACTIVE') AND left_qty = missing_quantity AND missing_marked_id IS NOT NULL THEN 'Item attempted but not found at FC; Cancel with ITEM_NOT_AVAILABLE_VENDOR'
                      WHEN (space_usage_purpose IN ('INWARDING','INWARDING_STAGING') OR space_usage_purpose = 'IN_TRANSIT') AND creator_process_phase = 'FULFILMENT_ORDER_RECEIVED' THEN 'Can not be picked. Item in non-pickable area. Cancel Order. ITEM_NOT_AVAILABLE_VENDOR'
                      WHEN (state = 'FULFILMENT' AND status = 'ACTIVE') AND job_status = 'IN_PROGRESS' AND space_usage_purpose = 'IN_TRANSIT' AND ii_created_time < DATE (CURRENT_DATE+INTERVAL '5 hour 30 minute') +INTERVAL '6 hour' AND creator_process_phase IN ('FULFILMENT_CRATING','FULFILMENT_BULK_PUT') THEN 'Pick Job abandoned. Cancel with ITEM_NOT_AVAILABLE_VENDOR'
                      WHEN (state = 'LOST' AND status = 'ACTIVE') AND job_status = 'IN_PROGRESS' AND space_usage_purpose = 'IN_TRANSIT' AND ii_created_time < DATE (CURRENT_DATE+INTERVAL '5 hour 30 minute') +INTERVAL '6 hour' AND creator_process_phase IN ('FULFILMENT_CRATING','FULFILMENT_BULK_PUT') THEN 'Pick Job cannot be completed due to system issues - inventory item lost in virtual space. Cancel with ITEM_NOT_AVAILABLE_VENDOR'
                      WHEN (state = 'FULFILMENT' AND status = 'ACTIVE') AND space_usage_purpose = 'STORAGE' AND (job_status != 'COMPLETED' OR job_status IS NULL) THEN 'Item available in Storage, order not picked at FC. Delivery will be attempted tomorrow'
                      WHEN (state = 'FULFILMENT' AND status = 'ACTIVE') AND canonical_name LIKE '%JIT%' AND (job_status = 'CREATED' OR job_status IS NULL) THEN 'Item available in JIT Staging area, pick delayed. Delivery will be attempted tomorrow'
                      WHEN (state = 'FULFILMENT' AND status = 'ACTIVE') AND space_usage_purpose IN ('INWARDING','INWARDING_STAGING') AND creator_process_phase = 'PENDING_PO_FULFILMENT' AND imr_id_direct IS NOT NULL AND imr_destination_space_canonical LIKE '%JIT%' THEN 'JIT Putaway to Staging Area delayed. Delivery will be attempted tomorrow'
                    END AS action_basis_inventory_item
             --this from is basis inventory item where ACTIVE inventory items with IMR either in CREATED or null IMR against that Inventory Item.
                    FROM (SELECT ii.src_created_time  AS ii_created_time,
                                 ii.id as inventory_item_id,
                                 ii.natural_id,
                                 ii.left_qty,
                                 ii.creator_process_phase,
                                 ii.creator_process_type,
                                 ii.creator_process_ref_id,
                                 ii.product_id,
                                 s.id AS space_id,
                                 s.canonical_name,
                                 s.display_name,
                                 s.natural_id AS space_natural_id,
                                 tj.job_status,
                                 iimr.id AS imr_id_from_jobstep,
                                 iimr.to_space_id AS imr_destination_space,
                                 ss.canonical_name AS imr_destination_space_canonical,
                                 s.space_usage_purpose,
                                 iimrdi.id AS imr_id_direct,
                                 ff.fulfilment_item_id,
                                 ff.order_item_id,
                                 ii.state,
                                 ii.status,
                                 DATE (p.updated_promise_time) AS promised_date,
                                 miss.id AS missing_marked_id,
                                 miss.qty_missing + miss.qty_damaged AS missing_quantity,
                                 f.name AS facility_name
                          FROM dw_bradman.inventory_item ii
                            LEFT JOIN transfer_job_step_request tjsr
                                   ON tjsr.inventory_item_id = ii.natural_id
                                  AND tjsr.status != 'CANCELLED'
                            LEFT JOIN transfer_job tj
                                   ON tj.id = tjsr.transfer_job_id
                                  AND tj.job_status != 'CANCELLED'
                            LEFT JOIN dw_bradman.inventory_item_movement_request iimr
                                   ON iimr.id = tjsr.inventory_movement_request_id
                                  AND (iimr.status IN ('CREATED', 'COMPLETED') 
                                   
                                   OR iimr.status IS NULL)
                            LEFT JOIN fulfilment_item_snapshot ff ON ff.fulfilment_item_id = ii.creator_process_ref_id
                            LEFT JOIN space s ON s.id = ii.space_id
                            LEFT JOIN facility f ON f.id = s.facility_id
                            LEFT JOIN dw_bradman.inventory_item_movement_request iimrdi ON iimrdi.inventory_item_id = ii.natural_id
                            LEFT JOIN space ss ON ss.natural_id = iimrdi.to_space_id
                            LEFT JOIN promise_snapshot p ON p.promised_entity_id = ff.order_item_id
                          ----This part of the query is used to extract missing marked ones created before today 6 am
                          
                            LEFT JOIN (SELECT DATE (src_created_time),
                                              *
                                       FROM dw_bradman.inventory_item_movement_request
                                       WHERE requesting_process_phase IN ('FULFILMENT_CRATING','FULFILMENT_BULK_PUT')
                                       AND   qty_missing > 0
                                       OR    qty_damaged > 0) miss ON miss.inventory_item_id = ii.natural_id
                          WHERE (ii.status = 'ACTIVE' OR ii.status IS NULL)) a) b
--left join bolt_order_item_snapshot ordjw on ordjw.order_item_id = a.fulfilment_item_id

  LEFT JOIN fulfilment_item_snapshot ffmp ON ffmp.jit_purchase_order_item_id = b.order_item_id
  LEFT JOIN bolt_order_item_snapshot ordmp ON ordmp.order_item_id = ffmp.order_item_id
  LEFT JOIN promise_snapshot p ON p.promised_entity_id = b.order_item_id
  LEFT JOIN listing_snapshot ls ON ls.listing_id = ordmp.listing_id
  LEFT JOIN sellerproduct_snapshot sps ON sps.sp_id = ls.sp_id
  LEFT JOIN product_snapshot_ prod ON prod.jpin = sps.jpin
  LEFT JOIN customer_snapshot_ c ON c.customerid = ordmp.buyer_id
  LEFT JOIN product_sourcing_attributes_snapshot psas ON psas.jpin = prod.jpin
WHERE DATE (p.updated_promise_time) >= DATE (CURRENT_DATE+INTERVAL '5 hour 30 minute') - 14
AND   DATE (p.promised_time) <= DATE (CURRENT_DATE+INTERVAL '5 hour 30 minute')
AND   action_basis_inventory_item IS NOT NULL
AND   ordmp.boltordertype = 'MARKETPLACE'
GROUP BY 1,
         2,
         3,
         4,
         5,
         7,
         6,
         8,
         9,
         10,
         11,
         12,
         13,
         b.action_basis_inventory_item,
         b.facility_name

commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/serviceRecoveryLog
