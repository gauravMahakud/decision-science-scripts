#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/appVersionLog
delete from app_version
where calendardate=current_date;
insert into app_version
select ca.calendardate, c.customerid  as uid,s.businessid as bid, c.status, max(app_version) as app_version /* assumption being app_version will always be progressive*/
from calendar ca
join customer_snapshot_ c on ca.calendardate>=date(c.srccreatedtime)
left join business_snapshot s on c.businessid = s.businessid
left join (select uid,app_version, date(min(convert_timezone('GMT', 'NEWZONE -5:30', ets)))
        from customer_app_events
        where uid is not null
        group by 1,2)ver on ver.uid=c.customerid and ver.date<=ca.calendardate
where c.istestcustomer is false and ca.calendardate=current_date
group by 1,2,3,4;
\o
commit;
EOF
echo "Ran summary script on" `date` >> ~/cronScripts/appVersionLog
