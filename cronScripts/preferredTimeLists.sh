#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/preferredTimeListsLog




drop table if exists preferred_time_inclusion_list;
commit;
drop table if exists preferred_time_exclusion_list;
commit;

with average_per_hour
as
(
select hour, avg(avg_cta) as average
from
(SELECT businessid,
       HOUR,
       SUM(number_of_cta) AS total_cta,
       COUNT(DISTINCT DATE) AS distinct_dates,
       SUM(number_of_cta)*1.00 / COUNT(DISTINCT DATE) AS avg_cta --,avg(number_of_cta) over (partition by hour) as avg_of_hour, sum(number_of_cta) over (partition by hour) * 1.00 / sum(distinct_dates) over (partition by hour) 
       FROM (SELECT DATE (ets +INTERVAL '5:30:00'),
                    c.businessid,
                    EXTRACT(HOUR FROM ets +INTERVAL '5:30:00') AS HOUR,
                    COUNT(DISTINCT eid) AS number_of_cta
             FROM customer_app_events cae
               LEFT JOIN customer_snapshot_ c ON c.customerid = cae.uid
             WHERE en = 'ON_APP_CAME_TO_FG'
             AND   istestcustomer IS FALSE
             AND   businessid NOT LIKE '%sajal%'
             AND   c.businessid NOT LIKE '%Puja%'
             AND   c.businessid NOT LIKE '%tech%'
             AND   DATE (ets +INTERVAL '5:30:00') >=date(getdate() + interval '5:30:00')- 31
             AND   DATE (ets +INTERVAL '5:30:00') <date(getdate() + interval '5:30:00')
             GROUP BY 1,
                      2,
                      3)
GROUP BY 1,
         2)
         group by 1),
         eligibility
         as
         (
         select a.*, average
         from
         (SELECT businessid,
       HOUR,
       case when hour < 14 and hour >= 6 then '06:00-13:59'
       when hour >= 14 and hour < 22 then '14:00-21:59'
       else '22:00-05:59' end as slot,
       max(date) as  last_cta,
       SUM(number_of_cta) AS total_cta,
       COUNT(DISTINCT DATE) AS distinct_dates,
       SUM(number_of_cta)*1.00 / COUNT(DISTINCT DATE) AS avg_cta --,avg(number_of_cta) over (partition by hour) as avg_of_hour, sum(number_of_cta) over (partition by hour) * 1.00 / sum(distinct_dates) over (partition by hour) 
       FROM (SELECT DATE (ets +INTERVAL '5:30:00'),
                    c.businessid,
                    EXTRACT(HOUR FROM ets +INTERVAL '5:30:00') AS HOUR,
                    COUNT(DISTINCT eid) AS number_of_cta
             FROM customer_app_events cae
               LEFT JOIN customer_snapshot_ c ON c.customerid = cae.uid
             WHERE en = 'ON_APP_CAME_TO_FG'
             AND   istestcustomer IS FALSE
             AND   businessid NOT LIKE '%sajal%'
             AND   c.businessid NOT LIKE '%Puja%'
             AND   c.businessid NOT LIKE '%tech%'
             AND   DATE (ets +INTERVAL '5:30:00') >= date(getdate() + interval '5:30:00')- 61
             AND   DATE (ets +INTERVAL '5:30:00') < date(getdate() + interval '5:30:00')
             GROUP BY 1,
                      2,
                      3)
GROUP BY 1,
         2,
         3) a
         left join average_per_hour aph on aph.hour = a.hour
         )
         

select * into preferred_time_inclusion_list
from
(
select *, row_number() over (partition by businessid, slot order by avg_cta desc, distinct_dates desc, last_cta desc ) as rnk_slot, row_number() over (partition by businessid order by avg_cta desc, distinct_dates desc, last_cta desc) as rnk_overall
from eligibility
)
where rnk_slot <= 2;

commit;
grant all on preferred_time_inclusion_list to periscope;
grant all on preferred_time_inclusion_list to yash;


commit;

