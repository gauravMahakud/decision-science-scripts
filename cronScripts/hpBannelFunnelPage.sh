#!/bin/sh
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PGPASSWORD='DWuser@123'
export PATH
psql -h datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com -p 5439 -U dwuser datawarehousedb << EOF
begin;
\o | cat >> ~/cronScripts/hpBannerFunnelPageLog


insert into hp_banner_funnel_page
WITH hp_banner_impr AS
(
  SELECT DISTINCT eid,
         ets +INTERVAL '5:30:00' AS ts,
         DATE (ets +INTERVAL '5:30:00'),
         enidarr,
         cardidxarr,
         uid,
         sid
  FROM customer_app_events
  WHERE en = 'ON_IMPRESSION_BROWSE_SCREEN'
  AND    grpid in ('UINODE-11386163283', 'UINODE-11386154371')
  AND   DATE (ets +INTERVAL '5:30:00')  = current_date
),
hp_banner_clicks AS
(
  SELECT 
         DATE (ets +INTERVAL '5:30:00'),
         sid, 
         enid,
         cardindx,
         businessid,
         max(extract(hour from ets +INTERVAL '5:30:00'))  last_click_hour
  FROM customer_app_events a
  join customer_snapshot_ c on c.customerid = a.uid and c.istestcustomer is false and c.businessid not like '%uja%' and c.businessid not like '%ech%' and c.businessid not like '%sajal%'
  WHERE en = 'ON_CLICK_BROWSE_ITEM'
  AND   grpid in ('UINODE-11386163283', 'UINODE-11386154371')
  AND   DATE (ets +INTERVAL '5:30:00') = current_date
  group by 1,2,3,4,5
),
hp_banner_impr_uinode AS
(
  SELECT DATE,
         sid,
         REPLACE(REPLACE(regexp_substr (SPLIT_PART(enidarr,',',CAST(NUMBER+ 1 AS INT)),'[C|U|n][^"]*'),']',''),'[','') AS enid,
         cast(REPLACE(REPLACE(SPLIT_PART(cardidxarr,',',CAST(NUMBER+ 1 AS INT)),']',''),'[','') as int) AS cardidx,
         businessid,
         max(extract(hour from ts)) as last_impr_hour
  FROM (SELECT *
        FROM hp_banner_impr hpb
          JOIN numbers n ON n.number < REGEXP_COUNT (enidarr,',') + 1
       ) a
  join customer_snapshot_ c on c.customerid = a.uid and c.istestcustomer is false and c.businessid not like '%uja%' and c.businessid not like '%ech%' and c.businessid not like '%sajal%'
  group by 1,2,3,4,5
),
inside_penid
as
(
select distinct uinodeid, childnodeid, trim(split_part(childnodeid, '|', cast(number + 1 as int))) as entityid
from
(
select distinct uinodeid, childnodeid, number
from uinode_snapshot_ ui
join numbers n on n.number < REGEXP_COUNT(childnodeid, '|') + 1
)
where uinodeid in (
select distinct enid
from 
(
select * 
from hp_banner_impr_uinode
union
select * 
from hp_banner_clicks
)
)
),
inside_impr_raw
as
(
 SELECT DISTINCT eid,
         ets +INTERVAL '5:30:00' AS ts,
         DATE (ets +INTERVAL '5:30:00'),
         enidarr,
         cardidxarr,
         uid,
         sid,
         grpid
  FROM customer_app_events
  WHERE en = 'ON_IMPRESSION_BROWSE_SCREEN'
  AND   penid in (select distinct entityid from inside_penid)
  AND   DATE (ets +INTERVAL '5:30:00')  = current_date
),
inside_clicks AS
(
  SELECT 
         DATE (ets +INTERVAL '5:30:00'),
         sid, 
         enid,
         cardindx,
         businessid,
         grpid,
         max(extract(hour from ets +INTERVAL '5:30:00'))  last_click_hour
  FROM customer_app_events a
  join customer_snapshot_ c on c.customerid = a.uid and c.istestcustomer is false and c.businessid not like '%uja%' and c.businessid not like '%ech%' and c.businessid not like '%sajal%'
  WHERE en = 'ON_CLICK_BROWSE_ITEM'
  AND   penid in (select distinct entityid from inside_penid)
  AND   DATE (ets +INTERVAL '5:30:00') = current_date
  group by 1,2,3,4,5,6
),
inside_impr
as
(
SELECT DATE,
         sid,
         REPLACE(REPLACE(regexp_substr (SPLIT_PART(enidarr,',',CAST(NUMBER+ 1 AS INT)),'[C|U|n][^"]*'),']',''),'[','') AS enid,
         REPLACE(REPLACE(SPLIT_PART(cardidxarr,',',CAST(NUMBER+ 1 AS INT)),']',''),'[','') AS cardidx,
         businessid,
         grpid,
         max(extract(hour from ts)) as last_impr_hour
  FROM (SELECT *
        FROM inside_impr_raw hpb
          JOIN numbers n ON n.number < REGEXP_COUNT (enidarr,',') + 1
       ) a
  join customer_snapshot_ c on c.customerid = a.uid and c.istestcustomer is false and c.businessid not like '%uja%' and c.businessid not like '%ech%' and c.businessid not like '%sajal%'
  group by 1,2,3,4,5,6
),
complete_stitch
as
(
select hpbi.date, hpbi.enid, hpbi.cardidx, iim.enid as insidecamp, iim.cardidx as insidecard,  count(distinct iim.businessid) as distinct_impr_biz, max(iim.last_impr_hour) as last_impr_hour, 
       count(distinct iic.businessid) as distinct_click_biz, max(iic.last_click_hour) as last_click_hour 
from hp_banner_impr_uinode hpbi
left join hp_banner_clicks hpbc on hpbc.enid =  hpbi.enid and hpbc.cardindx = hpbi.cardidx  and hpbi.sid = hpbc.sid and hpbi.businessid = hpbc.businessid
left join inside_penid ip on ip.uinodeid = hpbi.enid
left join inside_impr iim on iim.sid = hpbi.sid and iim.grpid = ip.entityid and iim.businessid = hpbi.businessid
left join inside_clicks iic on iic.sid = hpbi.sid and iic.grpid = ip.entityid and iic.businessid = hpbi.businessid
where hpbi.enid like '%UINODE%' and iim.enid is not null and iim.enid != ''
group by 1,2,3,4,5
),
impr_click_funnel_hp
as
(
select hpbi.date, hpbi.enid, hpbi.cardidx, count(distinct hpbi.businessid) as impr_bid_count, max(hpbi.last_impr_hour) as last_impr_hour, count(distinct hpbc.businessid) as click_bid_count, 
       max(hpbc.last_click_hour) as last_click_hour
from hp_banner_impr_uinode hpbi
left join hp_banner_clicks hpbc on hpbc.enid = hpbi.enid and hpbc.date = hpbi.date and hpbi.cardidx = hpbc.cardindx
where hpbi.enid like '%UINODE%'
group by 1,2,3
),
gmv_clicks
as
(select hpbc.date, hpbc.cardindx, iic.enid as insidecamp, iic.cardindx as insidecard, ca.campaignname, count(distinct iic.businessid) as clicks,  sum(isnull(caa.ppvs,0)) as ppvs, sum(isnull(caa.gmv,0)) as gmv, 
       sum(isnull(caa.bid_count,0)) as bid_count, sum(isnull(caa.units,0)) as units--, max(extract(hour from ts)) as last_hour_click
from
hp_banner_clicks hpbc 
left join inside_penid ip on ip.uinodeid = hpbc.enid
left join inside_clicks iic on iic.sid = hpbc.sid and iic.grpid = ip.entityid and iic.businessid = hpbc.businessid
left join campaign_snapshot_ ca on ca.campaignid = iic.enid
left join
(
SELECT campaignid,
              campaignname,
      internalcampaignname,
             DATE (ets),
             e.sid,
             max(case when e.state = 'Product Page View' then e.ets end) as ppv_ts,
             COUNT(DISTINCT CASE WHEN e.state = 'Product Page View' THEN businessid END) ppvs,             
             COUNT(DISTINCT CASE WHEN ord.order_item_id IS NOT NULL THEN c.businessid END) AS bid_count,
             SUM(CASE WHEN ord.order_item_id IS NOT NULL THEN ord.order_item_amount ELSE 0 END) AS gmv,
             SUM(CASE WHEN ord.order_item_id IS NOT NULL THEN ord.quantity ELSE 0 END) AS units
      FROM (SELECT campaignid,campaignname,internalcampaignname,
                   regexp_substr(TRIM(SPLIT_PART(B.campaignlistingidlist,',',cast(num.number +1 as INTEGER))),'L[^"]*') AS listing_id
            FROM numbers num
              INNER JOIN campaign_snapshot_ B ON num.number <= REGEXP_COUNT (B.campaignlistingidlist,',')
           ) ca
        JOIN listing_snapshot li ON li.listing_id = ca.listing_id
        JOIN sellerproduct_snapshot sp ON sp.sp_id = li.sp_id
        JOIN event_wise_target_source_new e ON e.jpin = sp.jpin and e.source_state = 'HomePage Banner'
        JOIN customer_snapshot_ c ON c.customerid = e.uid
        LEFT JOIN (SELECT cai.cartitemid,
                          cai.jpin,
                          ord.quantity,
                          ord.order_item_amount,
                          ord.order_item_id, ord.src_created_time
                   FROM cartitem_snapshot_ cai
                     JOIN bolt_order_item_v2_snapshot ord ON ord.cartitemid = cai.cartitemid
                   WHERE DATE (ord.src_created_time) =current_date) ord
               ON ord.cartitemid = e.target_identifier
              AND ord.jpin = e.jpin
      WHERE DATE (ets) = current_date
      
     
      GROUP BY 1,
               2,
               3,4,5
) caa on caa.campaignid = ca.campaignid and hpbc.date = caa.date and iic.sid = caa.sid --and hpbc.ts < caa.ppv_ts
group by 1,2,3,4,5)


select distinct ihp.*, ui.internalname, cs.insidecamp,camp.campaignname as campaignname, cs.insidecard, cs.distinct_impr_biz, cs.last_impr_hour as inside_last_impr_hr, cs.distinct_click_biz, 
       cs.last_click_hour as inside_last_click_hr, gc.clicks, gc.ppvs, gc.gmv, gc.bid_count,  gc.units
from impr_click_funnel_hp ihp
left join complete_stitch  cs on cs.date = ihp.date and ihp.enid = cs.enid and ihp.cardidx = cs.cardidx
left join gmv_clicks gc on gc.date = ihp.date and gc.cardindx = ihp.cardidx and gc.insidecamp = cs.insidecamp and gc.insidecard = cs.insidecard
left join uinode_snapshot_ ui on ui.uinodeid = ihp.enid
left join campaign_snapshot_ camp on camp.campaignid = cs.insidecamp
;
commit;




EOF
echo "Ran summary script on" `date` >> ~/cronScripts/hpBannerFunnelPageLog



